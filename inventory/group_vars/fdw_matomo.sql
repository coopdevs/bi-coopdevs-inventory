drop foreign table if exists matomo_db_confirm_date;	create FOREIGN TABLE matomo_db_confirm_date("page_name" text
	,"idvisit" bigint NOT NULL
	,"visitor" varchar(65)
	,"name" varchar(90) NOT NULL
	,"server_time" timestamp )SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'confirm_date');
drop foreign table if exists matomo_db_login_page;	create FOREIGN TABLE matomo_db_login_page("page_name" text
	,"idvisit" bigint NOT NULL
	,"visitor" varchar(65))SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'login_page');
drop foreign table if exists matomo_db_matomo_access;	create FOREIGN TABLE matomo_db_matomo_access("idaccess" int NOT NULL
	,"login" varchar(100) NOT NULL
	,"idsite" int NOT NULL
	,"access" varchar(50))SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_access');
drop foreign table if exists matomo_db_matomo_archive_blob_2020_11;	create FOREIGN TABLE matomo_db_matomo_archive_blob_2020_11("idarchive" int NOT NULL
	,"name" varchar(190) NOT NULL
	,"idsite" int
	,"date1" date
	,"date2" date
	,"period" smallint
	,"ts_archived" timestamp
	,"value" varchar(5000))SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_archive_blob_2020_11');
drop foreign table if exists matomo_db_matomo_archive_blob_2020_12;	create FOREIGN TABLE matomo_db_matomo_archive_blob_2020_12("idarchive" int NOT NULL
	,"name" varchar(190) NOT NULL
	,"idsite" int
	,"date1" date
	,"date2" date
	,"period" smallint
	,"ts_archived" timestamp
	,"value" varchar(5000))SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_archive_blob_2020_12');
drop foreign table if exists matomo_db_matomo_archive_blob_2021_01;	create FOREIGN TABLE matomo_db_matomo_archive_blob_2021_01("idarchive" int NOT NULL
	,"name" varchar(190) NOT NULL
	,"idsite" int
	,"date1" date
	,"date2" date
	,"period" smallint
	,"ts_archived" timestamp
	,"value" varchar(5000))SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_archive_blob_2021_01');
drop foreign table if exists matomo_db_matomo_archive_blob_2021_02;	create FOREIGN TABLE matomo_db_matomo_archive_blob_2021_02("idarchive" int NOT NULL
	,"name" varchar(190) NOT NULL
	,"idsite" int
	,"date1" date
	,"date2" date
	,"period" smallint
	,"ts_archived" timestamp
	,"value" varchar(5000))SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_archive_blob_2021_02');
drop foreign table if exists matomo_db_matomo_archive_blob_2021_03;	create FOREIGN TABLE matomo_db_matomo_archive_blob_2021_03("idarchive" int NOT NULL
	,"name" varchar(190) NOT NULL
	,"idsite" int
	,"date1" date
	,"date2" date
	,"period" smallint
	,"ts_archived" timestamp
	,"value" varchar(5000))SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_archive_blob_2021_03');
drop foreign table if exists matomo_db_matomo_archive_blob_2021_04;	create FOREIGN TABLE matomo_db_matomo_archive_blob_2021_04("idarchive" int NOT NULL
	,"name" varchar(190) NOT NULL
	,"idsite" int
	,"date1" date
	,"date2" date
	,"period" smallint
	,"ts_archived" timestamp
	,"value" varchar(5000))SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_archive_blob_2021_04');
drop foreign table if exists matomo_db_matomo_archive_blob_2021_05;	create FOREIGN TABLE matomo_db_matomo_archive_blob_2021_05("idarchive" int NOT NULL
	,"name" varchar(190) NOT NULL
	,"idsite" int
	,"date1" date
	,"date2" date
	,"period" smallint
	,"ts_archived" timestamp
	,"value" varchar(5000))SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_archive_blob_2021_05');
drop foreign table if exists matomo_db_matomo_archive_blob_2021_06;	create FOREIGN TABLE matomo_db_matomo_archive_blob_2021_06("idarchive" int NOT NULL
	,"name" varchar(190) NOT NULL
	,"idsite" int
	,"date1" date
	,"date2" date
	,"period" smallint
	,"ts_archived" timestamp
	,"value" varchar(5000))SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_archive_blob_2021_06');
drop foreign table if exists matomo_db_matomo_archive_blob_2021_07;	create FOREIGN TABLE matomo_db_matomo_archive_blob_2021_07("idarchive" int NOT NULL
	,"name" varchar(190) NOT NULL
	,"idsite" int
	,"date1" date
	,"date2" date
	,"period" smallint
	,"ts_archived" timestamp
	,"value" varchar(5000))SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_archive_blob_2021_07');
drop foreign table if exists matomo_db_matomo_archive_blob_2021_08;	create FOREIGN TABLE matomo_db_matomo_archive_blob_2021_08("idarchive" int NOT NULL
	,"name" varchar(190) NOT NULL
	,"idsite" int
	,"date1" date
	,"date2" date
	,"period" smallint
	,"ts_archived" timestamp
	,"value" varchar(5000))SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_archive_blob_2021_08');
drop foreign table if exists matomo_db_matomo_archive_blob_2021_09;	create FOREIGN TABLE matomo_db_matomo_archive_blob_2021_09("idarchive" int NOT NULL
	,"name" varchar(190) NOT NULL
	,"idsite" int
	,"date1" date
	,"date2" date
	,"period" smallint
	,"ts_archived" timestamp
	,"value" varchar(5000))SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_archive_blob_2021_09');
drop foreign table if exists matomo_db_matomo_archive_blob_2021_10;	create FOREIGN TABLE matomo_db_matomo_archive_blob_2021_10("idarchive" int NOT NULL
	,"name" varchar(190) NOT NULL
	,"idsite" int
	,"date1" date
	,"date2" date
	,"period" smallint
	,"ts_archived" timestamp
	,"value" varchar(5000))SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_archive_blob_2021_10');
drop foreign table if exists matomo_db_matomo_archive_blob_2021_11;	create FOREIGN TABLE matomo_db_matomo_archive_blob_2021_11("idarchive" int NOT NULL
	,"name" varchar(190) NOT NULL
	,"idsite" int
	,"date1" date
	,"date2" date
	,"period" smallint
	,"ts_archived" timestamp
	,"value" varchar(5000))SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_archive_blob_2021_11');
drop foreign table if exists matomo_db_matomo_archive_blob_2021_12;	create FOREIGN TABLE matomo_db_matomo_archive_blob_2021_12("idarchive" int NOT NULL
	,"name" varchar(190) NOT NULL
	,"idsite" int
	,"date1" date
	,"date2" date
	,"period" smallint
	,"ts_archived" timestamp
	,"value" varchar(5000))SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_archive_blob_2021_12');
drop foreign table if exists matomo_db_matomo_archive_invalidations;	create FOREIGN TABLE matomo_db_matomo_archive_invalidations("idinvalidation" bigint NOT NULL
	,"idarchive" int
	,"name" varchar(255) NOT NULL
	,"idsite" int NOT NULL
	,"date1" date NOT NULL
	,"date2" date NOT NULL
	,"period" smallint NOT NULL
	,"ts_invalidated" timestamp
	,"ts_started" timestamp
	,"status" smallint
	,"report" varchar(255))SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_archive_invalidations');
drop foreign table if exists matomo_db_matomo_archive_numeric_2020_11;	create FOREIGN TABLE matomo_db_matomo_archive_numeric_2020_11("idarchive" int NOT NULL
	,"name" varchar(190) NOT NULL
	,"idsite" int
	,"date1" date
	,"date2" date
	,"period" smallint
	,"ts_archived" timestamp
	,"value" numeric(9,2))SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_archive_numeric_2020_11');
drop foreign table if exists matomo_db_matomo_archive_numeric_2020_12;	create FOREIGN TABLE matomo_db_matomo_archive_numeric_2020_12("idarchive" int NOT NULL
	,"name" varchar(190) NOT NULL
	,"idsite" int
	,"date1" date
	,"date2" date
	,"period" smallint
	,"ts_archived" timestamp
	,"value" numeric(9,2))SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_archive_numeric_2020_12');
drop foreign table if exists matomo_db_matomo_archive_numeric_2021_01;	create FOREIGN TABLE matomo_db_matomo_archive_numeric_2021_01("idarchive" int NOT NULL
	,"name" varchar(190) NOT NULL
	,"idsite" int
	,"date1" date
	,"date2" date
	,"period" smallint
	,"ts_archived" timestamp
	,"value" numeric(9,2))SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_archive_numeric_2021_01');
drop foreign table if exists matomo_db_matomo_archive_numeric_2021_02;	create FOREIGN TABLE matomo_db_matomo_archive_numeric_2021_02("idarchive" int NOT NULL
	,"name" varchar(190) NOT NULL
	,"idsite" int
	,"date1" date
	,"date2" date
	,"period" smallint
	,"ts_archived" timestamp
	,"value" numeric(9,2))SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_archive_numeric_2021_02');
drop foreign table if exists matomo_db_matomo_archive_numeric_2021_03;	create FOREIGN TABLE matomo_db_matomo_archive_numeric_2021_03("idarchive" int NOT NULL
	,"name" varchar(190) NOT NULL
	,"idsite" int
	,"date1" date
	,"date2" date
	,"period" smallint
	,"ts_archived" timestamp
	,"value" numeric(9,2))SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_archive_numeric_2021_03');
drop foreign table if exists matomo_db_matomo_archive_numeric_2021_04;	create FOREIGN TABLE matomo_db_matomo_archive_numeric_2021_04("idarchive" int NOT NULL
	,"name" varchar(190) NOT NULL
	,"idsite" int
	,"date1" date
	,"date2" date
	,"period" smallint
	,"ts_archived" timestamp
	,"value" numeric(9,2))SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_archive_numeric_2021_04');
drop foreign table if exists matomo_db_matomo_archive_numeric_2021_05;	create FOREIGN TABLE matomo_db_matomo_archive_numeric_2021_05("idarchive" int NOT NULL
	,"name" varchar(190) NOT NULL
	,"idsite" int
	,"date1" date
	,"date2" date
	,"period" smallint
	,"ts_archived" timestamp
	,"value" numeric(9,2))SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_archive_numeric_2021_05');
drop foreign table if exists matomo_db_matomo_archive_numeric_2021_06;	create FOREIGN TABLE matomo_db_matomo_archive_numeric_2021_06("idarchive" int NOT NULL
	,"name" varchar(190) NOT NULL
	,"idsite" int
	,"date1" date
	,"date2" date
	,"period" smallint
	,"ts_archived" timestamp
	,"value" numeric(9,2))SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_archive_numeric_2021_06');
drop foreign table if exists matomo_db_matomo_archive_numeric_2021_07;	create FOREIGN TABLE matomo_db_matomo_archive_numeric_2021_07("idarchive" int NOT NULL
	,"name" varchar(190) NOT NULL
	,"idsite" int
	,"date1" date
	,"date2" date
	,"period" smallint
	,"ts_archived" timestamp
	,"value" numeric(9,2))SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_archive_numeric_2021_07');
drop foreign table if exists matomo_db_matomo_archive_numeric_2021_08;	create FOREIGN TABLE matomo_db_matomo_archive_numeric_2021_08("idarchive" int NOT NULL
	,"name" varchar(190) NOT NULL
	,"idsite" int
	,"date1" date
	,"date2" date
	,"period" smallint
	,"ts_archived" timestamp
	,"value" numeric(9,2))SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_archive_numeric_2021_08');
drop foreign table if exists matomo_db_matomo_archive_numeric_2021_09;	create FOREIGN TABLE matomo_db_matomo_archive_numeric_2021_09("idarchive" int NOT NULL
	,"name" varchar(190) NOT NULL
	,"idsite" int
	,"date1" date
	,"date2" date
	,"period" smallint
	,"ts_archived" timestamp
	,"value" numeric(9,2))SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_archive_numeric_2021_09');
drop foreign table if exists matomo_db_matomo_archive_numeric_2021_10;	create FOREIGN TABLE matomo_db_matomo_archive_numeric_2021_10("idarchive" int NOT NULL
	,"name" varchar(190) NOT NULL
	,"idsite" int
	,"date1" date
	,"date2" date
	,"period" smallint
	,"ts_archived" timestamp
	,"value" numeric(9,2))SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_archive_numeric_2021_10');
drop foreign table if exists matomo_db_matomo_archive_numeric_2021_11;	create FOREIGN TABLE matomo_db_matomo_archive_numeric_2021_11("idarchive" int NOT NULL
	,"name" varchar(190) NOT NULL
	,"idsite" int
	,"date1" date
	,"date2" date
	,"period" smallint
	,"ts_archived" timestamp
	,"value" numeric(9,2))SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_archive_numeric_2021_11');
drop foreign table if exists matomo_db_matomo_archive_numeric_2021_12;	create FOREIGN TABLE matomo_db_matomo_archive_numeric_2021_12("idarchive" int NOT NULL
	,"name" varchar(190) NOT NULL
	,"idsite" int
	,"date1" date
	,"date2" date
	,"period" smallint
	,"ts_archived" timestamp
	,"value" numeric(9,2))SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_archive_numeric_2021_12');
drop foreign table if exists matomo_db_matomo_brute_force_log;	create FOREIGN TABLE matomo_db_matomo_brute_force_log("id_brute_force_log" bigint NOT NULL
	,"ip_address" varchar(60)
	,"attempted_at" timestamp  NOT NULL
	,"login" varchar(100))SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_brute_force_log');
drop foreign table if exists matomo_db_matomo_custom_dimensions;	create FOREIGN TABLE matomo_db_matomo_custom_dimensions("idcustomdimension" bigint NOT NULL
	,"idsite" bigint NOT NULL
	,"name" varchar(100) NOT NULL
	,"index" smallint NOT NULL
	,"scope" varchar(10) NOT NULL
	,"active" smallint NOT NULL
	,"extractions" text NOT NULL
	,"case_sensitive" smallint NOT NULL)SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_custom_dimensions');
drop foreign table if exists matomo_db_matomo_experiments;	create FOREIGN TABLE matomo_db_matomo_experiments("idexperiment" int NOT NULL
	,"idsite" int NOT NULL
	,"confidence_threshold" decimal(3,1) NOT NULL
	,"mde_relative" smallint NOT NULL
	,"name" varchar(60) NOT NULL
	,"description" varchar(1000) NOT NULL
	,"hypothesis" varchar(1000) NOT NULL
	,"included_targets" text NOT NULL
	,"excluded_targets" text NOT NULL
	,"success_metrics" text NOT NULL
	,"percentage_participants" smallint NOT NULL
	,"original_redirect_url" text
	,"status" varchar(30) NOT NULL
	,"start_date" timestamp
	,"modified_date" timestamp  NOT NULL
	,"end_date" timestamp )SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_experiments');
drop foreign table if exists matomo_db_matomo_experiments_strategy;	create FOREIGN TABLE matomo_db_matomo_experiments_strategy("idexperiment" int NOT NULL
	,"metric" varchar(60) NOT NULL
	,"strategy" varchar(5) NOT NULL)SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_experiments_strategy');
drop foreign table if exists matomo_db_matomo_experiments_variations;	create FOREIGN TABLE matomo_db_matomo_experiments_variations("idvariation" int NOT NULL
	,"idexperiment" int NOT NULL
	,"name" varchar(60) NOT NULL
	,"percentage" smallint
	,"redirect_url" text
	,"deleted" smallint NOT NULL)SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_experiments_variations');
drop foreign table if exists matomo_db_matomo_goal;	create FOREIGN TABLE matomo_db_matomo_goal("idsite" int NOT NULL
	,"idgoal" int NOT NULL
	,"name" varchar(50) NOT NULL
	,"description" varchar(255) NOT NULL
	,"match_attribute" varchar(20) NOT NULL
	,"pattern" varchar(255) NOT NULL
	,"pattern_type" varchar(25) NOT NULL
	,"case_sensitive" smallint NOT NULL
	,"allow_multiple" smallint NOT NULL
	,"revenue" numeric(9,2) NOT NULL
	,"deleted" smallint NOT NULL
	,"event_value_as_revenue" smallint NOT NULL)SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_goal');
drop foreign table if exists matomo_db_matomo_locks;	create FOREIGN TABLE matomo_db_matomo_locks("key" varchar(70) NOT NULL
	,"value" varchar(255)
	,"expiry_time" bigint)SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_locks');
drop foreign table if exists matomo_db_matomo_log_abtesting;	create FOREIGN TABLE matomo_db_matomo_log_abtesting("idvisitor" varchar(8) NOT NULL
	,"idvisit" bigint NOT NULL
	,"idsite" int NOT NULL
	,"idexperiment" int NOT NULL
	,"idvariation" int NOT NULL
	,"entered" smallint
	,"server_time" timestamp  NOT NULL)SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'v_matomo_log_abtesting');
drop foreign table if exists matomo_db_matomo_log_action;	create FOREIGN TABLE matomo_db_matomo_log_action("idaction" int NOT NULL
	,"name" varchar(4096)
	,"hash" bigint NOT NULL
	,"type" smallint
	,"url_prefix" smallint)SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_log_action');
drop foreign table if exists matomo_db_matomo_log_conversion;	create FOREIGN TABLE matomo_db_matomo_log_conversion("idvisit" bigint NOT NULL
	,"idsite" int NOT NULL
	,"idvisitor" varchar(8) NOT NULL
	,"server_time" timestamp  NOT NULL
	,"idaction_url" int
	,"idlink_va" bigint
	,"idgoal" int NOT NULL
	,"buster" bigint NOT NULL
	,"idorder" varchar(100)
	,"items" smallint
	,"url" varchar(4096) NOT NULL
	,"revenue" float
	,"revenue_shipping" numeric(9,2)
	,"revenue_subtotal" numeric(9,2)
	,"revenue_tax" numeric(9,2)
	,"revenue_discount" numeric(9,2)
	,"visitor_returning" smallint
	,"visitor_seconds_since_first" int
	,"visitor_seconds_since_order" int
	,"visitor_count_visits" int NOT NULL
	,"referer_keyword" varchar(255)
	,"referer_name" varchar(255)
	,"referer_type" smallint
	,"config_browser_name" varchar(40)
	,"config_client_type" smallint
	,"config_device_brand" varchar(100)
	,"config_device_model" varchar(100)
	,"config_device_type" smallint
	,"location_city" varchar(255)
	,"location_country" char(3)
	,"location_latitude" decimal(9,6)
	,"location_longitude" decimal(9,6)
	,"location_region" char(3)
	,"custom_dimension_1" varchar(255)
	,"custom_dimension_2" varchar(255)
	,"custom_dimension_3" varchar(255)
	,"custom_dimension_4" varchar(255)
	,"custom_dimension_5" varchar(255))SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'v_matomo_log_conversion');
drop foreign table if exists matomo_db_matomo_log_conversion_item;	create FOREIGN TABLE matomo_db_matomo_log_conversion_item("idsite" int NOT NULL
	,"idvisitor" varchar(8) NOT NULL
	,"server_time" timestamp  NOT NULL
	,"idvisit" bigint NOT NULL
	,"idorder" varchar(100) NOT NULL
	,"idaction_sku" int NOT NULL
	,"idaction_name" int NOT NULL
	,"idaction_category" int NOT NULL
	,"idaction_category2" int NOT NULL
	,"idaction_category3" int NOT NULL
	,"idaction_category4" int NOT NULL
	,"idaction_category5" int NOT NULL
	,"price" numeric(9,2) NOT NULL
	,"quantity" int NOT NULL
	,"deleted" smallint NOT NULL)SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'v_matomo_log_conversion_item');
drop foreign table if exists matomo_db_matomo_log_form;	create FOREIGN TABLE matomo_db_matomo_log_form("idlogform" bigint NOT NULL
	,"idsiteform" int NOT NULL
	,"idsite" int NOT NULL
	,"idvisit" int NOT NULL
	,"idvisitor" varchar(8) NOT NULL
	,"first_idformview" varchar(6) NOT NULL
	,"last_idformview" varchar(6) NOT NULL
	,"num_views" int NOT NULL
	,"num_starts" int NOT NULL
	,"num_submissions" smallint NOT NULL
	,"converted" smallint NOT NULL
	,"form_last_action_time" timestamp  NOT NULL
	,"time_hesitation" int NOT NULL
	,"time_spent" int NOT NULL
	,"time_to_first_submission" int NOT NULL)SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'v_matomo_log_form');
drop foreign table if exists matomo_db_matomo_log_form_field;	create FOREIGN TABLE matomo_db_matomo_log_form_field("idlogform" bigint NOT NULL
	,"idlogformpage" bigint NOT NULL
	,"idformview" char(6) NOT NULL
	,"idpageview" char(6)
	,"field_name" varchar(75) NOT NULL
	,"time_spent" int NOT NULL
	,"time_hesitation" int NOT NULL
	,"field_size" int NOT NULL
	,"left_blank" smallint NOT NULL
	,"submitted" smallint NOT NULL
	,"num_changes" smallint NOT NULL
	,"num_focus" smallint NOT NULL
	,"num_deletes" smallint NOT NULL
	,"num_cursor" smallint NOT NULL)SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_log_form_field');
drop foreign table if exists matomo_db_matomo_log_form_page;	create FOREIGN TABLE matomo_db_matomo_log_form_page("idlogformpage" bigint NOT NULL
	,"idlogform" bigint NOT NULL
	,"idaction_url" int
	,"num_views" int NOT NULL
	,"num_starts" int NOT NULL
	,"num_submissions" smallint NOT NULL
	,"time_hesitation" int NOT NULL
	,"time_spent" int NOT NULL
	,"time_to_first_submission" int NOT NULL
	,"entry_field_name" varchar(75)
	,"exit_field_name" varchar(75))SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_log_form_page');
drop foreign table if exists matomo_db_matomo_log_link_visit_action;	create FOREIGN TABLE matomo_db_matomo_log_link_visit_action("idlink_va" bigint NOT NULL
	,"idsite" int NOT NULL
	,"idvisitor" varchar(8) NOT NULL
	,"idvisit" bigint NOT NULL
	,"idaction_url_ref" int
	,"idaction_name_ref" int
	,"custom_float" numeric(9,2)
	,"pageview_position" int
	,"server_time" timestamp  NOT NULL
	,"idpageview" char(6)
	,"idaction_name" int
	,"idaction_url" int
	,"search_cat" varchar(200)
	,"search_count" int
	,"time_spent_ref_action" int
	,"idaction_product_cat" int
	,"idaction_product_cat2" int
	,"idaction_product_cat3" int
	,"idaction_product_cat4" int
	,"idaction_product_cat5" int
	,"idaction_product_name" int
	,"product_price" numeric(9,2)
	,"idaction_product_sku" int
	,"idaction_event_action" int
	,"idaction_event_category" int
	,"idaction_content_interaction" int
	,"idaction_content_name" int
	,"idaction_content_piece" int
	,"idaction_content_target" int
	,"time_dom_completion" int
	,"time_dom_processing" int
	,"time_network" int
	,"time_on_load" int
	,"time_server" int
	,"time_transfer" int
	,"time_spent" int
	,"custom_dimension_1" varchar(255)
	,"custom_dimension_2" varchar(255)
	,"custom_dimension_3" varchar(255)
	,"custom_dimension_4" varchar(255)
	,"custom_dimension_5" varchar(255))SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'v_matomo_log_link_visit_action');
drop foreign table if exists matomo_db_matomo_log_profiling;	create FOREIGN TABLE matomo_db_matomo_log_profiling("query" text NOT NULL
	,"count" int
	,"sum_time_ms" float
	,"idprofiling" bigint NOT NULL)SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_log_profiling');
drop foreign table if exists matomo_db_matomo_log_visit;	create FOREIGN TABLE matomo_db_matomo_log_visit("idvisit" bigint NOT NULL
	,"idsite" int NOT NULL
	,"idvisitor" varchar(8) NOT NULL
	,"visit_last_action_time" timestamp  NOT NULL
	,"config_id" varchar(8) NOT NULL
	,"location_ip" varchar(16) NOT NULL
	,"profilable" smallint
	,"user_id" varchar(200)
	,"visit_first_action_time" timestamp  NOT NULL
	,"visit_goal_buyer" smallint
	,"visit_goal_converted" smallint
	,"visitor_returning" smallint
	,"visitor_seconds_since_first" int
	,"visitor_seconds_since_order" int
	,"visitor_count_visits" int NOT NULL
	,"visit_entry_idaction_name" int
	,"visit_entry_idaction_url" int
	,"visit_exit_idaction_name" int
	,"visit_exit_idaction_url" int
	,"visit_total_actions" int
	,"visit_total_interactions" int
	,"visit_total_searches" smallint
	,"referer_keyword" varchar(255)
	,"referer_name" varchar(255)
	,"referer_type" smallint
	,"referer_url" varchar(1500)
	,"location_browser_lang" varchar(20)
	,"config_browser_engine" varchar(10)
	,"config_browser_name" varchar(40)
	,"config_browser_version" varchar(20)
	,"config_client_type" smallint
	,"config_device_brand" varchar(100)
	,"config_device_model" varchar(100)
	,"config_device_type" smallint
	,"config_os" char(3)
	,"config_os_version" varchar(100)
	,"visit_total_events" int
	,"visitor_localtime" time
	,"visitor_seconds_since_last" int
	,"config_resolution" varchar(18)
	,"config_cookie" smallint
	,"config_flash" smallint
	,"config_java" smallint
	,"config_pdf" smallint
	,"config_quicktime" smallint
	,"config_realplayer" smallint
	,"config_silverlight" smallint
	,"config_windowsmedia" smallint
	,"visit_total_time" int NOT NULL
	,"location_city" varchar(255)
	,"location_country" char(3)
	,"location_latitude" decimal(9,6)
	,"location_longitude" decimal(9,6)
	,"location_region" char(3)
	,"last_idlink_va" bigint
	,"custom_dimension_1" varchar(255)
	,"custom_dimension_2" varchar(255)
	,"custom_dimension_3" varchar(255)
	,"custom_dimension_4" varchar(255)
	,"custom_dimension_5" varchar(255))SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'v_matomo_log_visit');
drop foreign table if exists matomo_db_matomo_logger_message;	create FOREIGN TABLE matomo_db_matomo_logger_message("idlogger_message" int NOT NULL
	,"tag" varchar(50)
	,"timestamp" timestamp
	,"level" varchar(16)
	,"message" text)SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_logger_message');
drop foreign table if exists matomo_db_matomo_option;	create FOREIGN TABLE matomo_db_matomo_option("option_name" varchar(191) NOT NULL
	,"option_value" varchar(5000) NOT NULL
	,"autoload" smallint NOT NULL)SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_option');
drop foreign table if exists matomo_db_matomo_plugin_setting;	create FOREIGN TABLE matomo_db_matomo_plugin_setting("plugin_name" varchar(60) NOT NULL
	,"setting_name" varchar(255) NOT NULL
	,"setting_value" varchar(5000) NOT NULL
	,"json_encoded" smallint NOT NULL
	,"user_login" varchar(100) NOT NULL
	,"idplugin_setting" bigint NOT NULL)SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_plugin_setting');
drop foreign table if exists matomo_db_matomo_privacy_logdata_anonymizations;	create FOREIGN TABLE matomo_db_matomo_privacy_logdata_anonymizations("idlogdata_anonymization" bigint NOT NULL
	,"idsites" text
	,"date_start" timestamp  NOT NULL
	,"date_end" timestamp  NOT NULL
	,"anonymize_ip" smallint NOT NULL
	,"anonymize_location" smallint NOT NULL
	,"anonymize_userid" smallint NOT NULL
	,"unset_visit_columns" text NOT NULL
	,"unset_link_visit_action_columns" text NOT NULL
	,"output" varchar(5000)
	,"scheduled_date" timestamp
	,"job_start_date" timestamp
	,"job_finish_date" timestamp
	,"requester" varchar(100) NOT NULL)SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_privacy_logdata_anonymizations');
drop foreign table if exists matomo_db_matomo_report;	create FOREIGN TABLE matomo_db_matomo_report("idreport" int NOT NULL
	,"idsite" int NOT NULL
	,"login" varchar(100) NOT NULL
	,"description" varchar(255) NOT NULL
	,"idsegment" int
	,"period" varchar(10) NOT NULL
	,"hour" smallint NOT NULL
	,"type" varchar(10) NOT NULL
	,"format" varchar(10) NOT NULL
	,"reports" text NOT NULL
	,"parameters" text
	,"ts_created" timestamp
	,"ts_last_sent" timestamp
	,"deleted" smallint NOT NULL
	,"evolution_graph_within_period" smallint NOT NULL
	,"evolution_graph_period_n" int NOT NULL
	,"period_param" varchar(10))SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_report');
drop foreign table if exists matomo_db_matomo_report_subscriptions;	create FOREIGN TABLE matomo_db_matomo_report_subscriptions("idreport" int NOT NULL
	,"token" varchar(100)
	,"email" varchar(100) NOT NULL
	,"ts_subscribed" timestamp NOT NULL
	,"ts_unsubscribed" timestamp)SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_report_subscriptions');
drop foreign table if exists matomo_db_matomo_segment;	create FOREIGN TABLE matomo_db_matomo_segment("idsegment" int NOT NULL
	,"name" varchar(255) NOT NULL
	,"definition" text NOT NULL
	,"hash" char(32)
	,"login" varchar(100) NOT NULL
	,"enable_all_users" smallint NOT NULL
	,"enable_only_idsite" int
	,"auto_archive" smallint NOT NULL
	,"ts_created" timestamp
	,"ts_last_edit" timestamp
	,"deleted" smallint NOT NULL)SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_segment');
drop foreign table if exists matomo_db_matomo_sequence;	create FOREIGN TABLE matomo_db_matomo_sequence("name" varchar(120) NOT NULL
	,"value" bigint NOT NULL)SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_sequence');
drop foreign table if exists matomo_db_matomo_session;	create FOREIGN TABLE matomo_db_matomo_session("id" varchar(191) NOT NULL
	,"modified" int
	,"lifetime" int
	,"data" text)SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_session');
drop foreign table if exists matomo_db_matomo_site;	create FOREIGN TABLE matomo_db_matomo_site("idsite" int NOT NULL
	,"name" varchar(90) NOT NULL
	,"main_url" varchar(255) NOT NULL
	,"ts_created" timestamp
	,"ecommerce" smallint
	,"sitesearch" smallint
	,"sitesearch_keyword_parameters" text NOT NULL
	,"sitesearch_category_parameters" text NOT NULL
	,"timezone" varchar(50) NOT NULL
	,"currency" char(3) NOT NULL
	,"exclude_unknown_urls" smallint
	,"excluded_ips" text NOT NULL
	,"excluded_parameters" text NOT NULL
	,"excluded_user_agents" text NOT NULL
	,"group" varchar(250) NOT NULL
	,"type" varchar(255) NOT NULL
	,"keep_url_fragment" smallint NOT NULL
	,"creator_login" varchar(100))SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_site');
drop foreign table if exists matomo_db_matomo_site_form;	create FOREIGN TABLE matomo_db_matomo_site_form("idsiteform" int NOT NULL
	,"idsite" int NOT NULL
	,"name" varchar(50) NOT NULL
	,"description" varchar(1000) NOT NULL
	,"status" varchar(10) NOT NULL
	,"in_overview" smallint NOT NULL
	,"match_form_rules" text
	,"match_page_rules" text
	,"conversion_rules" text
	,"fields" varchar(5000)
	,"auto_created" smallint NOT NULL
	,"created_date" timestamp  NOT NULL
	,"updated_date" timestamp  NOT NULL)SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_site_form');
drop foreign table if exists matomo_db_matomo_site_setting;	create FOREIGN TABLE matomo_db_matomo_site_setting("idsite" int NOT NULL
	,"plugin_name" varchar(60) NOT NULL
	,"setting_name" varchar(255) NOT NULL
	,"setting_value" varchar(5000) NOT NULL
	,"json_encoded" smallint NOT NULL
	,"idsite_setting" bigint NOT NULL)SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_site_setting');
drop foreign table if exists matomo_db_matomo_site_url;	create FOREIGN TABLE matomo_db_matomo_site_url("idsite" int NOT NULL
	,"url" varchar(190) NOT NULL)SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_site_url');
drop foreign table if exists matomo_db_matomo_tagmanager_container;	create FOREIGN TABLE matomo_db_matomo_tagmanager_container("idcontainer" varchar(8) NOT NULL
	,"idsite" int NOT NULL
	,"context" varchar(10) NOT NULL
	,"name" varchar(50) NOT NULL
	,"description" varchar(1000) NOT NULL
	,"status" varchar(10) NOT NULL
	,"created_date" timestamp  NOT NULL
	,"updated_date" timestamp  NOT NULL
	,"deleted_date" timestamp )SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_tagmanager_container');
drop foreign table if exists matomo_db_matomo_tagmanager_container_release;	create FOREIGN TABLE matomo_db_matomo_tagmanager_container_release("idcontainerrelease" bigint NOT NULL
	,"idcontainer" varchar(8) NOT NULL
	,"idcontainerversion" bigint NOT NULL
	,"idsite" int NOT NULL
	,"status" varchar(10) NOT NULL
	,"environment" varchar(40) NOT NULL
	,"release_login" varchar(100) NOT NULL
	,"release_date" timestamp  NOT NULL
	,"deleted_date" timestamp )SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_tagmanager_container_release');
drop foreign table if exists matomo_db_matomo_tagmanager_container_version;	create FOREIGN TABLE matomo_db_matomo_tagmanager_container_version("idcontainerversion" bigint NOT NULL
	,"idcontainer" varchar(8) NOT NULL
	,"idsite" int NOT NULL
	,"status" varchar(10) NOT NULL
	,"revision" int NOT NULL
	,"name" varchar(50) NOT NULL
	,"description" varchar(1000) NOT NULL
	,"created_date" timestamp  NOT NULL
	,"updated_date" timestamp  NOT NULL
	,"deleted_date" timestamp )SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_tagmanager_container_version');
drop foreign table if exists matomo_db_matomo_tagmanager_tag;	create FOREIGN TABLE matomo_db_matomo_tagmanager_tag("idtag" bigint NOT NULL
	,"idcontainerversion" bigint NOT NULL
	,"idsite" int NOT NULL
	,"type" varchar(50) NOT NULL
	,"name" varchar(50) NOT NULL
	,"status" varchar(10) NOT NULL
	,"parameters" varchar(5000) NOT NULL
	,"fire_trigger_ids" text NOT NULL
	,"block_trigger_ids" text NOT NULL
	,"fire_limit" varchar(20) NOT NULL
	,"priority" smallint NOT NULL
	,"fire_delay" int NOT NULL
	,"start_date" timestamp
	,"end_date" timestamp
	,"created_date" timestamp  NOT NULL
	,"updated_date" timestamp  NOT NULL
	,"deleted_date" timestamp )SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_tagmanager_tag');
drop foreign table if exists matomo_db_matomo_tagmanager_trigger;	create FOREIGN TABLE matomo_db_matomo_tagmanager_trigger("idtrigger" bigint NOT NULL
	,"idcontainerversion" bigint NOT NULL
	,"idsite" int NOT NULL
	,"type" varchar(50) NOT NULL
	,"name" varchar(50) NOT NULL
	,"status" varchar(10) NOT NULL
	,"parameters" varchar(5000) NOT NULL
	,"conditions" varchar(5000) NOT NULL
	,"created_date" timestamp  NOT NULL
	,"updated_date" timestamp  NOT NULL
	,"deleted_date" timestamp )SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_tagmanager_trigger');
drop foreign table if exists matomo_db_matomo_tagmanager_variable;	create FOREIGN TABLE matomo_db_matomo_tagmanager_variable("idvariable" bigint NOT NULL
	,"idcontainerversion" bigint NOT NULL
	,"idsite" int NOT NULL
	,"type" varchar(50) NOT NULL
	,"name" varchar(50) NOT NULL
	,"status" varchar(10) NOT NULL
	,"parameters" varchar(5000) NOT NULL
	,"lookup_table" varchar(5000) NOT NULL
	,"default_value" text
	,"created_date" timestamp  NOT NULL
	,"updated_date" timestamp  NOT NULL
	,"deleted_date" timestamp )SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_tagmanager_variable');
drop foreign table if exists matomo_db_matomo_tracking_failure;	create FOREIGN TABLE matomo_db_matomo_tracking_failure("idsite" bigint NOT NULL
	,"idfailure" smallint NOT NULL
	,"date_first_occurred" timestamp  NOT NULL
	,"request_url" varchar(5000) NOT NULL)SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_tracking_failure');
drop foreign table if exists matomo_db_matomo_twofactor_recovery_code;	create FOREIGN TABLE matomo_db_matomo_twofactor_recovery_code("idrecoverycode" bigint NOT NULL
	,"login" varchar(100) NOT NULL
	,"recovery_code" varchar(40) NOT NULL)SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_twofactor_recovery_code');
drop foreign table if exists matomo_db_matomo_user;	create FOREIGN TABLE matomo_db_matomo_user("login" varchar(100) NOT NULL
	,"password" varchar(255) NOT NULL
	,"email" varchar(100) NOT NULL
	,"twofactor_secret" varchar(40) NOT NULL
	,"superuser_access" smallint NOT NULL
	,"date_registered" timestamp
	,"ts_password_modified" timestamp)SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_user');
drop foreign table if exists matomo_db_matomo_user_dashboard;	create FOREIGN TABLE matomo_db_matomo_user_dashboard("login" varchar(100) NOT NULL
	,"iddashboard" int NOT NULL
	,"name" varchar(100)
	,"layout" text NOT NULL)SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_user_dashboard');
drop foreign table if exists matomo_db_matomo_user_language;	create FOREIGN TABLE matomo_db_matomo_user_language("login" varchar(100) NOT NULL
	,"language" varchar(10) NOT NULL
	,"use_12_hour_clock" smallint NOT NULL)SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_user_language');
drop foreign table if exists matomo_db_matomo_user_token_auth;	create FOREIGN TABLE matomo_db_matomo_user_token_auth("idusertokenauth" bigint NOT NULL
	,"login" varchar(100) NOT NULL
	,"description" varchar(100) NOT NULL
	,"password" varchar(191) NOT NULL
	,"hash_algo" varchar(30) NOT NULL
	,"system_token" smallint NOT NULL
	,"last_used" timestamp
	,"date_created" timestamp  NOT NULL
	,"date_expired" timestamp )SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'matomo_user_token_auth');
drop foreign table if exists matomo_db_payment_date;	create FOREIGN TABLE matomo_db_payment_date("page_name" text
	,"idvisit" bigint NOT NULL
	,"visitor" varchar(65)
	,"name" varchar(90) NOT NULL
	,"server_time" timestamp )SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'payment_date');
drop foreign table if exists matomo_db_product_min_server_time;	create FOREIGN TABLE matomo_db_product_min_server_time("page_name" text
	,"idvisit" bigint NOT NULL
	,"visitor" varchar(65)
	,"name" varchar(90) NOT NULL
	,"min_server_time" timestamp )SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'product_min_server_time');
drop foreign table if exists matomo_db_product_min_server_time_next;	create FOREIGN TABLE matomo_db_product_min_server_time_next("page_name" text
	,"idvisit" bigint NOT NULL
	,"visitor" varchar(65)
	,"name" varchar(90) NOT NULL
	,"min_server_time" timestamp
	,"min_server_time_next" timestamp )SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'product_min_server_time_next');
drop foreign table if exists matomo_db_thanks_date;	create FOREIGN TABLE matomo_db_thanks_date("page_name" text
	,"idvisit" bigint NOT NULL
	,"visitor" varchar(65)
	,"name" varchar(90) NOT NULL
	,"server_time" timestamp )SERVER mysql_fdw_matomo OPTIONS (dbname 'matomo_db', table_name 'thanks_date');
