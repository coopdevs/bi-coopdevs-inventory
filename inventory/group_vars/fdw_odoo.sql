drop foreign table if exists odoo_account_account cascade;
create FOREIGN TABLE odoo_account_account("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"currency_id" int4
	,"code" varchar(64) NOT NULL
	,"deprecated" bool
	,"user_type_id" int4 NOT NULL
	,"internal_type" varchar
	,"internal_group" varchar
	,"last_time_entries_checked" timestamp
	,"reconcile" bool
	,"note" text
	,"company_id" int4 NOT NULL
	,"group_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"asset_profile_id" int4
	,"centralized" bool
	,"_api_external_id" int4
	,"external_id_sequence_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_account');
drop foreign table if exists odoo_account_account_account_group_rel cascade;
create FOREIGN TABLE odoo_account_account_account_group_rel("account_group_id" int4 NOT NULL
	,"account_account_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_account_account_group_rel');
drop foreign table if exists odoo_account_account_account_tag cascade;
create FOREIGN TABLE odoo_account_account_account_tag("account_account_id" int4 NOT NULL
	,"account_account_tag_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_account_account_tag');
drop foreign table if exists odoo_account_account_aged_partner_balance_wizard_rel cascade;
create FOREIGN TABLE odoo_account_account_aged_partner_balance_wizard_rel("aged_partner_balance_wizard_id" int4 NOT NULL
	,"account_account_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_account_aged_partner_balance_wizard_rel');
drop foreign table if exists odoo_account_account_general_ledger_report_wizard_rel cascade;
create FOREIGN TABLE odoo_account_account_general_ledger_report_wizard_rel("general_ledger_report_wizard_id" int4 NOT NULL
	,"account_account_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_account_general_ledger_report_wizard_rel');
drop foreign table if exists odoo_account_account_open_items_report_wizard_rel cascade;
create FOREIGN TABLE odoo_account_account_open_items_report_wizard_rel("open_items_report_wizard_id" int4 NOT NULL
	,"account_account_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_account_open_items_report_wizard_rel');
drop foreign table if exists odoo_account_account_report_aged_partner_balance_rel cascade;
create FOREIGN TABLE odoo_account_account_report_aged_partner_balance_rel("report_aged_partner_balance_id" int4 NOT NULL
	,"account_account_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_account_report_aged_partner_balance_rel');
drop foreign table if exists odoo_account_account_report_general_ledger_rel cascade;
create FOREIGN TABLE odoo_account_account_report_general_ledger_rel("report_general_ledger_id" int4 NOT NULL
	,"account_account_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_account_report_general_ledger_rel');
drop foreign table if exists odoo_account_account_report_open_items_rel cascade;
create FOREIGN TABLE odoo_account_account_report_open_items_rel("report_open_items_id" int4 NOT NULL
	,"account_account_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_account_report_open_items_rel');
drop foreign table if exists odoo_account_account_report_trial_balance_account_rel cascade;
create FOREIGN TABLE odoo_account_account_report_trial_balance_account_rel("report_trial_balance_account_id" int4 NOT NULL
	,"account_account_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_account_report_trial_balance_account_rel');
drop foreign table if exists odoo_account_account_report_trial_balance_rel cascade;
create FOREIGN TABLE odoo_account_account_report_trial_balance_rel("report_trial_balance_id" int4 NOT NULL
	,"account_account_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_account_report_trial_balance_rel');
drop foreign table if exists odoo_account_account_tag cascade;
create FOREIGN TABLE odoo_account_account_tag("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"applicability" varchar NOT NULL
	,"color" int4
	,"active" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_account_tag');
drop foreign table if exists odoo_account_account_tag_account_tax_template_rel cascade;
create FOREIGN TABLE odoo_account_account_tag_account_tax_template_rel("account_tax_template_id" int4 NOT NULL
	,"account_account_tag_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_account_tag_account_tax_template_rel');
drop foreign table if exists odoo_account_account_tax_default_rel cascade;
create FOREIGN TABLE odoo_account_account_tax_default_rel("account_id" int4 NOT NULL
	,"tax_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_account_tax_default_rel');
drop foreign table if exists odoo_account_account_template cascade;
create FOREIGN TABLE odoo_account_account_template("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"currency_id" int4
	,"code" varchar(64) NOT NULL
	,"user_type_id" int4 NOT NULL
	,"reconcile" bool
	,"note" text
	,"nocreate" bool
	,"chart_template_id" int4
	,"group_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_account_template');
drop foreign table if exists odoo_account_account_template_account_tag cascade;
create FOREIGN TABLE odoo_account_account_template_account_tag("account_account_template_id" int4 NOT NULL
	,"account_account_tag_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_account_template_account_tag');
drop foreign table if exists odoo_account_account_template_tax_rel cascade;
create FOREIGN TABLE odoo_account_account_template_tax_rel("account_id" int4 NOT NULL
	,"tax_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_account_template_tax_rel');
drop foreign table if exists odoo_account_account_trial_balance_report_wizard_rel cascade;
create FOREIGN TABLE odoo_account_account_trial_balance_report_wizard_rel("trial_balance_report_wizard_id" int4 NOT NULL
	,"account_account_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_account_trial_balance_report_wizard_rel');
drop foreign table if exists odoo_account_account_type cascade;
create FOREIGN TABLE odoo_account_account_type("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"include_initial_balance" bool
	,"type" varchar NOT NULL
	,"internal_group" varchar
	,"note" text
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_account_type');
drop foreign table if exists odoo_account_account_type_general_ledger_report_wizard_rel cascade;
create FOREIGN TABLE odoo_account_account_type_general_ledger_report_wizard_rel("general_ledger_report_wizard_id" int4 NOT NULL
	,"account_account_type_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_account_type_general_ledger_report_wizard_rel');
drop foreign table if exists odoo_account_account_type_rel cascade;
create FOREIGN TABLE odoo_account_account_type_rel("journal_id" int4 NOT NULL
	,"account_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_account_type_rel');
drop foreign table if exists odoo_account_analytic_account cascade;
create FOREIGN TABLE odoo_account_analytic_account("id" int4 NOT NULL
	,"message_main_attachment_id" int4
	,"name" varchar NOT NULL
	,"code" varchar
	,"active" bool
	,"group_id" int4
	,"company_id" int4
	,"partner_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_analytic_account');
drop foreign table if exists odoo_account_analytic_account_general_ledger_report_wizard_rel cascade;
create FOREIGN TABLE odoo_account_analytic_account_general_ledger_report_wizard_rel("general_ledger_report_wizard_id" int4 NOT NULL
	,"account_analytic_account_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_analytic_account_general_ledger_report_wizard_rel');
drop foreign table if exists odoo_account_analytic_account_report_general_ledger_rel cascade;
create FOREIGN TABLE odoo_account_analytic_account_report_general_ledger_rel("report_general_ledger_id" int4 NOT NULL
	,"account_analytic_account_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_analytic_account_report_general_ledger_rel');
drop foreign table if exists odoo_account_analytic_distribution cascade;
create FOREIGN TABLE odoo_account_analytic_distribution("id" int4 NOT NULL
	,"account_id" int4 NOT NULL
	,"percentage" float8 NOT NULL
	,"tag_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_analytic_distribution');
drop foreign table if exists odoo_account_analytic_group cascade;
create FOREIGN TABLE odoo_account_analytic_group("id" int4 NOT NULL
	,"parent_path" varchar
	,"name" varchar NOT NULL
	,"description" text
	,"parent_id" int4
	,"complete_name" varchar
	,"company_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_analytic_group');
drop foreign table if exists odoo_account_analytic_line cascade;
create FOREIGN TABLE odoo_account_analytic_line("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"date" date NOT NULL
	,"amount" numeric NOT NULL
	,"unit_amount" float8
	,"product_uom_id" int4
	,"account_id" int4 NOT NULL
	,"partner_id" int4
	,"user_id" int4
	,"company_id" int4 NOT NULL
	,"currency_id" int4
	,"group_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"product_id" int4
	,"general_account_id" int4
	,"move_id" int4
	,"code" varchar(8)
	,"ref" varchar
	,"so_line" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_analytic_line');
drop foreign table if exists odoo_account_analytic_line_tag_rel cascade;
create FOREIGN TABLE odoo_account_analytic_line_tag_rel("line_id" int4 NOT NULL
	,"tag_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_analytic_line_tag_rel');
drop foreign table if exists odoo_account_analytic_tag cascade;
create FOREIGN TABLE odoo_account_analytic_tag("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"color" int4
	,"active" bool
	,"active_analytic_distribution" bool
	,"company_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_analytic_tag');
drop foreign table if exists odoo_account_analytic_tag_account_invoice_line_rel cascade;
create FOREIGN TABLE odoo_account_analytic_tag_account_invoice_line_rel("account_invoice_line_id" int4 NOT NULL
	,"account_analytic_tag_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_analytic_tag_account_invoice_line_rel');
drop foreign table if exists odoo_account_analytic_tag_account_invoice_tax_rel cascade;
create FOREIGN TABLE odoo_account_analytic_tag_account_invoice_tax_rel("account_invoice_tax_id" int4 NOT NULL
	,"account_analytic_tag_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_analytic_tag_account_invoice_tax_rel');
drop foreign table if exists odoo_account_analytic_tag_account_move_line_rel cascade;
create FOREIGN TABLE odoo_account_analytic_tag_account_move_line_rel("account_move_line_id" int4 NOT NULL
	,"account_analytic_tag_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_analytic_tag_account_move_line_rel');
drop foreign table if exists odoo_account_analytic_tag_account_reconcile_model_rel cascade;
create FOREIGN TABLE odoo_account_analytic_tag_account_reconcile_model_rel("account_reconcile_model_id" int4 NOT NULL
	,"account_analytic_tag_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_analytic_tag_account_reconcile_model_rel');
drop foreign table if exists odoo_account_analytic_tag_contract_line_rel cascade;
create FOREIGN TABLE odoo_account_analytic_tag_contract_line_rel("contract_line_id" int4 NOT NULL
	,"account_analytic_tag_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_analytic_tag_contract_line_rel');
drop foreign table if exists odoo_account_analytic_tag_general_ledger_report_wizard_rel cascade;
create FOREIGN TABLE odoo_account_analytic_tag_general_ledger_report_wizard_rel("general_ledger_report_wizard_id" int4 NOT NULL
	,"account_analytic_tag_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_analytic_tag_general_ledger_report_wizard_rel');
drop foreign table if exists odoo_account_analytic_tag_mis_budget_by_account_item_rel cascade;
create FOREIGN TABLE odoo_account_analytic_tag_mis_budget_by_account_item_rel("mis_budget_by_account_item_id" int4 NOT NULL
	,"account_analytic_tag_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_analytic_tag_mis_budget_by_account_item_rel');
drop foreign table if exists odoo_account_analytic_tag_mis_budget_item_rel cascade;
create FOREIGN TABLE odoo_account_analytic_tag_mis_budget_item_rel("mis_budget_item_id" int4 NOT NULL
	,"account_analytic_tag_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_analytic_tag_mis_budget_item_rel');
drop foreign table if exists odoo_account_analytic_tag_mis_report_instance_period_rel cascade;
create FOREIGN TABLE odoo_account_analytic_tag_mis_report_instance_period_rel("mis_report_instance_period_id" int4 NOT NULL
	,"account_analytic_tag_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_analytic_tag_mis_report_instance_period_rel');
drop foreign table if exists odoo_account_analytic_tag_mis_report_instance_rel cascade;
create FOREIGN TABLE odoo_account_analytic_tag_mis_report_instance_rel("mis_report_instance_id" int4 NOT NULL
	,"account_analytic_tag_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_analytic_tag_mis_report_instance_rel');
drop foreign table if exists odoo_account_analytic_tag_report_general_ledger_rel cascade;
create FOREIGN TABLE odoo_account_analytic_tag_report_general_ledger_rel("report_general_ledger_id" int4 NOT NULL
	,"account_analytic_tag_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_analytic_tag_report_general_ledger_rel');
drop foreign table if exists odoo_account_analytic_tag_sale_order_line_rel cascade;
create FOREIGN TABLE odoo_account_analytic_tag_sale_order_line_rel("sale_order_line_id" int4 NOT NULL
	,"account_analytic_tag_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_analytic_tag_sale_order_line_rel');
drop foreign table if exists odoo_account_asset cascade;
create FOREIGN TABLE odoo_account_asset("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"code" varchar(32)
	,"purchase_value" float8 NOT NULL
	,"salvage_value" numeric
	,"depreciation_base" numeric
	,"value_residual" numeric
	,"value_depreciated" numeric
	,"note" text
	,"profile_id" int4 NOT NULL
	,"date_start" date NOT NULL
	,"date_remove" date
	,"state" varchar NOT NULL
	,"active" bool
	,"partner_id" int4
	,"method" varchar NOT NULL
	,"method_number" int4
	,"method_period" varchar NOT NULL
	,"method_end" date
	,"method_progress_factor" numeric
	,"method_time" varchar NOT NULL
	,"days_calc" bool
	,"use_leap_years" bool
	,"prorata" bool
	,"company_id" int4 NOT NULL
	,"company_currency_id" int4
	,"account_analytic_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"annual_percentage" numeric
	,"method_percentage" numeric)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_asset');
drop foreign table if exists odoo_account_asset_compute cascade;
create FOREIGN TABLE odoo_account_asset_compute("id" int4 NOT NULL
	,"date_end" date NOT NULL
	,"note" text
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_asset_compute');
drop foreign table if exists odoo_account_asset_group cascade;
create FOREIGN TABLE odoo_account_asset_group("id" int4 NOT NULL
	,"parent_path" varchar
	,"name" varchar(64) NOT NULL
	,"code" varchar
	,"company_id" int4 NOT NULL
	,"parent_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_asset_group');
drop foreign table if exists odoo_account_asset_group_rel cascade;
create FOREIGN TABLE odoo_account_asset_group_rel("asset_id" int4 NOT NULL
	,"group_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_asset_group_rel');
drop foreign table if exists odoo_account_asset_line cascade;
create FOREIGN TABLE odoo_account_asset_line("id" int4 NOT NULL
	,"name" varchar(64)
	,"asset_id" int4 NOT NULL
	,"previous_id" int4
	,"amount" numeric NOT NULL
	,"remaining_value" numeric
	,"depreciated_value" numeric
	,"line_date" date NOT NULL
	,"line_days" int4
	,"move_id" int4
	,"move_check" bool
	,"type" varchar
	,"init_entry" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_asset_line');
drop foreign table if exists odoo_account_asset_profile cascade;
create FOREIGN TABLE odoo_account_asset_profile("id" int4 NOT NULL
	,"name" varchar(64) NOT NULL
	,"note" text
	,"account_analytic_id" int4
	,"account_asset_id" int4 NOT NULL
	,"account_depreciation_id" int4 NOT NULL
	,"account_expense_depreciation_id" int4 NOT NULL
	,"account_plus_value_id" int4
	,"account_min_value_id" int4
	,"account_residual_value_id" int4
	,"journal_id" int4 NOT NULL
	,"company_id" int4 NOT NULL
	,"method" varchar NOT NULL
	,"method_number" int4
	,"method_period" varchar NOT NULL
	,"method_progress_factor" numeric
	,"method_time" varchar NOT NULL
	,"days_calc" bool
	,"use_leap_years" bool
	,"prorata" bool
	,"open_asset" bool
	,"asset_product_item" bool
	,"active" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"annual_percentage" numeric
	,"method_percentage" numeric)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_asset_profile');
drop foreign table if exists odoo_account_asset_profile_group_rel cascade;
create FOREIGN TABLE odoo_account_asset_profile_group_rel("profile_id" int4 NOT NULL
	,"group_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_asset_profile_group_rel');
drop foreign table if exists odoo_account_asset_recompute_trigger cascade;
create FOREIGN TABLE odoo_account_asset_recompute_trigger("id" int4 NOT NULL
	,"reason" varchar NOT NULL
	,"company_id" int4 NOT NULL
	,"date_trigger" timestamp
	,"date_completed" timestamp
	,"state" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_asset_recompute_trigger');
drop foreign table if exists odoo_account_asset_remove cascade;
create FOREIGN TABLE odoo_account_asset_remove("id" int4 NOT NULL
	,"date_remove" date NOT NULL
	,"force_date" date
	,"sale_value" float8
	,"account_sale_id" int4
	,"account_plus_value_id" int4
	,"account_min_value_id" int4
	,"account_residual_value_id" int4
	,"posting_regime" varchar NOT NULL
	,"note" text
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_asset_remove');
drop foreign table if exists odoo_account_asset_report cascade;
create FOREIGN TABLE odoo_account_asset_report("id" int4
	,"depreciation_date" date
	,"date" date
	,"gross_value" float8
	,"depreciation_value" numeric
	,"posted_value" numeric
	,"unposted_value" numeric
	,"asset_id" int4
	,"move_check" bool
	,"asset_profile_id" int4
	,"partner_id" int4
	,"state" varchar
	,"depreciation_count" int8
	,"company_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_asset_report');
drop foreign table if exists odoo_account_bank_statement cascade;
create FOREIGN TABLE odoo_account_bank_statement("id" int4 NOT NULL
	,"message_main_attachment_id" int4
	,"name" varchar
	,"reference" varchar
	,"date" date NOT NULL
	,"date_done" timestamp
	,"balance_start" numeric
	,"balance_end_real" numeric
	,"accounting_date" date
	,"state" varchar NOT NULL
	,"journal_id" int4 NOT NULL
	,"company_id" int4
	,"total_entry_encoding" numeric
	,"balance_end" numeric
	,"difference" numeric
	,"user_id" int4
	,"cashbox_start_id" int4
	,"cashbox_end_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_bank_statement');
drop foreign table if exists odoo_account_bank_statement_cashbox cascade;
create FOREIGN TABLE odoo_account_bank_statement_cashbox("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_bank_statement_cashbox');
drop foreign table if exists odoo_account_bank_statement_closebalance cascade;
create FOREIGN TABLE odoo_account_bank_statement_closebalance("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_bank_statement_closebalance');
drop foreign table if exists odoo_account_bank_statement_import cascade;
create FOREIGN TABLE odoo_account_bank_statement_import("id" int4 NOT NULL
	,"data_file" bytea NOT NULL
	,"filename" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_bank_statement_import');
drop foreign table if exists odoo_account_bank_statement_import_journal_creation cascade;
create FOREIGN TABLE odoo_account_bank_statement_import_journal_creation("id" int4 NOT NULL
	,"journal_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_bank_statement_import_journal_creation');
drop foreign table if exists odoo_account_bank_statement_line cascade;
create FOREIGN TABLE odoo_account_bank_statement_line("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"date" date NOT NULL
	,"amount" numeric
	,"partner_id" int4
	,"account_number" varchar
	,"bank_account_id" int4
	,"account_id" int4
	,"statement_id" int4 NOT NULL
	,"journal_id" int4
	,"partner_name" varchar
	,"ref" varchar
	,"note" text
	,"sequence" int4
	,"company_id" int4
	,"amount_currency" numeric
	,"currency_id" int4
	,"move_name" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"unique_import_id" varchar)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_bank_statement_line');
drop foreign table if exists odoo_account_banking_mandate cascade;
create FOREIGN TABLE odoo_account_banking_mandate("id" int4 NOT NULL
	,"message_main_attachment_id" int4
	,"format" varchar NOT NULL
	,"type" varchar
	,"partner_bank_id" int4
	,"partner_id" int4
	,"company_id" int4 NOT NULL
	,"unique_mandate_reference" varchar
	,"signature_date" date
	,"last_debit_date" date
	,"state" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"recurrent_sequence_type" varchar
	,"scheme" varchar
	,"display_name" varchar)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_banking_mandate');
drop foreign table if exists odoo_account_cash_rounding cascade;
create FOREIGN TABLE odoo_account_cash_rounding("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"rounding" float8 NOT NULL
	,"strategy" varchar NOT NULL
	,"account_id" int4
	,"rounding_method" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_cash_rounding');
drop foreign table if exists odoo_account_cashbox_line cascade;
create FOREIGN TABLE odoo_account_cashbox_line("id" int4 NOT NULL
	,"coin_value" numeric NOT NULL
	,"number" int4
	,"cashbox_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_cashbox_line');
drop foreign table if exists odoo_account_chart_template cascade;
create FOREIGN TABLE odoo_account_chart_template("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"parent_id" int4
	,"code_digits" int4 NOT NULL
	,"visible" bool
	,"currency_id" int4 NOT NULL
	,"use_anglo_saxon" bool
	,"complete_tax_set" bool
	,"bank_account_code_prefix" varchar NOT NULL
	,"cash_account_code_prefix" varchar NOT NULL
	,"transfer_account_code_prefix" varchar NOT NULL
	,"income_currency_exchange_account_id" int4
	,"expense_currency_exchange_account_id" int4
	,"property_account_receivable_id" int4
	,"property_account_payable_id" int4
	,"property_account_expense_categ_id" int4
	,"property_account_income_categ_id" int4
	,"property_account_expense_id" int4
	,"property_account_income_id" int4
	,"property_stock_account_input_categ_id" int4
	,"property_stock_account_output_categ_id" int4
	,"property_stock_valuation_account_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_chart_template');
drop foreign table if exists odoo_account_common_journal_report cascade;
create FOREIGN TABLE odoo_account_common_journal_report("id" int4 NOT NULL
	,"amount_currency" bool
	,"company_id" int4 NOT NULL
	,"date_from" date
	,"date_to" date
	,"target_move" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_common_journal_report');
drop foreign table if exists odoo_account_common_journal_report_account_journal_rel cascade;
create FOREIGN TABLE odoo_account_common_journal_report_account_journal_rel("account_common_journal_report_id" int4 NOT NULL
	,"account_journal_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_common_journal_report_account_journal_rel');
drop foreign table if exists odoo_account_common_report cascade;
create FOREIGN TABLE odoo_account_common_report("id" int4 NOT NULL
	,"company_id" int4 NOT NULL
	,"date_from" date
	,"date_to" date
	,"target_move" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_common_report');
drop foreign table if exists odoo_account_common_report_account_journal_rel cascade;
create FOREIGN TABLE odoo_account_common_report_account_journal_rel("account_common_report_id" int4 NOT NULL
	,"account_journal_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_common_report_account_journal_rel');
drop foreign table if exists odoo_account_financial_year_op cascade;
create FOREIGN TABLE odoo_account_financial_year_op("id" int4 NOT NULL
	,"company_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_financial_year_op');
drop foreign table if exists odoo_account_fiscal_position cascade;
create FOREIGN TABLE odoo_account_fiscal_position("id" int4 NOT NULL
	,"sequence" int4
	,"name" varchar NOT NULL
	,"active" bool
	,"company_id" int4
	,"note" text
	,"auto_apply" bool
	,"vat_required" bool
	,"country_id" int4
	,"country_group_id" int4
	,"zip_from" int4
	,"zip_to" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"aeat_perception_key_id" int4
	,"aeat_perception_subkey_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_fiscal_position');
drop foreign table if exists odoo_account_fiscal_position_account cascade;
create FOREIGN TABLE odoo_account_fiscal_position_account("id" int4 NOT NULL
	,"position_id" int4 NOT NULL
	,"account_src_id" int4 NOT NULL
	,"account_dest_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_fiscal_position_account');
drop foreign table if exists odoo_account_fiscal_position_account_template cascade;
create FOREIGN TABLE odoo_account_fiscal_position_account_template("id" int4 NOT NULL
	,"position_id" int4 NOT NULL
	,"account_src_id" int4 NOT NULL
	,"account_dest_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_fiscal_position_account_template');
drop foreign table if exists odoo_account_fiscal_position_res_country_state_rel cascade;
create FOREIGN TABLE odoo_account_fiscal_position_res_country_state_rel("account_fiscal_position_id" int4 NOT NULL
	,"res_country_state_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_fiscal_position_res_country_state_rel');
drop foreign table if exists odoo_account_fiscal_position_tax cascade;
create FOREIGN TABLE odoo_account_fiscal_position_tax("id" int4 NOT NULL
	,"position_id" int4 NOT NULL
	,"tax_src_id" int4 NOT NULL
	,"tax_dest_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_fiscal_position_tax');
drop foreign table if exists odoo_account_fiscal_position_tax_template cascade;
create FOREIGN TABLE odoo_account_fiscal_position_tax_template("id" int4 NOT NULL
	,"position_id" int4 NOT NULL
	,"tax_src_id" int4 NOT NULL
	,"tax_dest_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_fiscal_position_tax_template');
drop foreign table if exists odoo_account_fiscal_position_template cascade;
create FOREIGN TABLE odoo_account_fiscal_position_template("id" int4 NOT NULL
	,"sequence" int4
	,"name" varchar NOT NULL
	,"chart_template_id" int4 NOT NULL
	,"note" text
	,"auto_apply" bool
	,"vat_required" bool
	,"country_id" int4
	,"country_group_id" int4
	,"zip_from" int4
	,"zip_to" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_fiscal_position_template');
drop foreign table if exists odoo_account_fiscal_position_template_res_country_state_rel cascade;
create FOREIGN TABLE odoo_account_fiscal_position_template_res_country_state_rel("account_fiscal_position_template_id" int4 NOT NULL
	,"res_country_state_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_fiscal_position_template_res_country_state_rel');
drop foreign table if exists odoo_account_fiscal_year cascade;
create FOREIGN TABLE odoo_account_fiscal_year("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"date_from" date NOT NULL
	,"date_to" date NOT NULL
	,"company_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_fiscal_year');
drop foreign table if exists odoo_account_full_reconcile cascade;
create FOREIGN TABLE odoo_account_full_reconcile("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"exchange_move_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_full_reconcile');
drop foreign table if exists odoo_account_group cascade;
create FOREIGN TABLE odoo_account_group("id" int4 NOT NULL
	,"parent_path" varchar
	,"parent_id" int4
	,"name" varchar NOT NULL
	,"code_prefix" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"level" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_group');
drop foreign table if exists odoo_account_incoterms cascade;
create FOREIGN TABLE odoo_account_incoterms("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"code" varchar(3) NOT NULL
	,"active" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_incoterms');
drop foreign table if exists odoo_account_invoice cascade;
create FOREIGN TABLE odoo_account_invoice("id" int4 NOT NULL
	,"message_main_attachment_id" int4
	,"access_token" varchar
	,"name" varchar
	,"origin" varchar
	,"type" varchar
	,"refund_invoice_id" int4
	,"number" varchar
	,"move_name" varchar
	,"reference" varchar
	,"comment" text
	,"state" varchar
	,"sent" bool
	,"date_invoice" date
	,"date_due" date
	,"partner_id" int4
	,"vendor_bill_id" int4
	,"payment_term_id" int4
	,"date" date
	,"account_id" int4
	,"move_id" int4
	,"amount_untaxed" numeric
	,"amount_untaxed_signed" numeric
	,"amount_tax" numeric
	,"amount_total" numeric
	,"amount_total_signed" numeric
	,"amount_total_company_signed" numeric
	,"currency_id" int4 NOT NULL
	,"journal_id" int4 NOT NULL
	,"company_id" int4 NOT NULL
	,"reconciled" bool
	,"partner_bank_id" int4
	,"residual" numeric
	,"residual_signed" numeric
	,"residual_company_signed" numeric
	,"user_id" int4
	,"fiscal_position_id" int4
	,"commercial_partner_id" int4
	,"cash_rounding_id" int4
	,"incoterm_id" int4
	,"source_email" varchar
	,"vendor_display_name" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"returned_payment" bool
	,"old_contract_id" int4
	,"payment_mode_id" int4
	,"team_id" int4
	,"partner_shipping_id" int4
	,"reference_type" varchar NOT NULL
	,"subscription_request" int4
	,"release_capital_request" bool
	,"incoterms_id" int4
	,"mandate_id" int4
	,"_api_external_id" int4
	,"external_id_sequence_id" int4
	,"aeat_perception_key_id" int4
	,"aeat_perception_subkey_id" int4
	,"eu_triangular_deal" bool
	,"campaign_id" int4
	,"source_id" int4
	,"medium_id" int4
	,"oc_taxes" varchar
	,"oc_total" float8
	,"oc_untaxed" float8
	,"oc_total_taxed" float8)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_invoice');
drop foreign table if exists odoo_account_invoice_account_invoice_send_rel cascade;
create FOREIGN TABLE odoo_account_invoice_account_invoice_send_rel("account_invoice_send_id" int4 NOT NULL
	,"account_invoice_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_invoice_account_invoice_send_rel');
drop foreign table if exists odoo_account_invoice_account_move_line_rel cascade;
create FOREIGN TABLE odoo_account_invoice_account_move_line_rel("account_invoice_id" int4 NOT NULL
	,"account_move_line_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_invoice_account_move_line_rel');
drop foreign table if exists odoo_account_invoice_account_register_payments_rel cascade;
create FOREIGN TABLE odoo_account_invoice_account_register_payments_rel("account_register_payments_id" int4 NOT NULL
	,"account_invoice_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_invoice_account_register_payments_rel');
drop foreign table if exists odoo_account_invoice_confirm cascade;
create FOREIGN TABLE odoo_account_invoice_confirm("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_invoice_confirm');
drop foreign table if exists odoo_account_invoice_import_wizard cascade;
create FOREIGN TABLE odoo_account_invoice_import_wizard("id" int4 NOT NULL
	,"journal_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_invoice_import_wizard');
drop foreign table if exists odoo_account_invoice_import_wizard_ir_attachment_rel cascade;
create FOREIGN TABLE odoo_account_invoice_import_wizard_ir_attachment_rel("account_invoice_import_wizard_id" int4 NOT NULL
	,"ir_attachment_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_invoice_import_wizard_ir_attachment_rel');
drop foreign table if exists odoo_account_invoice_invoice_claim_1_send_wizard_rel cascade;
create FOREIGN TABLE odoo_account_invoice_invoice_claim_1_send_wizard_rel("invoice_claim_1_send_wizard_id" int4 NOT NULL
	,"account_invoice_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_invoice_invoice_claim_1_send_wizard_rel');
drop foreign table if exists odoo_account_invoice_line cascade;
create FOREIGN TABLE odoo_account_invoice_line("id" int4 NOT NULL
	,"name" text NOT NULL
	,"origin" varchar
	,"sequence" int4
	,"invoice_id" int4
	,"uom_id" int4
	,"product_id" int4
	,"account_id" int4
	,"price_unit" numeric
	,"price_subtotal" numeric
	,"price_total" numeric
	,"price_subtotal_signed" numeric
	,"quantity" numeric
	,"discount" numeric
	,"account_analytic_id" int4
	,"company_id" int4
	,"partner_id" int4
	,"currency_id" int4
	,"is_rounding_line" bool
	,"display_type" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"asset_profile_id" int4
	,"asset_id" int4
	,"contract_line_id" int4
	,"oc_amount_untaxed" float8
	,"oc_amount_total" float8
	,"oc_amount_taxes" float8)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_invoice_line');
drop foreign table if exists odoo_account_invoice_line_tax cascade;
create FOREIGN TABLE odoo_account_invoice_line_tax("invoice_line_id" int4 NOT NULL
	,"tax_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_invoice_line_tax');
drop foreign table if exists odoo_account_invoice_payment_line_multi cascade;
create FOREIGN TABLE odoo_account_invoice_payment_line_multi("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_invoice_payment_line_multi');
drop foreign table if exists odoo_account_invoice_payment_rel cascade;
create FOREIGN TABLE odoo_account_invoice_payment_rel("payment_id" int4 NOT NULL
	,"invoice_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_invoice_payment_rel');
drop foreign table if exists odoo_account_invoice_queue_job_rel cascade;
create FOREIGN TABLE odoo_account_invoice_queue_job_rel("invoice_id" int4 NOT NULL
	,"job_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_invoice_queue_job_rel');
drop foreign table if exists odoo_account_invoice_refund cascade;
create FOREIGN TABLE odoo_account_invoice_refund("id" int4 NOT NULL
	,"date_invoice" date NOT NULL
	,"date" date
	,"description" varchar NOT NULL
	,"filter_refund" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_invoice_refund');
drop foreign table if exists odoo_account_invoice_report cascade;
create FOREIGN TABLE odoo_account_invoice_report("id" int4
	,"number" varchar
	,"date" date
	,"product_id" int4
	,"partner_id" int4
	,"country_id" int4
	,"account_analytic_id" int4
	,"payment_term_id" int4
	,"uom_name" varchar
	,"currency_id" int4
	,"journal_id" int4
	,"fiscal_position_id" int4
	,"user_id" int4
	,"company_id" int4
	,"nbr" int4
	,"invoice_id" int4
	,"type" varchar
	,"state" varchar
	,"categ_id" int4
	,"date_due" date
	,"account_id" int4
	,"account_line_id" int4
	,"partner_bank_id" int4
	,"product_qty" numeric
	,"price_total" numeric
	,"price_average" numeric
	,"amount_total" numeric
	,"currency_rate" numeric
	,"residual" numeric
	,"commercial_partner_id" int4
	,"team_id" int4
	,"release_capital_request" bool)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_invoice_report');
drop foreign table if exists odoo_account_invoice_send cascade;
create FOREIGN TABLE odoo_account_invoice_send("id" int4 NOT NULL
	,"is_email" bool
	,"is_print" bool
	,"printed" bool
	,"composer_id" int4 NOT NULL
	,"template_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_invoice_send');
drop foreign table if exists odoo_account_invoice_tax cascade;
create FOREIGN TABLE odoo_account_invoice_tax("id" int4 NOT NULL
	,"invoice_id" int4
	,"name" varchar NOT NULL
	,"tax_id" int4
	,"account_id" int4 NOT NULL
	,"account_analytic_id" int4
	,"amount" numeric
	,"amount_rounding" numeric
	,"manual" bool
	,"sequence" int4
	,"company_id" int4
	,"currency_id" int4
	,"base" numeric
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_invoice_tax');
drop foreign table if exists odoo_account_invoice_transaction_rel cascade;
create FOREIGN TABLE odoo_account_invoice_transaction_rel("transaction_id" int4 NOT NULL
	,"invoice_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_invoice_transaction_rel');
drop foreign table if exists odoo_account_journal cascade;
create FOREIGN TABLE odoo_account_journal("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"code" varchar(5) NOT NULL
	,"active" bool
	,"type" varchar NOT NULL
	,"default_credit_account_id" int4
	,"default_debit_account_id" int4
	,"update_posted" bool
	,"group_invoice_lines" bool
	,"sequence_id" int4 NOT NULL
	,"refund_sequence_id" int4
	,"sequence" int4
	,"currency_id" int4
	,"company_id" int4 NOT NULL
	,"refund_sequence" bool
	,"at_least_one_inbound" bool
	,"at_least_one_outbound" bool
	,"profit_account_id" int4
	,"loss_account_id" int4
	,"bank_account_id" int4
	,"bank_statements_source" varchar
	,"post_at_bank_rec" bool
	,"alias_id" int4
	,"show_on_dashboard" bool
	,"color" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"default_expense_account_id" int4
	,"default_expense_partner_id" int4
	,"return_auto_reconcile" bool
	,"n43_date_type" varchar
	,"inbound_payment_order_only" bool
	,"outbound_payment_order_only" bool
	,"get_cooperator_payment" bool
	,"get_general_payment" bool
	,"_api_external_id" int4
	,"external_id_sequence_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_journal');
drop foreign table if exists odoo_account_journal_account_payment_line_create_rel cascade;
create FOREIGN TABLE odoo_account_journal_account_payment_line_create_rel("account_payment_line_create_id" int4 NOT NULL
	,"account_journal_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_journal_account_payment_line_create_rel');
drop foreign table if exists odoo_account_journal_account_payment_mode_rel cascade;
create FOREIGN TABLE odoo_account_journal_account_payment_mode_rel("account_payment_mode_id" int4 NOT NULL
	,"account_journal_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_journal_account_payment_mode_rel');
drop foreign table if exists odoo_account_journal_account_print_journal_rel cascade;
create FOREIGN TABLE odoo_account_journal_account_print_journal_rel("account_print_journal_id" int4 NOT NULL
	,"account_journal_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_journal_account_print_journal_rel');
drop foreign table if exists odoo_account_journal_account_reconcile_model_rel cascade;
create FOREIGN TABLE odoo_account_journal_account_reconcile_model_rel("account_reconcile_model_id" int4 NOT NULL
	,"account_journal_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_journal_account_reconcile_model_rel');
drop foreign table if exists odoo_account_journal_account_reconcile_model_template_rel cascade;
create FOREIGN TABLE odoo_account_journal_account_reconcile_model_template_rel("account_reconcile_model_template_id" int4 NOT NULL
	,"account_journal_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_journal_account_reconcile_model_template_rel');
drop foreign table if exists odoo_account_journal_general_ledger_report_wizard_rel cascade;
create FOREIGN TABLE odoo_account_journal_general_ledger_report_wizard_rel("general_ledger_report_wizard_id" int4 NOT NULL
	,"account_journal_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_journal_general_ledger_report_wizard_rel');
drop foreign table if exists odoo_account_journal_inbound_payment_method_rel cascade;
create FOREIGN TABLE odoo_account_journal_inbound_payment_method_rel("journal_id" int4 NOT NULL
	,"inbound_payment_method" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_journal_inbound_payment_method_rel');
drop foreign table if exists odoo_account_journal_journal_ledger_report_wizard_rel cascade;
create FOREIGN TABLE odoo_account_journal_journal_ledger_report_wizard_rel("journal_ledger_report_wizard_id" int4 NOT NULL
	,"account_journal_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_journal_journal_ledger_report_wizard_rel');
drop foreign table if exists odoo_account_journal_outbound_payment_method_rel cascade;
create FOREIGN TABLE odoo_account_journal_outbound_payment_method_rel("journal_id" int4 NOT NULL
	,"outbound_payment_method" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_journal_outbound_payment_method_rel');
drop foreign table if exists odoo_account_journal_report_general_ledger_rel cascade;
create FOREIGN TABLE odoo_account_journal_report_general_ledger_rel("report_general_ledger_id" int4 NOT NULL
	,"account_journal_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_journal_report_general_ledger_rel');
drop foreign table if exists odoo_account_journal_report_journal_ledger_rel cascade;
create FOREIGN TABLE odoo_account_journal_report_journal_ledger_rel("report_journal_ledger_id" int4 NOT NULL
	,"account_journal_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_journal_report_journal_ledger_rel');
drop foreign table if exists odoo_account_journal_report_trial_balance_rel cascade;
create FOREIGN TABLE odoo_account_journal_report_trial_balance_rel("report_trial_balance_id" int4 NOT NULL
	,"account_journal_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_journal_report_trial_balance_rel');
drop foreign table if exists odoo_account_journal_trial_balance_report_wizard_rel cascade;
create FOREIGN TABLE odoo_account_journal_trial_balance_report_wizard_rel("trial_balance_report_wizard_id" int4 NOT NULL
	,"account_journal_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_journal_trial_balance_report_wizard_rel');
drop foreign table if exists odoo_account_journal_type_rel cascade;
create FOREIGN TABLE odoo_account_journal_type_rel("journal_id" int4 NOT NULL
	,"type_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_journal_type_rel');
drop foreign table if exists odoo_account_move cascade;
create FOREIGN TABLE odoo_account_move("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"ref" varchar
	,"date" date NOT NULL
	,"journal_id" int4 NOT NULL
	,"currency_id" int4
	,"state" varchar NOT NULL
	,"partner_id" int4
	,"amount" numeric
	,"narration" text
	,"company_id" int4
	,"matched_percentage" numeric
	,"tax_cash_basis_rec_id" int4
	,"auto_reverse" bool
	,"reverse_date" date
	,"reverse_entry_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"move_type" varchar
	,"stock_move_id" int4
	,"payment_order_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_move');
drop foreign table if exists odoo_account_move_line cascade;
create FOREIGN TABLE odoo_account_move_line("id" int4 NOT NULL
	,"name" varchar
	,"quantity" numeric
	,"product_uom_id" int4
	,"product_id" int4
	,"debit" numeric
	,"credit" numeric
	,"balance" numeric
	,"debit_cash_basis" numeric
	,"credit_cash_basis" numeric
	,"balance_cash_basis" numeric
	,"amount_currency" numeric
	,"company_currency_id" int4
	,"currency_id" int4
	,"amount_residual" numeric
	,"amount_residual_currency" numeric
	,"tax_base_amount" numeric
	,"account_id" int4 NOT NULL
	,"move_id" int4 NOT NULL
	,"ref" varchar
	,"payment_id" int4
	,"statement_line_id" int4
	,"statement_id" int4
	,"reconciled" bool
	,"full_reconcile_id" int4
	,"journal_id" int4
	,"blocked" bool
	,"date_maturity" date NOT NULL
	,"date" date
	,"tax_line_id" int4
	,"analytic_account_id" int4
	,"company_id" int4
	,"invoice_id" int4
	,"partner_id" int4
	,"user_type_id" int4
	,"tax_exigible" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"asset_profile_id" int4
	,"asset_id" int4
	,"stored_invoice_id" int4
	,"invoice_user_id" int4
	,"payment_mode_id" int4
	,"partner_bank_id" int4
	,"bank_payment_line_id" int4
	,"mandate_id" int4
	,"aeat_perception_key_id" int4
	,"aeat_perception_subkey_id" int4
	,"l10n_es_aeat_349_operation_key" varchar)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_move_line');
drop foreign table if exists odoo_account_move_line_account_payment_line_create_rel cascade;
create FOREIGN TABLE odoo_account_move_line_account_payment_line_create_rel("account_payment_line_create_id" int4 NOT NULL
	,"account_move_line_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_move_line_account_payment_line_create_rel');
drop foreign table if exists odoo_account_move_line_account_tax_rel cascade;
create FOREIGN TABLE odoo_account_move_line_account_tax_rel("account_move_line_id" int4 NOT NULL
	,"account_tax_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_move_line_account_tax_rel');
drop foreign table if exists odoo_account_move_line_l10n_es_aeat_tax_line_rel cascade;
create FOREIGN TABLE odoo_account_move_line_l10n_es_aeat_tax_line_rel("l10n_es_aeat_tax_line_id" int4 NOT NULL
	,"account_move_line_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_move_line_l10n_es_aeat_tax_line_rel');
drop foreign table if exists odoo_account_move_line_payment_return_line_rel cascade;
create FOREIGN TABLE odoo_account_move_line_payment_return_line_rel("payment_return_line_id" int4 NOT NULL
	,"account_move_line_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_move_line_payment_return_line_rel');
drop foreign table if exists odoo_account_move_reversal cascade;
create FOREIGN TABLE odoo_account_move_reversal("id" int4 NOT NULL
	,"date" date NOT NULL
	,"journal_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_move_reversal');
drop foreign table if exists odoo_account_partial_reconcile cascade;
create FOREIGN TABLE odoo_account_partial_reconcile("id" int4 NOT NULL
	,"debit_move_id" int4 NOT NULL
	,"credit_move_id" int4 NOT NULL
	,"amount" numeric
	,"amount_currency" numeric
	,"currency_id" int4
	,"company_id" int4
	,"full_reconcile_id" int4
	,"max_date" date
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_partial_reconcile');
drop foreign table if exists odoo_account_partial_reconcile_account_move_line_rel cascade;
create FOREIGN TABLE odoo_account_partial_reconcile_account_move_line_rel("partial_reconcile_id" int4 NOT NULL
	,"move_line_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_partial_reconcile_account_move_line_rel');
drop foreign table if exists odoo_account_payment cascade;
create FOREIGN TABLE odoo_account_payment("id" int4 NOT NULL
	,"message_main_attachment_id" int4
	,"name" varchar
	,"state" varchar
	,"payment_type" varchar NOT NULL
	,"payment_reference" varchar
	,"move_name" varchar
	,"destination_journal_id" int4
	,"multi" bool
	,"payment_method_id" int4 NOT NULL
	,"partner_type" varchar
	,"partner_id" int4
	,"amount" numeric NOT NULL
	,"currency_id" int4 NOT NULL
	,"payment_date" date NOT NULL
	,"communication" varchar
	,"journal_id" int4 NOT NULL
	,"payment_difference_handling" varchar
	,"writeoff_account_id" int4
	,"writeoff_label" varchar
	,"partner_bank_account_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"payment_transaction_id" int4
	,"payment_token_id" int4
	,"_api_external_id" int4
	,"external_id_sequence_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_payment');
drop foreign table if exists odoo_account_payment_line cascade;
create FOREIGN TABLE odoo_account_payment_line("id" int4 NOT NULL
	,"name" varchar
	,"order_id" int4
	,"company_id" int4
	,"company_currency_id" int4
	,"payment_type" varchar
	,"state" varchar
	,"move_line_id" int4
	,"currency_id" int4 NOT NULL
	,"amount_currency" numeric
	,"partner_id" int4 NOT NULL
	,"partner_bank_id" int4
	,"date" date
	,"communication" varchar NOT NULL
	,"communication_type" varchar NOT NULL
	,"bank_line_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"mandate_id" int4
	,"priority" varchar
	,"local_instrument" varchar
	,"category_purpose" varchar
	,"purpose" varchar)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_payment_line');
drop foreign table if exists odoo_account_payment_line_create cascade;
create FOREIGN TABLE odoo_account_payment_line_create("id" int4 NOT NULL
	,"order_id" int4
	,"target_move" varchar
	,"allow_blocked" bool
	,"invoice" bool
	,"date_type" varchar NOT NULL
	,"due_date" date
	,"move_date" date
	,"payment_mode" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"limit_enabled" bool
	,"limit" int4
	,"queue_enabled" bool
	,"due_date_from" date
	,"move_date_from" date)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_payment_line_create');
drop foreign table if exists odoo_account_payment_line_create_res_partner_rel cascade;
create FOREIGN TABLE odoo_account_payment_line_create_res_partner_rel("account_payment_line_create_id" int4 NOT NULL
	,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_payment_line_create_res_partner_rel');
drop foreign table if exists odoo_account_payment_method cascade;
create FOREIGN TABLE odoo_account_payment_method("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"code" varchar NOT NULL
	,"payment_type" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"active" bool
	,"bank_account_required" bool
	,"payment_order_only" bool
	,"mandate_required" bool
	,"pain_version" varchar
	,"convert_to_ascii" bool)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_payment_method');
drop foreign table if exists odoo_account_payment_mode cascade;
create FOREIGN TABLE odoo_account_payment_mode("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"company_id" int4 NOT NULL
	,"bank_account_link" varchar NOT NULL
	,"fixed_journal_id" int4
	,"payment_method_id" int4 NOT NULL
	,"payment_type" varchar
	,"payment_method_code" varchar
	,"active" bool
	,"note" text
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"show_bank_account" varchar
	,"show_bank_account_from_journal" bool
	,"show_bank_account_chars" int4
	,"payment_order_ok" bool
	,"no_debit_before_maturity" bool
	,"default_payment_mode" varchar
	,"default_invoice" bool
	,"default_target_move" varchar
	,"default_date_type" varchar
	,"default_date_prefered" varchar
	,"group_lines" bool
	,"generate_move" bool
	,"offsetting_account" varchar
	,"transfer_account_id" int4
	,"transfer_journal_id" int4
	,"move_option" varchar
	,"post_move" bool
	,"initiating_party_issuer" varchar(35)
	,"initiating_party_identifier" varchar(35)
	,"initiating_party_scheme" varchar(35)
	,"sepa_creditor_identifier" varchar(35))SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_payment_mode');
drop foreign table if exists odoo_account_payment_mode_variable_journal_rel cascade;
create FOREIGN TABLE odoo_account_payment_mode_variable_journal_rel("payment_mode_id" int4 NOT NULL
	,"journal_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_payment_mode_variable_journal_rel');
drop foreign table if exists odoo_account_payment_order cascade;
create FOREIGN TABLE odoo_account_payment_order("id" int4 NOT NULL
	,"message_main_attachment_id" int4
	,"name" varchar
	,"payment_mode_id" int4 NOT NULL
	,"payment_type" varchar NOT NULL
	,"payment_method_id" int4
	,"company_id" int4
	,"company_currency_id" int4
	,"journal_id" int4
	,"state" varchar
	,"date_prefered" varchar NOT NULL
	,"date_scheduled" date
	,"date_generated" date
	,"date_uploaded" date
	,"date_done" date
	,"generated_user_id" int4
	,"total_company_currency" numeric
	,"description" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"charge_bearer" varchar
	,"batch_booking" bool)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_payment_order');
drop foreign table if exists odoo_account_payment_order_queue_job_rel cascade;
create FOREIGN TABLE odoo_account_payment_order_queue_job_rel("account_payment_order_id" int4 NOT NULL
	,"job_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_payment_order_queue_job_rel');
drop foreign table if exists odoo_account_payment_return_gateway cascade;
create FOREIGN TABLE odoo_account_payment_return_gateway("id" int4 NOT NULL
	,"message_main_attachment_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_payment_return_gateway');
drop foreign table if exists odoo_account_payment_term cascade;
create FOREIGN TABLE odoo_account_payment_term("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"active" bool
	,"note" text
	,"company_id" int4 NOT NULL
	,"sequence" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_payment_term');
drop foreign table if exists odoo_account_payment_term_line cascade;
create FOREIGN TABLE odoo_account_payment_term_line("id" int4 NOT NULL
	,"value" varchar NOT NULL
	,"value_amount" numeric
	,"days" int4 NOT NULL
	,"day_of_the_month" int4
	,"option" varchar NOT NULL
	,"payment_id" int4 NOT NULL
	,"sequence" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_payment_term_line');
drop foreign table if exists odoo_account_print_journal cascade;
create FOREIGN TABLE odoo_account_print_journal("id" int4 NOT NULL
	,"sort_selection" varchar NOT NULL
	,"amount_currency" bool
	,"company_id" int4 NOT NULL
	,"date_from" date
	,"date_to" date
	,"target_move" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_print_journal');
drop foreign table if exists odoo_account_reconcile_model cascade;
create FOREIGN TABLE odoo_account_reconcile_model("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"sequence" int4 NOT NULL
	,"company_id" int4 NOT NULL
	,"rule_type" varchar NOT NULL
	,"auto_reconcile" bool
	,"match_nature" varchar NOT NULL
	,"match_amount" varchar
	,"match_amount_min" float8
	,"match_amount_max" float8
	,"match_label" varchar
	,"match_label_param" varchar
	,"match_same_currency" bool
	,"match_total_amount" bool
	,"match_total_amount_param" float8
	,"match_partner" bool
	,"account_id" int4
	,"journal_id" int4
	,"label" varchar
	,"amount_type" varchar NOT NULL
	,"force_tax_included" bool
	,"amount" numeric NOT NULL
	,"tax_id" int4
	,"analytic_account_id" int4
	,"has_second_line" bool
	,"second_account_id" int4
	,"second_journal_id" int4
	,"second_label" varchar
	,"second_amount_type" varchar NOT NULL
	,"force_second_tax_included" bool
	,"second_amount" numeric NOT NULL
	,"second_tax_id" int4
	,"second_analytic_account_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_reconcile_model');
drop foreign table if exists odoo_account_reconcile_model_res_partner_category_rel cascade;
create FOREIGN TABLE odoo_account_reconcile_model_res_partner_category_rel("account_reconcile_model_id" int4 NOT NULL
	,"res_partner_category_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_reconcile_model_res_partner_category_rel');
drop foreign table if exists odoo_account_reconcile_model_res_partner_rel cascade;
create FOREIGN TABLE odoo_account_reconcile_model_res_partner_rel("account_reconcile_model_id" int4 NOT NULL
	,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_reconcile_model_res_partner_rel');
drop foreign table if exists odoo_account_reconcile_model_template cascade;
create FOREIGN TABLE odoo_account_reconcile_model_template("id" int4 NOT NULL
	,"chart_template_id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"sequence" int4 NOT NULL
	,"rule_type" varchar NOT NULL
	,"auto_reconcile" bool
	,"match_nature" varchar NOT NULL
	,"match_amount" varchar
	,"match_amount_min" float8
	,"match_amount_max" float8
	,"match_label" varchar
	,"match_label_param" varchar
	,"match_same_currency" bool
	,"match_total_amount" bool
	,"match_total_amount_param" float8
	,"match_partner" bool
	,"account_id" int4
	,"label" varchar
	,"amount_type" varchar NOT NULL
	,"amount" numeric NOT NULL
	,"force_tax_included" bool
	,"tax_id" int4
	,"has_second_line" bool
	,"second_account_id" int4
	,"second_label" varchar
	,"second_amount_type" varchar NOT NULL
	,"second_amount" numeric NOT NULL
	,"force_second_tax_included" bool
	,"second_tax_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_reconcile_model_template');
drop foreign table if exists odoo_account_reconcile_model_template_res_partner_category_rel cascade;
create FOREIGN TABLE odoo_account_reconcile_model_template_res_partner_category_rel("account_reconcile_model_template_id" int4 NOT NULL
	,"res_partner_category_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_reconcile_model_template_res_partner_category_rel');
drop foreign table if exists odoo_account_reconcile_model_template_res_partner_rel cascade;
create FOREIGN TABLE odoo_account_reconcile_model_template_res_partner_rel("account_reconcile_model_template_id" int4 NOT NULL
	,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_reconcile_model_template_res_partner_rel');
drop foreign table if exists odoo_account_register_payments cascade;
create FOREIGN TABLE odoo_account_register_payments("id" int4 NOT NULL
	,"group_invoices" bool
	,"multi" bool
	,"payment_type" varchar NOT NULL
	,"payment_method_id" int4 NOT NULL
	,"partner_type" varchar
	,"partner_id" int4
	,"amount" numeric NOT NULL
	,"currency_id" int4 NOT NULL
	,"payment_date" date NOT NULL
	,"communication" varchar
	,"journal_id" int4 NOT NULL
	,"payment_difference_handling" varchar
	,"writeoff_account_id" int4
	,"writeoff_label" varchar
	,"partner_bank_account_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_register_payments');
drop foreign table if exists odoo_account_setup_bank_manual_config cascade;
create FOREIGN TABLE odoo_account_setup_bank_manual_config("id" int4 NOT NULL
	,"res_partner_bank_id" int4 NOT NULL
	,"create_or_link_option" varchar
	,"new_journal_code" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_setup_bank_manual_config');
drop foreign table if exists odoo_account_tax cascade;
create FOREIGN TABLE odoo_account_tax("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"type_tax_use" varchar NOT NULL
	,"amount_type" varchar NOT NULL
	,"active" bool
	,"company_id" int4 NOT NULL
	,"sequence" int4 NOT NULL
	,"amount" numeric NOT NULL
	,"account_id" int4
	,"refund_account_id" int4
	,"description" varchar
	,"price_include" bool
	,"include_base_amount" bool
	,"analytic" bool
	,"tax_group_id" int4 NOT NULL
	,"tax_exigibility" varchar
	,"cash_basis_account_id" int4
	,"cash_basis_base_account_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"oc_code" varchar)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_tax');
drop foreign table if exists odoo_account_tax_account_tag cascade;
create FOREIGN TABLE odoo_account_tax_account_tag("account_tax_id" int4 NOT NULL
	,"account_account_tag_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_tax_account_tag');
drop foreign table if exists odoo_account_tax_filiation_rel cascade;
create FOREIGN TABLE odoo_account_tax_filiation_rel("parent_tax" int4 NOT NULL
	,"child_tax" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_tax_filiation_rel');
drop foreign table if exists odoo_account_tax_group cascade;
create FOREIGN TABLE odoo_account_tax_group("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"sequence" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_tax_group');
drop foreign table if exists odoo_account_tax_sale_advance_payment_inv_rel cascade;
create FOREIGN TABLE odoo_account_tax_sale_advance_payment_inv_rel("sale_advance_payment_inv_id" int4 NOT NULL
	,"account_tax_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_tax_sale_advance_payment_inv_rel');
drop foreign table if exists odoo_account_tax_sale_order_line_rel cascade;
create FOREIGN TABLE odoo_account_tax_sale_order_line_rel("sale_order_line_id" int4 NOT NULL
	,"account_tax_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_tax_sale_order_line_rel');
drop foreign table if exists odoo_account_tax_template cascade;
create FOREIGN TABLE odoo_account_tax_template("id" int4 NOT NULL
	,"chart_template_id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"type_tax_use" varchar NOT NULL
	,"amount_type" varchar NOT NULL
	,"active" bool
	,"sequence" int4 NOT NULL
	,"amount" numeric NOT NULL
	,"account_id" int4
	,"refund_account_id" int4
	,"description" varchar
	,"price_include" bool
	,"include_base_amount" bool
	,"analytic" bool
	,"tax_group_id" int4
	,"tax_exigibility" varchar
	,"cash_basis_account_id" int4
	,"cash_basis_base_account_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"aeat_349_map_line" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_tax_template');
drop foreign table if exists odoo_account_tax_template_filiation_rel cascade;
create FOREIGN TABLE odoo_account_tax_template_filiation_rel("parent_tax" int4 NOT NULL
	,"child_tax" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_tax_template_filiation_rel');
drop foreign table if exists odoo_account_tax_template_l10n_es_aeat_map_tax_line_rel cascade;
create FOREIGN TABLE odoo_account_tax_template_l10n_es_aeat_map_tax_line_rel("l10n_es_aeat_map_tax_line_id" int4 NOT NULL
	,"account_tax_template_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_tax_template_l10n_es_aeat_map_tax_line_rel');
drop foreign table if exists odoo_account_unreconcile cascade;
create FOREIGN TABLE odoo_account_unreconcile("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'account_unreconcile');
drop foreign table if exists odoo_add_mis_report_instance_dashboard_wizard cascade;
create FOREIGN TABLE odoo_add_mis_report_instance_dashboard_wizard("id" int4 NOT NULL
	,"name" varchar(32) NOT NULL
	,"dashboard_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'add_mis_report_instance_dashboard_wizard');
drop foreign table if exists odoo_adsl_service_contract_info cascade;
create FOREIGN TABLE odoo_adsl_service_contract_info("id" int4 NOT NULL
	,"administrative_number" varchar NOT NULL
	,"router_product_id" int4 NOT NULL
	,"router_lot_id" int4 NOT NULL
	,"ppp_user" varchar NOT NULL
	,"ppp_password" varchar NOT NULL
	,"endpoint_user" varchar NOT NULL
	,"endpoint_password" varchar NOT NULL
	,"phone_number" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"id_order" varchar
	,"previous_id" varchar)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'adsl_service_contract_info');
drop foreign table if exists odoo_aeat_349_map_line cascade;
create FOREIGN TABLE odoo_aeat_349_map_line("id" int4 NOT NULL
	,"physical_product" bool
	,"operation_key" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'aeat_349_map_line');
drop foreign table if exists odoo_aeat_model_export_config cascade;
create FOREIGN TABLE odoo_aeat_model_export_config("id" int4 NOT NULL
	,"name" varchar
	,"model_number" varchar(3)
	,"model_id" int4
	,"active" bool
	,"date_start" date
	,"date_end" date
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'aeat_model_export_config');
drop foreign table if exists odoo_aeat_model_export_config_line cascade;
create FOREIGN TABLE odoo_aeat_model_export_config_line("id" int4 NOT NULL
	,"sequence" int4
	,"export_config_id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"repeat_expression" varchar
	,"repeat" bool
	,"conditional_expression" varchar
	,"conditional" bool
	,"subconfig_id" int4
	,"export_type" varchar NOT NULL
	,"apply_sign" bool
	,"positive_sign" varchar(1)
	,"negative_sign" varchar(1)
	,"size" int4
	,"alignment" varchar
	,"bool_no" varchar(1)
	,"bool_yes" varchar(1)
	,"decimal_size" int4
	,"expression" varchar
	,"fixed_value" varchar
	,"value" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'aeat_model_export_config_line');
drop foreign table if exists odoo_aeat_tax_agency cascade;
create FOREIGN TABLE odoo_aeat_tax_agency("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'aeat_tax_agency');
drop foreign table if exists odoo_aged_partner_balance_wizard cascade;
create FOREIGN TABLE odoo_aged_partner_balance_wizard("id" int4 NOT NULL
	,"company_id" int4
	,"date_at" date NOT NULL
	,"target_move" varchar NOT NULL
	,"receivable_accounts_only" bool
	,"payable_accounts_only" bool
	,"show_move_line_details" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"group_by_select" varchar)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'aged_partner_balance_wizard');
drop foreign table if exists odoo_aged_partner_balance_wizard_res_partner_rel cascade;
create FOREIGN TABLE odoo_aged_partner_balance_wizard_res_partner_rel("aged_partner_balance_wizard_id" int4 NOT NULL
	,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'aged_partner_balance_wizard_res_partner_rel');
drop foreign table if exists odoo_auditlog_autovacuum cascade;
create FOREIGN TABLE odoo_auditlog_autovacuum("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'auditlog_autovacuum');
drop foreign table if exists odoo_auditlog_http_request cascade;
create FOREIGN TABLE odoo_auditlog_http_request("id" int4 NOT NULL
	,"display_name" varchar
	,"name" varchar
	,"root_url" varchar
	,"user_id" int4
	,"http_session_id" int4
	,"user_context" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'auditlog_http_request');
drop foreign table if exists odoo_auditlog_http_session cascade;
create FOREIGN TABLE odoo_auditlog_http_session("id" int4 NOT NULL
	,"display_name" varchar
	,"name" varchar
	,"user_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'auditlog_http_session');
drop foreign table if exists odoo_auditlog_log cascade;
create FOREIGN TABLE odoo_auditlog_log("id" int4 NOT NULL
	,"name" varchar(64)
	,"model_id" int4
	,"res_id" int4
	,"user_id" int4
	,"method" varchar(64)
	,"http_session_id" int4
	,"http_request_id" int4
	,"log_type" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'auditlog_log');
drop foreign table if exists odoo_auditlog_log_line cascade;
create FOREIGN TABLE odoo_auditlog_log_line("id" int4 NOT NULL
	,"field_id" int4 NOT NULL
	,"log_id" int4
	,"old_value" text
	,"new_value" text
	,"old_value_text" text
	,"new_value_text" text
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'auditlog_log_line');
drop foreign table if exists odoo_auditlog_rule cascade;
create FOREIGN TABLE odoo_auditlog_rule("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"model_id" int4 NOT NULL
	,"log_read" bool
	,"log_write" bool
	,"log_unlink" bool
	,"log_create" bool
	,"log_type" varchar NOT NULL
	,"state" varchar NOT NULL
	,"action_id" int4
	,"capture_record" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'auditlog_rule');
drop foreign table if exists odoo_audittail_rules_users cascade;
create FOREIGN TABLE odoo_audittail_rules_users("user_id" int4 NOT NULL
	,"rule_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'audittail_rules_users');
drop foreign table if exists odoo_auth_api_key cascade;
create FOREIGN TABLE odoo_auth_api_key("id" int4 NOT NULL
	,"server_env_defaults" text
	,"name" varchar
	,"user_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'auth_api_key');
drop foreign table if exists odoo_b cascade;
create FOREIGN TABLE odoo_b("a" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'b');
drop foreign table if exists odoo_bank_payment_line cascade;
create FOREIGN TABLE odoo_bank_payment_line("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"order_id" int4
	,"payment_type" varchar
	,"state" varchar
	,"partner_id" int4
	,"amount_currency" numeric
	,"amount_company_currency" numeric
	,"communication" varchar NOT NULL
	,"company_id" int4
	,"company_currency_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'bank_payment_line');
drop foreign table if exists odoo_barcode_nomenclature cascade;
create FOREIGN TABLE odoo_barcode_nomenclature("id" int4 NOT NULL
	,"name" varchar(32) NOT NULL
	,"upc_ean_conv" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'barcode_nomenclature');
drop foreign table if exists odoo_barcode_rule cascade;
create FOREIGN TABLE odoo_barcode_rule("id" int4 NOT NULL
	,"name" varchar(32) NOT NULL
	,"barcode_nomenclature_id" int4
	,"sequence" int4
	,"encoding" varchar NOT NULL
	,"type" varchar NOT NULL
	,"pattern" varchar(32) NOT NULL
	,"alias" varchar(32) NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'barcode_rule');
drop foreign table if exists odoo_base_automation cascade;
create FOREIGN TABLE odoo_base_automation("id" int4 NOT NULL
	,"action_server_id" int4 NOT NULL
	,"active" bool
	,"trigger" varchar NOT NULL
	,"trg_date_id" int4
	,"trg_date_range" int4
	,"trg_date_range_type" varchar
	,"trg_date_calendar_id" int4
	,"filter_pre_domain" varchar
	,"filter_domain" varchar
	,"last_run" timestamp
	,"on_change_fields" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'base_automation');
drop foreign table if exists odoo_base_automation_lead_test cascade;
create FOREIGN TABLE odoo_base_automation_lead_test("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"user_id" int4
	,"state" varchar
	,"active" bool
	,"partner_id" int4
	,"date_action_last" timestamp
	,"customer" bool
	,"priority" bool
	,"deadline" bool
	,"is_assigned_to_admin" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'base_automation_lead_test');
drop foreign table if exists odoo_base_automation_line_test cascade;
create FOREIGN TABLE odoo_base_automation_line_test("id" int4 NOT NULL
	,"name" varchar
	,"lead_id" int4
	,"user_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'base_automation_line_test');
drop foreign table if exists odoo_base_import_import cascade;
create FOREIGN TABLE odoo_base_import_import("id" int4 NOT NULL
	,"res_model" varchar
	,"file" bytea
	,"file_name" varchar
	,"file_type" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'base_import_import');
drop foreign table if exists odoo_base_import_mapping cascade;
create FOREIGN TABLE odoo_base_import_mapping("id" int4 NOT NULL
	,"res_model" varchar
	,"column_name" varchar
	,"field_name" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'base_import_mapping');
drop foreign table if exists odoo_base_import_tests_models_char cascade;
create FOREIGN TABLE odoo_base_import_tests_models_char("id" int4 NOT NULL
	,"value" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'base_import_tests_models_char');
drop foreign table if exists odoo_base_import_tests_models_char_noreadonly cascade;
create FOREIGN TABLE odoo_base_import_tests_models_char_noreadonly("id" int4 NOT NULL
	,"value" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'base_import_tests_models_char_noreadonly');
drop foreign table if exists odoo_base_import_tests_models_char_readonly cascade;
create FOREIGN TABLE odoo_base_import_tests_models_char_readonly("id" int4 NOT NULL
	,"value" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'base_import_tests_models_char_readonly');
drop foreign table if exists odoo_base_import_tests_models_char_required cascade;
create FOREIGN TABLE odoo_base_import_tests_models_char_required("id" int4 NOT NULL
	,"value" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'base_import_tests_models_char_required');
drop foreign table if exists odoo_base_import_tests_models_char_states cascade;
create FOREIGN TABLE odoo_base_import_tests_models_char_states("id" int4 NOT NULL
	,"value" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'base_import_tests_models_char_states');
drop foreign table if exists odoo_base_import_tests_models_char_stillreadonly cascade;
create FOREIGN TABLE odoo_base_import_tests_models_char_stillreadonly("id" int4 NOT NULL
	,"value" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'base_import_tests_models_char_stillreadonly');
drop foreign table if exists odoo_base_import_tests_models_complex cascade;
create FOREIGN TABLE odoo_base_import_tests_models_complex("id" int4 NOT NULL
	,"f" float8
	,"m" numeric
	,"c" varchar
	,"currency_id" int4
	,"d" date
	,"dt" timestamp
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'base_import_tests_models_complex');
drop foreign table if exists odoo_base_import_tests_models_float cascade;
create FOREIGN TABLE odoo_base_import_tests_models_float("id" int4 NOT NULL
	,"value" float8
	,"value2" numeric
	,"currency_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'base_import_tests_models_float');
drop foreign table if exists odoo_base_import_tests_models_m2o cascade;
create FOREIGN TABLE odoo_base_import_tests_models_m2o("id" int4 NOT NULL
	,"value" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'base_import_tests_models_m2o');
drop foreign table if exists odoo_base_import_tests_models_m2o_related cascade;
create FOREIGN TABLE odoo_base_import_tests_models_m2o_related("id" int4 NOT NULL
	,"value" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'base_import_tests_models_m2o_related');
drop foreign table if exists odoo_base_import_tests_models_m2o_required cascade;
create FOREIGN TABLE odoo_base_import_tests_models_m2o_required("id" int4 NOT NULL
	,"value" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'base_import_tests_models_m2o_required');
drop foreign table if exists odoo_base_import_tests_models_m2o_required_related cascade;
create FOREIGN TABLE odoo_base_import_tests_models_m2o_required_related("id" int4 NOT NULL
	,"value" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'base_import_tests_models_m2o_required_related');
drop foreign table if exists odoo_base_import_tests_models_o2m cascade;
create FOREIGN TABLE odoo_base_import_tests_models_o2m("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'base_import_tests_models_o2m');
drop foreign table if exists odoo_base_import_tests_models_o2m_child cascade;
create FOREIGN TABLE odoo_base_import_tests_models_o2m_child("id" int4 NOT NULL
	,"parent_id" int4
	,"value" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'base_import_tests_models_o2m_child');
drop foreign table if exists odoo_base_import_tests_models_preview cascade;
create FOREIGN TABLE odoo_base_import_tests_models_preview("id" int4 NOT NULL
	,"name" varchar
	,"somevalue" int4 NOT NULL
	,"othervalue" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'base_import_tests_models_preview');
drop foreign table if exists odoo_base_language_export cascade;
create FOREIGN TABLE odoo_base_language_export("id" int4 NOT NULL
	,"name" varchar
	,"lang" varchar NOT NULL
	,"format" varchar NOT NULL
	,"data" bytea
	,"state" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'base_language_export');
drop foreign table if exists odoo_base_language_import cascade;
create FOREIGN TABLE odoo_base_language_import("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"code" varchar(6) NOT NULL
	,"data" bytea NOT NULL
	,"filename" varchar NOT NULL
	,"overwrite" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'base_language_import');
drop foreign table if exists odoo_base_language_install cascade;
create FOREIGN TABLE odoo_base_language_install("id" int4 NOT NULL
	,"lang" varchar NOT NULL
	,"overwrite" bool
	,"state" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'base_language_install');
drop foreign table if exists odoo_base_module_uninstall cascade;
create FOREIGN TABLE odoo_base_module_uninstall("id" int4 NOT NULL
	,"show_all" bool
	,"module_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'base_module_uninstall');
drop foreign table if exists odoo_base_module_update cascade;
create FOREIGN TABLE odoo_base_module_update("id" int4 NOT NULL
	,"updated" int4
	,"added" int4
	,"state" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'base_module_update');
drop foreign table if exists odoo_base_module_upgrade cascade;
create FOREIGN TABLE odoo_base_module_upgrade("id" int4 NOT NULL
	,"module_info" text
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'base_module_upgrade');
drop foreign table if exists odoo_base_partner_merge_automatic_wizard cascade;
create FOREIGN TABLE odoo_base_partner_merge_automatic_wizard("id" int4 NOT NULL
	,"group_by_email" bool
	,"group_by_name" bool
	,"group_by_is_company" bool
	,"group_by_vat" bool
	,"group_by_parent_id" bool
	,"state" varchar NOT NULL
	,"number_group" int4
	,"current_line_id" int4
	,"dst_partner_id" int4
	,"exclude_contact" bool
	,"exclude_journal_item" bool
	,"maximum_group" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'base_partner_merge_automatic_wizard');
drop foreign table if exists odoo_base_partner_merge_automatic_wizard_res_partner_rel cascade;
create FOREIGN TABLE odoo_base_partner_merge_automatic_wizard_res_partner_rel("base_partner_merge_automatic_wizard_id" int4 NOT NULL
	,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'base_partner_merge_automatic_wizard_res_partner_rel');
drop foreign table if exists odoo_base_partner_merge_line cascade;
create FOREIGN TABLE odoo_base_partner_merge_line("id" int4 NOT NULL
	,"wizard_id" int4
	,"min_id" int4
	,"aggr_ids" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'base_partner_merge_line');
drop foreign table if exists odoo_base_update_translations cascade;
create FOREIGN TABLE odoo_base_update_translations("id" int4 NOT NULL
	,"lang" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'base_update_translations');
drop foreign table if exists odoo_broadband_isp_info cascade;
create FOREIGN TABLE odoo_broadband_isp_info("id" int4 NOT NULL
	,"service_full_street" varchar
	,"service_street" varchar
	,"service_street2" varchar
	,"service_zip_code" varchar
	,"service_city" varchar
	,"service_state_id" int4
	,"service_country_id" int4
	,"previous_service" varchar
	,"keep_phone_number" bool
	,"change_address" bool
	,"service_supplier_id" int4
	,"mm_fiber_coverage" varchar
	,"vdf_fiber_coverage" varchar
	,"adsl_coverage" varchar
	,"phone_number" varchar
	,"delivery_full_street" varchar
	,"delivery_street" varchar
	,"delivery_street2" varchar
	,"delivery_zip_code" varchar
	,"delivery_city" varchar
	,"delivery_state_id" int4
	,"delivery_country_id" int4
	,"invoice_full_street" varchar
	,"invoice_street" varchar
	,"invoice_street2" varchar
	,"invoice_zip_code" varchar
	,"invoice_city" varchar
	,"invoice_state_id" int4
	,"invoice_country_id" int4
	,"type" varchar
	,"previous_provider" int4
	,"previous_owner_vat_number" varchar
	,"previous_owner_first_name" varchar
	,"previous_owner_name" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"old_contract_pon" varchar
	,"old_contract_fiber_speed" varchar)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'broadband_isp_info');
drop foreign table if exists odoo_broadband_service_contract_info cascade;
create FOREIGN TABLE odoo_broadband_service_contract_info("id" int4 NOT NULL
	,"id_order" varchar
	,"previous_id" varchar
	,"phone_number" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'broadband_service_contract_info');
drop foreign table if exists odoo_bus_bus cascade;
create FOREIGN TABLE odoo_bus_bus("id" int4 NOT NULL
	,"channel" varchar
	,"message" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'bus_bus');
drop foreign table if exists odoo_bus_presence cascade;
create FOREIGN TABLE odoo_bus_presence("id" int4 NOT NULL
	,"user_id" int4 NOT NULL
	,"last_poll" timestamp
	,"last_presence" timestamp
	,"status" varchar)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'bus_presence');
drop foreign table if exists odoo_calendar_alarm cascade;
create FOREIGN TABLE odoo_calendar_alarm("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"type" varchar NOT NULL
	,"duration" int4 NOT NULL
	,"interval" varchar NOT NULL
	,"duration_minutes" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'calendar_alarm');
drop foreign table if exists odoo_calendar_alarm_calendar_event_rel cascade;
create FOREIGN TABLE odoo_calendar_alarm_calendar_event_rel("calendar_event_id" int4 NOT NULL
	,"calendar_alarm_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'calendar_alarm_calendar_event_rel');
drop foreign table if exists odoo_calendar_attendee cascade;
create FOREIGN TABLE odoo_calendar_attendee("id" int4 NOT NULL
	,"state" varchar
	,"common_name" varchar
	,"partner_id" int4
	,"email" varchar
	,"availability" varchar
	,"access_token" varchar
	,"event_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'calendar_attendee');
drop foreign table if exists odoo_calendar_contacts cascade;
create FOREIGN TABLE odoo_calendar_contacts("id" int4 NOT NULL
	,"user_id" int4 NOT NULL
	,"partner_id" int4 NOT NULL
	,"active" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'calendar_contacts');
drop foreign table if exists odoo_calendar_event cascade;
create FOREIGN TABLE odoo_calendar_event("id" int4 NOT NULL
	,"message_main_attachment_id" int4
	,"name" varchar NOT NULL
	,"state" varchar
	,"display_start" varchar
	,"start" timestamp NOT NULL
	,"stop" timestamp NOT NULL
	,"allday" bool
	,"start_date" date
	,"start_datetime" timestamp
	,"stop_date" date
	,"stop_datetime" timestamp
	,"duration" float8
	,"description" text
	,"privacy" varchar
	,"location" varchar
	,"show_as" varchar
	,"res_id" int4
	,"res_model_id" int4
	,"res_model" varchar
	,"rrule" varchar
	,"rrule_type" varchar
	,"recurrency" bool
	,"recurrent_id" int4
	,"recurrent_id_date" timestamp
	,"end_type" varchar
	,"interval" int4
	,"count" int4
	,"mo" bool
	,"tu" bool
	,"we" bool
	,"th" bool
	,"fr" bool
	,"sa" bool
	,"su" bool
	,"month_by" varchar
	,"day" int4
	,"week_list" varchar
	,"byday" varchar
	,"final_date" date
	,"user_id" int4
	,"active" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"opportunity_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'calendar_event');
drop foreign table if exists odoo_calendar_event_res_partner_rel cascade;
create FOREIGN TABLE odoo_calendar_event_res_partner_rel("calendar_event_id" int4 NOT NULL
	,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'calendar_event_res_partner_rel');
drop foreign table if exists odoo_calendar_event_type cascade;
create FOREIGN TABLE odoo_calendar_event_type("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'calendar_event_type');
drop foreign table if exists odoo_cash_box_in cascade;
create FOREIGN TABLE odoo_cash_box_in("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"amount" numeric NOT NULL
	,"ref" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'cash_box_in');
drop foreign table if exists odoo_cash_box_out cascade;
create FOREIGN TABLE odoo_cash_box_out("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"amount" numeric NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'cash_box_out');
drop foreign table if exists odoo_change_password_user cascade;
create FOREIGN TABLE odoo_change_password_user("id" int4 NOT NULL
	,"wizard_id" int4 NOT NULL
	,"user_id" int4 NOT NULL
	,"user_login" varchar
	,"new_passwd" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'change_password_user');
drop foreign table if exists odoo_change_password_wizard cascade;
create FOREIGN TABLE odoo_change_password_wizard("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'change_password_wizard');
drop foreign table if exists odoo_contract_address_change_wizard cascade;
create FOREIGN TABLE odoo_contract_address_change_wizard("id" int4 NOT NULL
	,"partner_id" int4
	,"partner_bank_id" int4 NOT NULL
	,"service_street" varchar NOT NULL
	,"service_street2" varchar
	,"service_zip_code" varchar NOT NULL
	,"service_city" varchar NOT NULL
	,"service_state_id" int4 NOT NULL
	,"service_country_id" int4 NOT NULL
	,"service_supplier_id" int4
	,"previous_product_id" int4
	,"product_id" int4 NOT NULL
	,"mm_fiber_coverage" varchar
	,"vdf_fiber_coverage" varchar
	,"adsl_coverage" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"notes" text
	,"contract_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'contract_address_change_wizard');
drop foreign table if exists odoo_contract_compensation_wizard cascade;
create FOREIGN TABLE odoo_contract_compensation_wizard("id" int4 NOT NULL
	,"partner_id" int4
	,"type" varchar
	,"days_without_service" int4
	,"exact_amount" float8
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'contract_compensation_wizard');
drop foreign table if exists odoo_contract_compensation_wizard_contract_contract_rel cascade;
create FOREIGN TABLE odoo_contract_compensation_wizard_contract_contract_rel("contract_compensation_wizard_id" int4 NOT NULL
	,"contract_contract_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'contract_compensation_wizard_contract_contract_rel');
drop foreign table if exists odoo_contract_contract cascade;
create FOREIGN TABLE odoo_contract_contract("id" int4 NOT NULL
	,"message_main_attachment_id" int4
	,"active" bool
	,"code" varchar
	,"group_id" int4
	,"manual_currency_id" int4
	,"contract_template_id" int4
	,"user_id" int4
	,"recurring_next_date" date
	,"date_end" date
	,"payment_term_id" int4
	,"fiscal_position_id" int4
	,"invoice_partner_id" int4
	,"partner_id" int4 NOT NULL
	,"commercial_partner_id" int4
	,"note" text
	,"is_terminated" bool
	,"terminate_reason_id" int4
	,"terminate_comment" text
	,"terminate_date" date
	,"name" varchar NOT NULL
	,"pricelist_id" int4
	,"contract_type" varchar
	,"journal_id" int4
	,"company_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"payment_mode_id" int4
	,"mandate_id" int4
	,"service_technology_id" int4 NOT NULL
	,"service_supplier_id" int4 NOT NULL
	,"service_partner_id" int4
	,"crm_lead_line_id" int4
	,"mobile_contract_service_info_id" int4
	,"vodafone_fiber_service_contract_info_id" int4
	,"mm_fiber_service_contract_info_id" int4
	,"adsl_service_contract_info_id" int4
	,"date_start" date
	,"phone_number" varchar
	,"ticket_number" varchar
	,"terminate_user_reason_id" int4
	,"xoln_fiber_service_contract_info_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'contract_contract');
drop foreign table if exists odoo_contract_contract_contract_email_change_wizard_rel cascade;
create FOREIGN TABLE odoo_contract_contract_contract_email_change_wizard_rel("contract_email_change_wizard_id" int4 NOT NULL
	,"contract_contract_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'contract_contract_contract_email_change_wizard_rel');
drop foreign table if exists odoo_contract_contract_contract_iban_change_wizard_rel cascade;
create FOREIGN TABLE odoo_contract_contract_contract_iban_change_wizard_rel("contract_iban_change_wizard_id" int4 NOT NULL
	,"contract_contract_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'contract_contract_contract_iban_change_wizard_rel');
drop foreign table if exists odoo_contract_contract_contract_tag_rel cascade;
create FOREIGN TABLE odoo_contract_contract_contract_tag_rel("contract_contract_id" int4 NOT NULL
	,"contract_tag_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'contract_contract_contract_tag_rel');
drop foreign table if exists odoo_contract_contract_partner_email_change_wizard_rel cascade;
create FOREIGN TABLE odoo_contract_contract_partner_email_change_wizard_rel("partner_email_change_wizard_id" int4 NOT NULL
	,"contract_contract_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'contract_contract_partner_email_change_wizard_rel');
drop foreign table if exists odoo_contract_contract_res_partner_rel cascade;
create FOREIGN TABLE odoo_contract_contract_res_partner_rel("contract_contract_id" int4 NOT NULL
	,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'contract_contract_res_partner_rel');
drop foreign table if exists odoo_contract_contract_terminate cascade;
create FOREIGN TABLE odoo_contract_contract_terminate("id" int4 NOT NULL
	,"contract_id" int4 NOT NULL
	,"terminate_reason_id" int4 NOT NULL
	,"terminate_comment" text
	,"terminate_date" date NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"terminate_user_reason_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'contract_contract_terminate');
drop foreign table if exists odoo_contract_email_change_wizard cascade;
create FOREIGN TABLE odoo_contract_email_change_wizard("id" int4 NOT NULL
	,"partner_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"summary" varchar NOT NULL
	,"done" bool
	,"location" varchar
	,"note" varchar
	,"start_date" date NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'contract_email_change_wizard');
drop foreign table if exists odoo_contract_email_change_wizard_res_partner_rel cascade;
create FOREIGN TABLE odoo_contract_email_change_wizard_res_partner_rel("contract_email_change_wizard_id" int4 NOT NULL
	,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'contract_email_change_wizard_res_partner_rel');
drop foreign table if exists odoo_contract_force_oc_integration_wizard cascade;
create FOREIGN TABLE odoo_contract_force_oc_integration_wizard("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'contract_force_oc_integration_wizard');
drop foreign table if exists odoo_contract_holder_change_wizard cascade;
create FOREIGN TABLE odoo_contract_holder_change_wizard("id" int4 NOT NULL
	,"partner_id" int4 NOT NULL
	,"contract_id" int4
	,"change_date" date NOT NULL
	,"service_partner_id" int4
	,"notes" text
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"payment_mode" int4 NOT NULL
	,"banking_mandate_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'contract_holder_change_wizard');
drop foreign table if exists odoo_contract_holder_change_wizard_res_partner_rel cascade;
create FOREIGN TABLE odoo_contract_holder_change_wizard_res_partner_rel("contract_holder_change_wizard_id" int4 NOT NULL
	,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'contract_holder_change_wizard_res_partner_rel');
drop foreign table if exists odoo_contract_iban_change_wizard cascade;
create FOREIGN TABLE odoo_contract_iban_change_wizard("id" int4 NOT NULL
	,"partner_id" int4
	,"account_banking_mandate_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"summary" varchar NOT NULL
	,"done" bool
	,"start_date" date NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'contract_iban_change_wizard');
drop foreign table if exists odoo_contract_invoice_payment_wizard cascade;
create FOREIGN TABLE odoo_contract_invoice_payment_wizard("id" int4 NOT NULL
	,"data" bytea
	,"errors" text
	,"state" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'contract_invoice_payment_wizard');
drop foreign table if exists odoo_contract_line cascade;
create FOREIGN TABLE odoo_contract_line("id" int4 NOT NULL
	,"sequence" int4
	,"contract_id" int4 NOT NULL
	,"analytic_account_id" int4
	,"date_start" date NOT NULL
	,"date_end" date
	,"recurring_next_date" date
	,"last_date_invoiced" date
	,"termination_notice_date" date
	,"successor_contract_line_id" int4
	,"predecessor_contract_line_id" int4
	,"manual_renew_needed" bool
	,"active" bool
	,"product_id" int4
	,"name" text NOT NULL
	,"quantity" float8 NOT NULL
	,"uom_id" int4
	,"automatic_price" bool
	,"specific_price" float8
	,"discount" numeric
	,"recurring_rule_type" varchar NOT NULL
	,"recurring_invoicing_type" varchar NOT NULL
	,"recurring_interval" int4 NOT NULL
	,"is_canceled" bool
	,"is_auto_renew" bool
	,"auto_renew_interval" int4
	,"auto_renew_rule_type" varchar
	,"termination_notice_interval" int4
	,"termination_notice_rule_type" varchar
	,"display_type" varchar
	,"note_invoicing_mode" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'contract_line');
drop foreign table if exists odoo_contract_line_wizard cascade;
create FOREIGN TABLE odoo_contract_line_wizard("id" int4 NOT NULL
	,"date_start" date
	,"date_end" date
	,"recurring_next_date" date
	,"is_auto_renew" bool
	,"manual_renew_needed" bool
	,"contract_line_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'contract_line_wizard');
drop foreign table if exists odoo_contract_manually_create_invoice cascade;
create FOREIGN TABLE odoo_contract_manually_create_invoice("id" int4 NOT NULL
	,"invoice_date" date NOT NULL
	,"contract_type" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'contract_manually_create_invoice');
drop foreign table if exists odoo_contract_one_shot_request_wizard cascade;
create FOREIGN TABLE odoo_contract_one_shot_request_wizard("id" int4 NOT NULL
	,"contract_id" int4
	,"start_date" date NOT NULL
	,"one_shot_product_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"summary" varchar NOT NULL
	,"done" bool NOT NULL
	,"location" varchar
	,"note" varchar
	,"activity_type" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'contract_one_shot_request_wizard');
drop foreign table if exists odoo_contract_tag cascade;
create FOREIGN TABLE odoo_contract_tag("id" int4 NOT NULL
	,"name" varchar
	,"company_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'contract_tag');
drop foreign table if exists odoo_contract_tariff_change_wizard cascade;
create FOREIGN TABLE odoo_contract_tariff_change_wizard("id" int4 NOT NULL
	,"contract_id" int4
	,"start_date" date NOT NULL
	,"new_tariff_product_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"summary" varchar NOT NULL
	,"done" bool
	,"location" varchar
	,"note" varchar)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'contract_tariff_change_wizard');
drop foreign table if exists odoo_contract_template cascade;
create FOREIGN TABLE odoo_contract_template("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"partner_id" int4
	,"pricelist_id" int4
	,"contract_type" varchar
	,"journal_id" int4
	,"company_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'contract_template');
drop foreign table if exists odoo_contract_template_line cascade;
create FOREIGN TABLE odoo_contract_template_line("id" int4 NOT NULL
	,"contract_id" int4 NOT NULL
	,"product_id" int4
	,"name" text NOT NULL
	,"quantity" float8 NOT NULL
	,"uom_id" int4
	,"automatic_price" bool
	,"specific_price" float8
	,"discount" numeric
	,"sequence" int4
	,"recurring_rule_type" varchar NOT NULL
	,"recurring_invoicing_type" varchar NOT NULL
	,"recurring_interval" int4 NOT NULL
	,"date_start" date
	,"recurring_next_date" date
	,"last_date_invoiced" date
	,"is_canceled" bool
	,"is_auto_renew" bool
	,"auto_renew_interval" int4
	,"auto_renew_rule_type" varchar
	,"termination_notice_interval" int4
	,"termination_notice_rule_type" varchar
	,"display_type" varchar
	,"note_invoicing_mode" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'contract_template_line');
drop foreign table if exists odoo_contract_terminate_reason cascade;
create FOREIGN TABLE odoo_contract_terminate_reason("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"terminate_comment_required" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"active" bool
	,"sequence" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'contract_terminate_reason');
drop foreign table if exists odoo_contract_terminate_user_reason cascade;
create FOREIGN TABLE odoo_contract_terminate_user_reason("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"active" bool
	,"sequence" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'contract_terminate_user_reason');
drop foreign table if exists odoo_coop_agreement cascade;
create FOREIGN TABLE odoo_coop_agreement("id" int4 NOT NULL
	,"partner_id" int4 NOT NULL
	,"code" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'coop_agreement');
drop foreign table if exists odoo_coop_agreement_product_template_rel cascade;
create FOREIGN TABLE odoo_coop_agreement_product_template_rel("coop_agreement_id" int4 NOT NULL
	,"product_template_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'coop_agreement_product_template_rel');
drop foreign table if exists odoo_crm_activity_report cascade;
create FOREIGN TABLE odoo_crm_activity_report("id" int4
	,"subtype_id" int4
	,"mail_activity_type_id" int4
	,"author_id" int4
	,"date" timestamp
	,"subject" varchar
	,"lead_id" int4
	,"user_id" int4
	,"team_id" int4
	,"country_id" int4
	,"company_id" int4
	,"stage_id" int4
	,"partner_id" int4
	,"lead_type" varchar
	,"active" bool
	,"probability" float8)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'crm_activity_report');
drop foreign table if exists odoo_crm_lead cascade;
create FOREIGN TABLE odoo_crm_lead("id" int4 NOT NULL
	,"campaign_id" int4
	,"source_id" int4
	,"medium_id" int4
	,"message_main_attachment_id" int4
	,"name" varchar NOT NULL
	,"partner_id" int4
	,"active" bool
	,"date_action_last" timestamp
	,"email_from" varchar
	,"website" varchar
	,"team_id" int4
	,"email_cc" text
	,"description" text
	,"contact_name" varchar
	,"partner_name" varchar
	,"type" varchar NOT NULL
	,"priority" varchar
	,"date_closed" timestamp
	,"stage_id" int4
	,"user_id" int4
	,"referred" varchar
	,"date_open" timestamp
	,"day_open" float8
	,"day_close" float8
	,"date_last_stage_update" timestamp
	,"date_conversion" timestamp
	,"message_bounce" int4
	,"probability" float8
	,"planned_revenue" numeric
	,"expected_revenue" numeric
	,"date_deadline" date
	,"color" int4
	,"street" varchar
	,"street2" varchar
	,"zip" varchar
	,"city" varchar
	,"state_id" int4
	,"country_id" int4
	,"phone" varchar
	,"mobile" varchar
	,"function" varchar
	,"title" int4
	,"company_id" int4
	,"lost_reason" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"subscription_request_id" int4
	,"iban" varchar
	,"skip_duplicated_phone_validation" bool)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'crm_lead');
drop foreign table if exists odoo_crm_lead2opportunity_partner cascade;
create FOREIGN TABLE odoo_crm_lead2opportunity_partner("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"user_id" int4
	,"team_id" int4
	,"action" varchar NOT NULL
	,"partner_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'crm_lead2opportunity_partner');
drop foreign table if exists odoo_crm_lead2opportunity_partner_mass cascade;
create FOREIGN TABLE odoo_crm_lead2opportunity_partner_mass("id" int4 NOT NULL
	,"team_id" int4
	,"deduplicate" bool
	,"action" varchar NOT NULL
	,"force_assignation" bool
	,"name" varchar NOT NULL
	,"user_id" int4
	,"partner_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'crm_lead2opportunity_partner_mass');
drop foreign table if exists odoo_crm_lead2opportunity_partner_mass_res_users_rel cascade;
create FOREIGN TABLE odoo_crm_lead2opportunity_partner_mass_res_users_rel("crm_lead2opportunity_partner_mass_id" int4 NOT NULL
	,"res_users_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'crm_lead2opportunity_partner_mass_res_users_rel');
drop foreign table if exists odoo_crm_lead_crm_lead2opportunity_partner_mass_rel cascade;
create FOREIGN TABLE odoo_crm_lead_crm_lead2opportunity_partner_mass_rel("crm_lead2opportunity_partner_mass_id" int4 NOT NULL
	,"crm_lead_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'crm_lead_crm_lead2opportunity_partner_mass_rel');
drop foreign table if exists odoo_crm_lead_crm_lead2opportunity_partner_rel cascade;
create FOREIGN TABLE odoo_crm_lead_crm_lead2opportunity_partner_rel("crm_lead2opportunity_partner_id" int4 NOT NULL
	,"crm_lead_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'crm_lead_crm_lead2opportunity_partner_rel');
drop foreign table if exists odoo_crm_lead_crm_lead_validate_wizard_rel cascade;
create FOREIGN TABLE odoo_crm_lead_crm_lead_validate_wizard_rel("crm_lead_validate_wizard_id" int4 NOT NULL
	,"crm_lead_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'crm_lead_crm_lead_validate_wizard_rel');
drop foreign table if exists odoo_crm_lead_line cascade;
create FOREIGN TABLE odoo_crm_lead_line("id" int4 NOT NULL
	,"lead_id" int4
	,"name" varchar NOT NULL
	,"product_id" int4
	,"category_id" int4
	,"product_tmpl_id" int4
	,"product_qty" int4 NOT NULL
	,"uom_id" int4
	,"price_unit" float8
	,"planned_revenue" float8
	,"expected_revenue" float8
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"broadband_isp_info" int4
	,"mobile_isp_info" int4
	,"is_mobile" bool
	,"ticket_number" varchar
	,"mobile_isp_info_icc" varchar
	,"create_user_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'crm_lead_line');
drop foreign table if exists odoo_crm_lead_line_crm_lead_lines_remesa_wizard_rel cascade;
create FOREIGN TABLE odoo_crm_lead_line_crm_lead_lines_remesa_wizard_rel("crm_lead_lines_remesa_wizard_id" int4 NOT NULL
	,"crm_lead_line_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'crm_lead_line_crm_lead_lines_remesa_wizard_rel');
drop foreign table if exists odoo_crm_lead_line_crm_lead_lines_validate_wizard_rel cascade;
create FOREIGN TABLE odoo_crm_lead_line_crm_lead_lines_validate_wizard_rel("crm_lead_lines_validate_wizard_id" int4 NOT NULL
	,"crm_lead_line_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'crm_lead_line_crm_lead_lines_validate_wizard_rel');
drop foreign table if exists odoo_crm_lead_lines_remesa_wizard cascade;
create FOREIGN TABLE odoo_crm_lead_lines_remesa_wizard("id" int4 NOT NULL
	,"errors" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'crm_lead_lines_remesa_wizard');
drop foreign table if exists odoo_crm_lead_lines_validate_wizard cascade;
create FOREIGN TABLE odoo_crm_lead_lines_validate_wizard("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'crm_lead_lines_validate_wizard');
drop foreign table if exists odoo_crm_lead_lost cascade;
create FOREIGN TABLE odoo_crm_lead_lost("id" int4 NOT NULL
	,"lost_reason_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'crm_lead_lost');
drop foreign table if exists odoo_crm_lead_tag cascade;
create FOREIGN TABLE odoo_crm_lead_tag("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"color" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'crm_lead_tag');
drop foreign table if exists odoo_crm_lead_tag_rel cascade;
create FOREIGN TABLE odoo_crm_lead_tag_rel("lead_id" int4 NOT NULL
	,"tag_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'crm_lead_tag_rel');
drop foreign table if exists odoo_crm_lead_validate_wizard cascade;
create FOREIGN TABLE odoo_crm_lead_validate_wizard("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'crm_lead_validate_wizard');
drop foreign table if exists odoo_crm_lost_reason cascade;
create FOREIGN TABLE odoo_crm_lost_reason("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"active" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'crm_lost_reason');
drop foreign table if exists odoo_crm_merge_opportunity cascade;
create FOREIGN TABLE odoo_crm_merge_opportunity("id" int4 NOT NULL
	,"user_id" int4
	,"team_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'crm_merge_opportunity');
drop foreign table if exists odoo_crm_partner_binding cascade;
create FOREIGN TABLE odoo_crm_partner_binding("id" int4 NOT NULL
	,"action" varchar NOT NULL
	,"partner_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'crm_partner_binding');
drop foreign table if exists odoo_crm_product_report cascade;
create FOREIGN TABLE odoo_crm_product_report("id" int4
	,"active" bool
	,"lead_id" int4
	,"campaign_id" int4
	,"country_id" int4
	,"company_id" int4
	,"create_date" timestamp
	,"date_closed" timestamp
	,"date_conversion" timestamp
	,"date_deadline" date
	,"date_open" timestamp
	,"lost_reason" int4
	,"name" varchar
	,"partner_id" int4
	,"partner_name" varchar
	,"probability" float8
	,"type" varchar
	,"stage_id" int4
	,"team_id" int4
	,"user_id" int4
	,"category_id" int4
	,"expected_revenue" float8
	,"planned_revenue" float8
	,"product_id" int4
	,"product_qty" int4
	,"product_tmpl_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'crm_product_report');
drop foreign table if exists odoo_crm_stage cascade;
create FOREIGN TABLE odoo_crm_stage("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"sequence" int4
	,"probability" float8 NOT NULL
	,"on_change" bool
	,"requirements" text
	,"team_id" int4
	,"legend_priority" text
	,"fold" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'crm_stage');
drop foreign table if exists odoo_crm_team cascade;
create FOREIGN TABLE odoo_crm_team("id" int4 NOT NULL
	,"message_main_attachment_id" int4
	,"name" varchar NOT NULL
	,"active" bool
	,"company_id" int4
	,"user_id" int4
	,"reply_to" varchar
	,"color" int4
	,"team_type" varchar NOT NULL
	,"dashboard_graph_model" varchar
	,"dashboard_graph_group" varchar
	,"dashboard_graph_period" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"use_leads" bool
	,"use_opportunities" bool
	,"alias_id" int4 NOT NULL
	,"dashboard_graph_group_pipeline" varchar
	,"use_quotations" bool
	,"use_invoices" bool
	,"invoiced_target" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'crm_team');
drop foreign table if exists odoo_custom_pop_message cascade;
create FOREIGN TABLE odoo_custom_pop_message("id" int4 NOT NULL
	,"name" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'custom_pop_message');
drop foreign table if exists odoo_date_range cascade;
create FOREIGN TABLE odoo_date_range("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"date_start" date NOT NULL
	,"date_end" date NOT NULL
	,"type_id" int4 NOT NULL
	,"type_name" varchar
	,"company_id" int4
	,"active" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'date_range');
drop foreign table if exists odoo_date_range_generator cascade;
create FOREIGN TABLE odoo_date_range_generator("id" int4 NOT NULL
	,"name_prefix" varchar NOT NULL
	,"date_start" date NOT NULL
	,"type_id" int4 NOT NULL
	,"company_id" int4
	,"unit_of_time" int4 NOT NULL
	,"duration_count" int4 NOT NULL
	,"count" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'date_range_generator');
drop foreign table if exists odoo_date_range_type cascade;
create FOREIGN TABLE odoo_date_range_type("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"allow_overlap" bool
	,"active" bool
	,"company_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'date_range_type');
drop foreign table if exists odoo_decimal_precision cascade;
create FOREIGN TABLE odoo_decimal_precision("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"digits" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'decimal_precision');
drop foreign table if exists odoo_decimal_precision_test cascade;
create FOREIGN TABLE odoo_decimal_precision_test("id" int4 NOT NULL
	,"float" float8
	,"float_2" numeric
	,"float_4" numeric
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'decimal_precision_test');
drop foreign table if exists odoo_digest_digest cascade;
create FOREIGN TABLE odoo_digest_digest("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"periodicity" varchar NOT NULL
	,"next_run_date" date
	,"template_id" int4 NOT NULL
	,"company_id" int4
	,"state" varchar
	,"kpi_res_users_connected" bool
	,"kpi_mail_message_total" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"kpi_account_total_revenue" bool
	,"kpi_crm_lead_created" bool
	,"kpi_crm_opportunities_won" bool
	,"kpi_all_sale_total" bool)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'digest_digest');
drop foreign table if exists odoo_digest_digest_res_users_rel cascade;
create FOREIGN TABLE odoo_digest_digest_res_users_rel("digest_digest_id" int4 NOT NULL
	,"res_users_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'digest_digest_res_users_rel');
drop foreign table if exists odoo_digest_tip cascade;
create FOREIGN TABLE odoo_digest_tip("id" int4 NOT NULL
	,"sequence" int4
	,"tip_description" text
	,"group_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'digest_tip');
drop foreign table if exists odoo_digest_tip_res_users_rel cascade;
create FOREIGN TABLE odoo_digest_tip_res_users_rel("digest_tip_id" int4 NOT NULL
	,"res_users_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'digest_tip_res_users_rel');
drop foreign table if exists odoo_discovery_channel cascade;
create FOREIGN TABLE odoo_discovery_channel("id" int4 NOT NULL
	,"name" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'discovery_channel');
drop foreign table if exists odoo_email_template_attachment_rel cascade;
create FOREIGN TABLE odoo_email_template_attachment_rel("email_template_id" int4 NOT NULL
	,"attachment_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'email_template_attachment_rel');
drop foreign table if exists odoo_email_template_preview cascade;
create FOREIGN TABLE odoo_email_template_preview("id" int4 NOT NULL
	,"res_id" varchar
	,"name" varchar
	,"model_id" int4
	,"model" varchar
	,"lang" varchar
	,"user_signature" bool
	,"subject" varchar
	,"email_from" varchar
	,"use_default_to" bool
	,"email_to" varchar
	,"partner_to" varchar
	,"email_cc" varchar
	,"reply_to" varchar
	,"mail_server_id" int4
	,"body_html" text
	,"report_name" varchar
	,"report_template" int4
	,"ref_ir_act_window" int4
	,"auto_delete" bool
	,"model_object_field" int4
	,"sub_object" int4
	,"sub_model_object_field" int4
	,"null_value" varchar
	,"copyvalue" varchar
	,"scheduled_date" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"force_email_send" bool
	,"easy_my_coop" bool)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'email_template_preview');
drop foreign table if exists odoo_email_template_preview_res_partner_rel cascade;
create FOREIGN TABLE odoo_email_template_preview_res_partner_rel("email_template_preview_id" int4 NOT NULL
	,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'email_template_preview_res_partner_rel');
drop foreign table if exists odoo_fetchmail_server cascade;
create FOREIGN TABLE odoo_fetchmail_server("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"active" bool
	,"state" varchar
	,"server" varchar
	,"port" int4
	,"type" varchar NOT NULL
	,"is_ssl" bool
	,"attach" bool
	,"original" bool
	,"date" timestamp
	,"user" varchar
	,"password" varchar
	,"object_id" int4
	,"priority" int4
	,"configuration" text
	,"script" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'fetchmail_server');
drop foreign table if exists odoo_general_ledger_report_wizard cascade;
create FOREIGN TABLE odoo_general_ledger_report_wizard("id" int4 NOT NULL
	,"company_id" int4
	,"date_range_id" int4
	,"date_from" date NOT NULL
	,"date_to" date NOT NULL
	,"target_move" varchar NOT NULL
	,"centralize" bool
	,"hide_account_at_0" bool
	,"show_analytic_tags" bool
	,"not_only_one_unaffected_earnings_account" bool
	,"foreign_currency" bool
	,"partner_ungrouped" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'general_ledger_report_wizard');
drop foreign table if exists odoo_general_ledger_report_wizard_res_partner_rel cascade;
create FOREIGN TABLE odoo_general_ledger_report_wizard_res_partner_rel("general_ledger_report_wizard_id" int4 NOT NULL
	,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'general_ledger_report_wizard_res_partner_rel');
drop foreign table if exists odoo_import_payment_group_wizard cascade;
create FOREIGN TABLE odoo_import_payment_group_wizard("id" int4 NOT NULL
	,"data" bytea
	,"reference" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'import_payment_group_wizard');
drop foreign table if exists odoo_invoice_claim_1_send_wizard cascade;
create FOREIGN TABLE odoo_invoice_claim_1_send_wizard("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'invoice_claim_1_send_wizard');
drop foreign table if exists odoo_ir_act_client cascade;
create FOREIGN TABLE odoo_ir_act_client("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"type" varchar NOT NULL
	,"help" text
	,"binding_model_id" int4
	,"binding_type" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"tag" varchar NOT NULL
	,"target" varchar
	,"res_model" varchar
	,"context" varchar NOT NULL
	,"params_store" bytea)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_act_client');
drop foreign table if exists odoo_ir_act_report_xml cascade;
create FOREIGN TABLE odoo_ir_act_report_xml("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"type" varchar NOT NULL
	,"help" text
	,"binding_model_id" int4
	,"binding_type" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"model" varchar NOT NULL
	,"report_type" varchar NOT NULL
	,"report_name" varchar NOT NULL
	,"report_file" varchar
	,"multi" bool
	,"paperformat_id" int4
	,"print_report_name" varchar
	,"attachment_use" bool
	,"attachment" varchar)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_act_report_xml');
drop foreign table if exists odoo_ir_act_server cascade;
create FOREIGN TABLE odoo_ir_act_server("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"type" varchar NOT NULL
	,"help" text
	,"binding_model_id" int4
	,"binding_type" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"usage" varchar NOT NULL
	,"state" varchar NOT NULL
	,"sequence" int4
	,"model_id" int4 NOT NULL
	,"model_name" varchar
	,"code" text
	,"crud_model_id" int4
	,"link_field_id" int4
	,"template_id" int4
	,"activity_type_id" int4
	,"activity_summary" varchar
	,"activity_note" text
	,"activity_date_deadline_range" int4
	,"activity_date_deadline_range_type" varchar
	,"activity_user_type" varchar NOT NULL
	,"activity_user_id" int4
	,"activity_user_field_name" varchar)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_act_server');
drop foreign table if exists odoo_ir_act_server_mail_channel_rel cascade;
create FOREIGN TABLE odoo_ir_act_server_mail_channel_rel("ir_act_server_id" int4 NOT NULL
	,"mail_channel_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_act_server_mail_channel_rel');
drop foreign table if exists odoo_ir_act_server_res_partner_rel cascade;
create FOREIGN TABLE odoo_ir_act_server_res_partner_rel("ir_act_server_id" int4 NOT NULL
	,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_act_server_res_partner_rel');
drop foreign table if exists odoo_ir_act_url cascade;
create FOREIGN TABLE odoo_ir_act_url("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"type" varchar NOT NULL
	,"help" text
	,"binding_model_id" int4
	,"binding_type" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"url" text NOT NULL
	,"target" varchar NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_act_url');
drop foreign table if exists odoo_ir_act_window cascade;
create FOREIGN TABLE odoo_ir_act_window("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"type" varchar NOT NULL
	,"help" text
	,"binding_model_id" int4
	,"binding_type" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"view_id" int4
	,"domain" varchar
	,"context" varchar NOT NULL
	,"res_id" int4
	,"res_model" varchar NOT NULL
	,"src_model" varchar
	,"target" varchar
	,"view_mode" varchar NOT NULL
	,"view_type" varchar NOT NULL
	,"usage" varchar
	,"limit" int4
	,"search_view_id" int4
	,"filter" bool
	,"auto_search" bool
	,"multi" bool)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_act_window');
drop foreign table if exists odoo_ir_act_window_group_rel cascade;
create FOREIGN TABLE odoo_ir_act_window_group_rel("act_id" int4 NOT NULL
	,"gid" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_act_window_group_rel');
drop foreign table if exists odoo_ir_act_window_view cascade;
create FOREIGN TABLE odoo_ir_act_window_view("id" int4 NOT NULL
	,"sequence" int4
	,"view_id" int4
	,"view_mode" varchar NOT NULL
	,"act_window_id" int4
	,"multi" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_act_window_view');
drop foreign table if exists odoo_ir_actions cascade;
create FOREIGN TABLE odoo_ir_actions("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"type" varchar NOT NULL
	,"help" text
	,"binding_model_id" int4
	,"binding_type" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_actions');
drop foreign table if exists odoo_ir_actions_todo cascade;
create FOREIGN TABLE odoo_ir_actions_todo("id" int4 NOT NULL
	,"action_id" int4 NOT NULL
	,"sequence" int4
	,"state" varchar NOT NULL
	,"name" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_actions_todo');
drop foreign table if exists odoo_ir_attachment cascade;
create FOREIGN TABLE odoo_ir_attachment("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"datas_fname" varchar
	,"description" text
	,"res_name" varchar
	,"res_model" varchar
	,"res_model_name" varchar
	,"res_field" varchar
	,"res_id" int4
	,"company_id" int4
	,"type" varchar NOT NULL
	,"url" varchar(1024)
	,"public" bool
	,"access_token" varchar
	,"db_datas" bytea
	,"store_fname" varchar
	,"file_size" int4
	,"checksum" varchar(40)
	,"mimetype" varchar
	,"index_content" text
	,"active" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_attachment');
drop foreign table if exists odoo_ir_config_parameter cascade;
create FOREIGN TABLE odoo_ir_config_parameter("id" int4 NOT NULL
	,"key" varchar NOT NULL
	,"value" text NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_config_parameter');
drop foreign table if exists odoo_ir_cron cascade;
create FOREIGN TABLE odoo_ir_cron("id" int4 NOT NULL
	,"ir_actions_server_id" int4 NOT NULL
	,"cron_name" varchar
	,"user_id" int4 NOT NULL
	,"active" bool
	,"interval_number" int4
	,"interval_type" varchar
	,"numbercall" int4
	,"doall" bool
	,"nextcall" timestamp NOT NULL
	,"priority" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_cron');
drop foreign table if exists odoo_ir_default cascade;
create FOREIGN TABLE odoo_ir_default("id" int4 NOT NULL
	,"field_id" int4 NOT NULL
	,"user_id" int4
	,"company_id" int4
	,"condition" varchar
	,"json_value" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_default');
drop foreign table if exists odoo_ir_demo cascade;
create FOREIGN TABLE odoo_ir_demo("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_demo');
drop foreign table if exists odoo_ir_demo_failure cascade;
create FOREIGN TABLE odoo_ir_demo_failure("id" int4 NOT NULL
	,"module_id" int4 NOT NULL
	,"error" varchar
	,"wizard_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_demo_failure');
drop foreign table if exists odoo_ir_demo_failure_wizard cascade;
create FOREIGN TABLE odoo_ir_demo_failure_wizard("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_demo_failure_wizard');
drop foreign table if exists odoo_ir_exports cascade;
create FOREIGN TABLE odoo_ir_exports("id" int4 NOT NULL
	,"name" varchar
	,"resource" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_exports');
drop foreign table if exists odoo_ir_exports_line cascade;
create FOREIGN TABLE odoo_ir_exports_line("id" int4 NOT NULL
	,"name" varchar
	,"export_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_exports_line');
drop foreign table if exists odoo_ir_filters cascade;
create FOREIGN TABLE odoo_ir_filters("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"user_id" int4
	,"domain" text NOT NULL
	,"context" text NOT NULL
	,"sort" text NOT NULL
	,"model_id" varchar NOT NULL
	,"is_default" bool
	,"action_id" int4
	,"active" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_filters');
drop foreign table if exists odoo_ir_logging cascade;
create FOREIGN TABLE odoo_ir_logging("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"name" varchar NOT NULL
	,"type" varchar NOT NULL
	,"dbname" varchar
	,"level" varchar
	,"message" text NOT NULL
	,"path" varchar NOT NULL
	,"func" varchar NOT NULL
	,"line" varchar NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_logging');
drop foreign table if exists odoo_ir_mail_server cascade;
create FOREIGN TABLE odoo_ir_mail_server("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"smtp_host" varchar NOT NULL
	,"smtp_port" int4 NOT NULL
	,"smtp_user" varchar
	,"smtp_pass" varchar
	,"smtp_encryption" varchar NOT NULL
	,"smtp_debug" bool
	,"sequence" int4
	,"active" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_mail_server');
drop foreign table if exists odoo_ir_model cascade;
create FOREIGN TABLE odoo_ir_model("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"model" varchar NOT NULL
	,"info" text
	,"state" varchar
	,"transient" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"is_mail_thread" bool)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_model');
drop foreign table if exists odoo_ir_model_access cascade;
create FOREIGN TABLE odoo_ir_model_access("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"active" bool
	,"model_id" int4 NOT NULL
	,"group_id" int4
	,"perm_read" bool
	,"perm_write" bool
	,"perm_create" bool
	,"perm_unlink" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_model_access');
drop foreign table if exists odoo_ir_model_constraint cascade;
create FOREIGN TABLE odoo_ir_model_constraint("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"definition" varchar
	,"model" int4 NOT NULL
	,"module" int4 NOT NULL
	,"type" varchar(1) NOT NULL
	,"date_update" timestamp
	,"date_init" timestamp
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_model_constraint');
drop foreign table if exists odoo_ir_model_data cascade;
create FOREIGN TABLE odoo_ir_model_data("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_date" timestamp
	,"write_uid" int4
	,"noupdate" bool
	,"name" varchar NOT NULL
	,"date_init" timestamp
	,"date_update" timestamp
	,"module" varchar NOT NULL
	,"model" varchar NOT NULL
	,"res_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_model_data');
drop foreign table if exists odoo_ir_model_fields cascade;
create FOREIGN TABLE odoo_ir_model_fields("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"complete_name" varchar
	,"model" varchar NOT NULL
	,"relation" varchar
	,"relation_field" varchar
	,"relation_field_id" int4
	,"model_id" int4 NOT NULL
	,"field_description" varchar NOT NULL
	,"help" text
	,"ttype" varchar NOT NULL
	,"selection" varchar
	,"copied" bool
	,"related" varchar
	,"related_field_id" int4
	,"required" bool
	,"readonly" bool
	,"index" bool
	,"translate" bool
	,"size" int4
	,"state" varchar NOT NULL
	,"on_delete" varchar
	,"domain" varchar
	,"selectable" bool
	,"relation_table" varchar
	,"column1" varchar
	,"column2" varchar
	,"compute" text
	,"depends" varchar
	,"store" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"serialization_field_id" int4
	,"track_visibility" varchar)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_model_fields');
drop foreign table if exists odoo_ir_model_fields_group_rel cascade;
create FOREIGN TABLE odoo_ir_model_fields_group_rel("field_id" int4 NOT NULL
	,"group_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_model_fields_group_rel');
drop foreign table if exists odoo_ir_model_fields_mis_report_query_rel cascade;
create FOREIGN TABLE odoo_ir_model_fields_mis_report_query_rel("mis_report_query_id" int4 NOT NULL
	,"ir_model_fields_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_model_fields_mis_report_query_rel');
drop foreign table if exists odoo_ir_model_relation cascade;
create FOREIGN TABLE odoo_ir_model_relation("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"model" int4 NOT NULL
	,"module" int4 NOT NULL
	,"date_update" timestamp
	,"date_init" timestamp
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_model_relation');
drop foreign table if exists odoo_ir_module_category cascade;
create FOREIGN TABLE odoo_ir_module_category("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_date" timestamp
	,"write_uid" int4
	,"parent_id" int4
	,"name" varchar NOT NULL
	,"description" text
	,"sequence" int4
	,"visible" bool
	,"exclusive" bool)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_module_category');
drop foreign table if exists odoo_ir_module_module cascade;
create FOREIGN TABLE odoo_ir_module_module("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_date" timestamp
	,"write_uid" int4
	,"website" varchar
	,"summary" varchar
	,"name" varchar NOT NULL
	,"author" varchar
	,"icon" varchar
	,"state" varchar(16)
	,"latest_version" varchar
	,"shortdesc" varchar
	,"category_id" int4
	,"description" text
	,"application" bool
	,"demo" bool
	,"web" bool
	,"license" varchar(32)
	,"sequence" int4
	,"auto_install" bool
	,"to_buy" bool
	,"maintainer" varchar
	,"contributors" text
	,"published_version" varchar
	,"url" varchar
	,"menus_by_module" text
	,"reports_by_module" text
	,"views_by_module" text)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_module_module');
drop foreign table if exists odoo_ir_module_module_dependency cascade;
create FOREIGN TABLE odoo_ir_module_module_dependency("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_date" timestamp
	,"write_uid" int4
	,"name" varchar
	,"module_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_module_module_dependency');
drop foreign table if exists odoo_ir_module_module_exclusion cascade;
create FOREIGN TABLE odoo_ir_module_module_exclusion("id" int4 NOT NULL
	,"name" varchar
	,"module_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_module_module_exclusion');
drop foreign table if exists odoo_ir_property cascade;
create FOREIGN TABLE odoo_ir_property("id" int4 NOT NULL
	,"name" varchar
	,"res_id" varchar
	,"company_id" int4
	,"fields_id" int4 NOT NULL
	,"value_float" float8
	,"value_integer" int4
	,"value_text" text
	,"value_binary" bytea
	,"value_reference" varchar
	,"value_datetime" timestamp
	,"type" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_property');
drop foreign table if exists odoo_ir_rule cascade;
create FOREIGN TABLE odoo_ir_rule("id" int4 NOT NULL
	,"name" varchar
	,"active" bool
	,"model_id" int4 NOT NULL
	,"domain_force" text
	,"perm_read" bool
	,"perm_write" bool
	,"perm_create" bool
	,"perm_unlink" bool
	,"global" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_rule');
drop foreign table if exists odoo_ir_sequence cascade;
create FOREIGN TABLE odoo_ir_sequence("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"code" varchar
	,"implementation" varchar NOT NULL
	,"active" bool
	,"prefix" varchar
	,"suffix" varchar
	,"number_next" int4 NOT NULL
	,"number_increment" int4 NOT NULL
	,"padding" int4 NOT NULL
	,"company_id" int4
	,"use_date_range" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_sequence');
drop foreign table if exists odoo_ir_sequence_date_range cascade;
create FOREIGN TABLE odoo_ir_sequence_date_range("id" int4 NOT NULL
	,"date_from" date NOT NULL
	,"date_to" date NOT NULL
	,"sequence_id" int4 NOT NULL
	,"number_next" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_sequence_date_range');
drop foreign table if exists odoo_ir_server_object_lines cascade;
create FOREIGN TABLE odoo_ir_server_object_lines("id" int4 NOT NULL
	,"server_id" int4
	,"col1" int4 NOT NULL
	,"value" text NOT NULL
	,"type" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_server_object_lines');
drop foreign table if exists odoo_ir_translation cascade;
create FOREIGN TABLE odoo_ir_translation("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"res_id" int4
	,"lang" varchar
	,"type" varchar
	,"src" text
	,"value" text
	,"module" varchar
	,"state" varchar
	,"comments" text)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_translation');
drop foreign table if exists odoo_ir_ui_menu cascade;
create FOREIGN TABLE odoo_ir_ui_menu("id" int4 NOT NULL
	,"parent_path" varchar
	,"name" varchar NOT NULL
	,"active" bool
	,"sequence" int4
	,"parent_id" int4
	,"web_icon" varchar
	,"action" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_ui_menu');
drop foreign table if exists odoo_ir_ui_menu_group_rel cascade;
create FOREIGN TABLE odoo_ir_ui_menu_group_rel("menu_id" int4 NOT NULL
	,"gid" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_ui_menu_group_rel');
drop foreign table if exists odoo_ir_ui_view cascade;
create FOREIGN TABLE odoo_ir_ui_view("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"model" varchar
	,"key" varchar
	,"priority" int4 NOT NULL
	,"type" varchar
	,"arch_db" text
	,"arch_fs" varchar
	,"inherit_id" int4
	,"field_parent" varchar
	,"mode" varchar NOT NULL
	,"active" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_ui_view');
drop foreign table if exists odoo_ir_ui_view_custom cascade;
create FOREIGN TABLE odoo_ir_ui_view_custom("id" int4 NOT NULL
	,"ref_id" int4 NOT NULL
	,"user_id" int4 NOT NULL
	,"arch" text NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_ui_view_custom');
drop foreign table if exists odoo_ir_ui_view_group_rel cascade;
create FOREIGN TABLE odoo_ir_ui_view_group_rel("view_id" int4 NOT NULL
	,"group_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'ir_ui_view_group_rel');
drop foreign table if exists odoo_journal_ledger_report_wizard cascade;
create FOREIGN TABLE odoo_journal_ledger_report_wizard("id" int4 NOT NULL
	,"company_id" int4
	,"date_range_id" int4
	,"date_from" date NOT NULL
	,"date_to" date NOT NULL
	,"move_target" varchar NOT NULL
	,"foreign_currency" bool
	,"sort_option" varchar NOT NULL
	,"group_option" varchar NOT NULL
	,"with_account_name" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'journal_ledger_report_wizard');
drop foreign table if exists odoo_l10n_es_aeat_certificate cascade;
create FOREIGN TABLE odoo_l10n_es_aeat_certificate("id" int4 NOT NULL
	,"name" varchar
	,"state" varchar
	,"file" bytea NOT NULL
	,"folder" varchar NOT NULL
	,"date_start" date
	,"date_end" date
	,"odoo_key" varchar
	,"private_key" varchar
	,"company_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_certificate');
drop foreign table if exists odoo_l10n_es_aeat_certificate_password cascade;
create FOREIGN TABLE odoo_l10n_es_aeat_certificate_password("id" int4 NOT NULL
	,"password" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_certificate_password');
drop foreign table if exists odoo_l10n_es_aeat_map_tax cascade;
create FOREIGN TABLE odoo_l10n_es_aeat_map_tax("id" int4 NOT NULL
	,"date_from" date
	,"date_to" date
	,"model" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_map_tax');
drop foreign table if exists odoo_l10n_es_aeat_map_tax_line cascade;
create FOREIGN TABLE odoo_l10n_es_aeat_map_tax_line("id" int4 NOT NULL
	,"field_number" int4 NOT NULL
	,"name" varchar NOT NULL
	,"map_parent_id" int4 NOT NULL
	,"move_type" varchar NOT NULL
	,"field_type" varchar NOT NULL
	,"sum_type" varchar NOT NULL
	,"exigible_type" varchar NOT NULL
	,"inverse" bool
	,"to_regularize" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_map_tax_line');
drop foreign table if exists odoo_l10n_es_aeat_mod111_report cascade;
create FOREIGN TABLE odoo_l10n_es_aeat_mod111_report("id" int4 NOT NULL
	,"message_main_attachment_id" int4
	,"company_id" int4 NOT NULL
	,"company_vat" varchar(9) NOT NULL
	,"number" varchar(3) NOT NULL
	,"previous_number" varchar(13)
	,"contact_name" varchar(40) NOT NULL
	,"contact_phone" varchar(9) NOT NULL
	,"contact_email" varchar(50)
	,"representative_vat" varchar(9)
	,"year" int4 NOT NULL
	,"type" varchar NOT NULL
	,"support_type" varchar NOT NULL
	,"calculation_date" timestamp
	,"state" varchar
	,"name" varchar(13)
	,"export_config_id" int4
	,"period_type" varchar NOT NULL
	,"date_start" date NOT NULL
	,"date_end" date NOT NULL
	,"counterpart_account_id" int4
	,"journal_id" int4
	,"move_id" int4
	,"partner_bank_id" int4
	,"casilla_01" int4
	,"casilla_04" int4
	,"casilla_07" int4
	,"casilla_10" int4
	,"casilla_11" float8
	,"casilla_12" float8
	,"casilla_13" int4
	,"casilla_14" float8
	,"casilla_15" float8
	,"casilla_16" int4
	,"casilla_17" float8
	,"casilla_18" float8
	,"casilla_19" int4
	,"casilla_20" float8
	,"casilla_21" float8
	,"casilla_22" int4
	,"casilla_23" float8
	,"casilla_24" float8
	,"casilla_25" int4
	,"casilla_26" float8
	,"casilla_27" float8
	,"casilla_29" float8
	,"tipo_declaracion" varchar NOT NULL
	,"colegio_concertado" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_mod111_report');
drop foreign table if exists odoo_l10n_es_aeat_mod190_report cascade;
create FOREIGN TABLE odoo_l10n_es_aeat_mod190_report("id" int4 NOT NULL
	,"message_main_attachment_id" int4
	,"company_id" int4 NOT NULL
	,"company_vat" varchar(9) NOT NULL
	,"number" varchar(3) NOT NULL
	,"previous_number" varchar(13)
	,"contact_name" varchar(40) NOT NULL
	,"contact_phone" varchar(9) NOT NULL
	,"contact_email" varchar(50)
	,"representative_vat" varchar(9)
	,"year" int4 NOT NULL
	,"type" varchar NOT NULL
	,"support_type" varchar NOT NULL
	,"calculation_date" timestamp
	,"state" varchar
	,"name" varchar(13)
	,"export_config_id" int4
	,"period_type" varchar NOT NULL
	,"date_start" date NOT NULL
	,"date_end" date NOT NULL
	,"counterpart_account_id" int4
	,"journal_id" int4
	,"move_id" int4
	,"partner_bank_id" int4
	,"casilla_01" int4
	,"casilla_02" float8
	,"casilla_03" float8
	,"registro_manual" bool
	,"calculado" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_mod190_report');
drop foreign table if exists odoo_l10n_es_aeat_mod190_report_line cascade;
create FOREIGN TABLE odoo_l10n_es_aeat_mod190_report_line("id" int4 NOT NULL
	,"report_id" int4
	,"partner_id" int4 NOT NULL
	,"partner_vat" varchar(15)
	,"representante_legal_vat" varchar(9)
	,"aeat_perception_key_id" int4 NOT NULL
	,"aeat_perception_subkey_id" int4
	,"ejercicio_devengo" varchar(4)
	,"ceuta_melilla" varchar(1)
	,"percepciones_dinerarias" float8
	,"retenciones_dinerarias" float8
	,"percepciones_en_especie" float8
	,"ingresos_a_cuenta_efectuados" float8
	,"ingresos_a_cuenta_repercutidos" float8
	,"percepciones_dinerarias_incap" float8
	,"retenciones_dinerarias_incap" float8
	,"percepciones_en_especie_incap" float8
	,"ingresos_a_cuenta_efectuados_incap" float8
	,"ingresos_a_cuenta_repercutidos_incap" float8
	,"codigo_provincia" varchar(2)
	,"a_nacimiento" varchar(4)
	,"situacion_familiar" varchar
	,"nif_conyuge" varchar(15)
	,"discapacidad" varchar
	,"contrato_o_relacion" varchar
	,"movilidad_geografica" varchar
	,"reduccion_aplicable" float8
	,"gastos_deducibles" float8
	,"pensiones_compensatorias" float8
	,"anualidades_por_alimentos" float8
	,"prestamos_vh" varchar
	,"hijos_y_descendientes_m" int4
	,"hijos_y_descendientes_m_entero" int4
	,"hijos_y_descendientes" int4
	,"hijos_y_descendientes_entero" int4
	,"hijos_y_desc_discapacidad_mr" int4
	,"hijos_y_desc_discapacidad_entero_mr" int4
	,"hijos_y_desc_discapacidad_33" int4
	,"hijos_y_desc_discapacidad_entero_33" int4
	,"hijos_y_desc_discapacidad_66" int4
	,"hijos_y_desc_discapacidad_entero_66" int4
	,"ascendientes" int4
	,"ascendientes_entero" int4
	,"ascendientes_m75" int4
	,"ascendientes_entero_m75" int4
	,"ascendientes_discapacidad_33" int4
	,"ascendientes_discapacidad_entero_33" int4
	,"ascendientes_discapacidad_mr" int4
	,"ascendientes_discapacidad_entero_mr" int4
	,"ascendientes_discapacidad_66" int4
	,"ascendientes_discapacidad_entero_66" int4
	,"computo_primeros_hijos_1" int4
	,"computo_primeros_hijos_2" int4
	,"computo_primeros_hijos_3" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_mod190_report_line');
drop foreign table if exists odoo_l10n_es_aeat_mod303_report cascade;
create FOREIGN TABLE odoo_l10n_es_aeat_mod303_report("id" int4 NOT NULL
	,"message_main_attachment_id" int4
	,"company_id" int4 NOT NULL
	,"company_vat" varchar(9) NOT NULL
	,"number" varchar(3) NOT NULL
	,"previous_number" varchar(13)
	,"contact_name" varchar(40) NOT NULL
	,"contact_phone" varchar(9) NOT NULL
	,"contact_email" varchar(50)
	,"representative_vat" varchar(9)
	,"year" int4 NOT NULL
	,"type" varchar NOT NULL
	,"support_type" varchar NOT NULL
	,"calculation_date" timestamp
	,"state" varchar
	,"name" varchar(13)
	,"export_config_id" int4
	,"period_type" varchar NOT NULL
	,"date_start" date NOT NULL
	,"date_end" date NOT NULL
	,"journal_id" int4
	,"move_id" int4
	,"partner_bank_id" int4
	,"devolucion_mensual" bool
	,"total_devengado" float8
	,"total_deducir" float8
	,"casilla_46" float8
	,"porcentaje_atribuible_estado" float8
	,"atribuible_estado" float8
	,"cuota_compensar" float8
	,"regularizacion_anual" float8
	,"casilla_69" float8
	,"casilla_77" float8
	,"previous_result" float8
	,"resultado_liquidacion" float8
	,"counterpart_account_id" int4
	,"exonerated_390" varchar NOT NULL
	,"has_operation_volume" bool
	,"has_347" bool
	,"is_voluntary_sii" bool
	,"main_activity_code" int4
	,"main_activity_iae" varchar(4)
	,"other_first_activity_code" int4
	,"other_first_activity_iae" varchar(4)
	,"other_second_activity_code" int4
	,"other_second_activity_iae" varchar(4)
	,"other_third_activity_code" int4
	,"other_third_activity_iae" varchar(4)
	,"other_fourth_activity_code" int4
	,"other_fourth_activity_iae" varchar(4)
	,"other_fifth_activity_code" int4
	,"other_fifth_activity_iae" varchar(4)
	,"casilla_88" float8
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"potential_cuota_compensar" float8)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_mod303_report');
drop foreign table if exists odoo_l10n_es_aeat_mod303_report_activity_code cascade;
create FOREIGN TABLE odoo_l10n_es_aeat_mod303_report_activity_code("id" int4 NOT NULL
	,"period_type" varchar NOT NULL
	,"code" int4 NOT NULL
	,"name" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_mod303_report_activity_code');
drop foreign table if exists odoo_l10n_es_aeat_mod349_partner_record cascade;
create FOREIGN TABLE odoo_l10n_es_aeat_mod349_partner_record("id" int4 NOT NULL
	,"report_id" int4
	,"partner_id" int4 NOT NULL
	,"partner_vat" varchar(15)
	,"country_id" int4
	,"operation_key" varchar
	,"total_operation_amount" float8
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_mod349_partner_record');
drop foreign table if exists odoo_l10n_es_aeat_mod349_partner_record_detail cascade;
create FOREIGN TABLE odoo_l10n_es_aeat_mod349_partner_record_detail("id" int4 NOT NULL
	,"report_id" int4 NOT NULL
	,"report_type" varchar
	,"partner_record_id" int4
	,"move_line_id" int4 NOT NULL
	,"amount_untaxed" float8
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_mod349_partner_record_detail');
drop foreign table if exists odoo_l10n_es_aeat_mod349_partner_refund cascade;
create FOREIGN TABLE odoo_l10n_es_aeat_mod349_partner_refund("id" int4 NOT NULL
	,"report_id" int4
	,"partner_id" int4 NOT NULL
	,"partner_vat" varchar(15)
	,"operation_key" varchar
	,"country_id" int4
	,"total_operation_amount" float8
	,"total_origin_amount" float8
	,"period_type" varchar
	,"year" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_mod349_partner_refund');
drop foreign table if exists odoo_l10n_es_aeat_mod349_partner_refund_detail cascade;
create FOREIGN TABLE odoo_l10n_es_aeat_mod349_partner_refund_detail("id" int4 NOT NULL
	,"report_id" int4 NOT NULL
	,"report_type" varchar
	,"refund_id" int4
	,"refund_line_id" int4 NOT NULL
	,"amount_untaxed" float8
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_mod349_partner_refund_detail');
drop foreign table if exists odoo_l10n_es_aeat_mod349_report cascade;
create FOREIGN TABLE odoo_l10n_es_aeat_mod349_report("id" int4 NOT NULL
	,"message_main_attachment_id" int4
	,"company_id" int4 NOT NULL
	,"company_vat" varchar(9) NOT NULL
	,"previous_number" varchar(13)
	,"contact_name" varchar(40) NOT NULL
	,"contact_phone" varchar(9) NOT NULL
	,"contact_email" varchar(50)
	,"representative_vat" varchar(9)
	,"year" int4 NOT NULL
	,"type" varchar NOT NULL
	,"support_type" varchar NOT NULL
	,"calculation_date" timestamp
	,"state" varchar
	,"name" varchar(13)
	,"export_config_id" int4
	,"period_type" varchar NOT NULL
	,"date_start" date NOT NULL
	,"date_end" date NOT NULL
	,"counterpart_account_id" int4
	,"journal_id" int4
	,"move_id" int4
	,"partner_bank_id" int4
	,"frequency_change" bool
	,"total_partner_records" int4
	,"total_partner_records_amount" float8
	,"total_partner_refunds" int4
	,"total_partner_refunds_amount" float8
	,"number" varchar(3) NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_mod349_report');
drop foreign table if exists odoo_l10n_es_aeat_mod390_report cascade;
create FOREIGN TABLE odoo_l10n_es_aeat_mod390_report("id" int4 NOT NULL
	,"message_main_attachment_id" int4
	,"company_id" int4 NOT NULL
	,"company_vat" varchar(9) NOT NULL
	,"number" varchar(3) NOT NULL
	,"previous_number" varchar(13)
	,"contact_name" varchar(40) NOT NULL
	,"contact_phone" varchar(9) NOT NULL
	,"contact_email" varchar(50)
	,"representative_vat" varchar(9)
	,"year" int4 NOT NULL
	,"type" varchar NOT NULL
	,"support_type" varchar NOT NULL
	,"calculation_date" timestamp
	,"state" varchar
	,"name" varchar(13)
	,"export_config_id" int4
	,"period_type" varchar NOT NULL
	,"date_start" date NOT NULL
	,"date_end" date NOT NULL
	,"counterpart_account_id" int4
	,"journal_id" int4
	,"move_id" int4
	,"partner_bank_id" int4
	,"has_347" bool
	,"main_activity" varchar(40)
	,"main_activity_code" varchar
	,"main_activity_iae" varchar(4)
	,"other_first_activity" varchar(40)
	,"other_first_activity_code" varchar
	,"other_first_activity_iae" varchar(4)
	,"other_second_activity" varchar(40)
	,"other_second_activity_code" varchar
	,"other_second_activity_iae" varchar(4)
	,"other_third_activity" varchar(40)
	,"other_third_activity_code" varchar
	,"other_third_activity_iae" varchar(4)
	,"other_fourth_activity" varchar(40)
	,"other_fourth_activity_code" varchar
	,"other_fourth_activity_iae" varchar(4)
	,"other_fifth_activity" varchar(40)
	,"other_fifth_activity_code" varchar
	,"other_fifth_activity_iae" varchar(4)
	,"first_representative_name" varchar(80)
	,"first_representative_vat" varchar(9)
	,"first_representative_date" date
	,"first_representative_notary" varchar(12)
	,"second_representative_name" varchar(80)
	,"second_representative_vat" varchar(9)
	,"second_representative_date" date
	,"second_representative_notary" varchar(12)
	,"third_representative_name" varchar(80)
	,"third_representative_vat" varchar(9)
	,"third_representative_date" date
	,"third_representative_notary" varchar(12)
	,"casilla_33" float8
	,"casilla_34" float8
	,"casilla_47" float8
	,"casilla_48" float8
	,"casilla_49" float8
	,"casilla_50" float8
	,"casilla_51" float8
	,"casilla_52" float8
	,"casilla_53" float8
	,"casilla_54" float8
	,"casilla_55" float8
	,"casilla_56" float8
	,"casilla_57" float8
	,"casilla_58" float8
	,"casilla_59" float8
	,"casilla_597" float8
	,"casilla_598" float8
	,"casilla_64" float8
	,"casilla_65" float8
	,"casilla_85" float8
	,"casilla_86" float8
	,"casilla_95" float8
	,"casilla_97" float8
	,"casilla_98" float8
	,"casilla_108" float8
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_mod390_report');
drop foreign table if exists odoo_l10n_es_aeat_report_compare_boe_file cascade;
create FOREIGN TABLE odoo_l10n_es_aeat_report_compare_boe_file("id" int4 NOT NULL
	,"data" bytea NOT NULL
	,"state" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_report_compare_boe_file');
drop foreign table if exists odoo_l10n_es_aeat_report_compare_boe_file_line cascade;
create FOREIGN TABLE odoo_l10n_es_aeat_report_compare_boe_file_line("id" int4 NOT NULL
	,"wizard_id" int4 NOT NULL
	,"export_line_id" int4 NOT NULL
	,"content" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_report_compare_boe_file_line');
drop foreign table if exists odoo_l10n_es_aeat_report_export_to_boe cascade;
create FOREIGN TABLE odoo_l10n_es_aeat_report_export_to_boe("id" int4 NOT NULL
	,"name" varchar
	,"data" bytea
	,"state" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_report_export_to_boe');
drop foreign table if exists odoo_l10n_es_aeat_report_perception_key cascade;
create FOREIGN TABLE odoo_l10n_es_aeat_report_perception_key("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"code" varchar(3) NOT NULL
	,"aeat_number" varchar(3) NOT NULL
	,"description" text
	,"active" bool
	,"ad_required" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_report_perception_key');
drop foreign table if exists odoo_l10n_es_aeat_report_perception_subkey cascade;
create FOREIGN TABLE odoo_l10n_es_aeat_report_perception_subkey("id" int4 NOT NULL
	,"aeat_perception_key_id" int4
	,"name" varchar(2) NOT NULL
	,"aeat_number" varchar(3) NOT NULL
	,"description" text
	,"active" bool
	,"ad_required" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_report_perception_subkey');
drop foreign table if exists odoo_l10n_es_aeat_soap cascade;
create FOREIGN TABLE odoo_l10n_es_aeat_soap("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_soap');
drop foreign table if exists odoo_l10n_es_aeat_tax_line cascade;
create FOREIGN TABLE odoo_l10n_es_aeat_tax_line("id" int4 NOT NULL
	,"res_id" int4 NOT NULL
	,"field_number" int4
	,"name" varchar
	,"amount" numeric
	,"map_line_id" int4 NOT NULL
	,"model" varchar NOT NULL
	,"model_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_tax_line');
drop foreign table if exists odoo_l10n_es_partner_import_wizard cascade;
create FOREIGN TABLE odoo_l10n_es_partner_import_wizard("id" int4 NOT NULL
	,"import_fail" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'l10n_es_partner_import_wizard');
drop foreign table if exists odoo_link_tracker cascade;
create FOREIGN TABLE odoo_link_tracker("id" int4 NOT NULL
	,"campaign_id" int4
	,"source_id" int4
	,"medium_id" int4
	,"url" varchar NOT NULL
	,"count" int4
	,"title" varchar
	,"favicon" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"mass_mailing_id" int4
	,"mass_mailing_campaign_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'link_tracker');
drop foreign table if exists odoo_link_tracker_click cascade;
create FOREIGN TABLE odoo_link_tracker_click("id" int4 NOT NULL
	,"click_date" date
	,"link_id" int4 NOT NULL
	,"ip" varchar
	,"country_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"mail_stat_id" int4
	,"mass_mailing_id" int4
	,"mass_mailing_campaign_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'link_tracker_click');
drop foreign table if exists odoo_link_tracker_code cascade;
create FOREIGN TABLE odoo_link_tracker_code("id" int4 NOT NULL
	,"code" varchar
	,"link_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'link_tracker_code');
drop foreign table if exists odoo_mail_activity cascade;
create FOREIGN TABLE odoo_mail_activity("id" int4 NOT NULL
	,"res_id" int4 NOT NULL
	,"res_model_id" int4 NOT NULL
	,"res_model" varchar
	,"res_name" varchar
	,"activity_type_id" int4
	,"summary" varchar
	,"note" text
	,"feedback" text
	,"date_deadline" date NOT NULL
	,"automated" bool
	,"user_id" int4 NOT NULL
	,"create_user_id" int4
	,"recommended_activity_type_id" int4
	,"previous_activity_type_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"calendar_event_id" int4
	,"done" bool
	,"date_done" date
	,"location" varchar
	,"partner_id" int4
	,"message_main_attachment_id" int4
	,"confirmation" bool)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_activity');
drop foreign table if exists odoo_mail_activity_rel cascade;
create FOREIGN TABLE odoo_mail_activity_rel("activity_id" int4 NOT NULL
	,"recommended_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_activity_rel');
drop foreign table if exists odoo_mail_activity_type cascade;
create FOREIGN TABLE odoo_mail_activity_type("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"summary" varchar
	,"sequence" int4
	,"active" bool
	,"delay_count" int4
	,"delay_unit" varchar NOT NULL
	,"delay_from" varchar NOT NULL
	,"icon" varchar
	,"decoration_type" varchar
	,"res_model_id" int4
	,"default_next_type_id" int4
	,"force_next" bool
	,"category" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_activity_type');
drop foreign table if exists odoo_mail_activity_type_mail_template_rel cascade;
create FOREIGN TABLE odoo_mail_activity_type_mail_template_rel("mail_activity_type_id" int4 NOT NULL
	,"mail_template_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_activity_type_mail_template_rel');
drop foreign table if exists odoo_mail_alias cascade;
create FOREIGN TABLE odoo_mail_alias("id" int4 NOT NULL
	,"alias_name" varchar
	,"alias_model_id" int4 NOT NULL
	,"alias_user_id" int4
	,"alias_defaults" text NOT NULL
	,"alias_force_thread_id" int4
	,"alias_parent_model_id" int4
	,"alias_parent_thread_id" int4
	,"alias_contact" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_alias');
drop foreign table if exists odoo_mail_blacklist cascade;
create FOREIGN TABLE odoo_mail_blacklist("id" int4 NOT NULL
	,"email" varchar NOT NULL
	,"active" bool
	,"message_main_attachment_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_blacklist');
drop foreign table if exists odoo_mail_channel cascade;
create FOREIGN TABLE odoo_mail_channel("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"channel_type" varchar
	,"description" text
	,"uuid" varchar(50)
	,"email_send" bool
	,"public" varchar NOT NULL
	,"group_odoo_id" int4
	,"moderation" bool
	,"moderation_notify" bool
	,"moderation_notify_msg" text
	,"moderation_guidelines" bool
	,"moderation_guidelines_msg" text
	,"alias_id" int4 NOT NULL
	,"message_main_attachment_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_channel');
drop foreign table if exists odoo_mail_channel_mail_wizard_invite_rel cascade;
create FOREIGN TABLE odoo_mail_channel_mail_wizard_invite_rel("mail_wizard_invite_id" int4 NOT NULL
	,"mail_channel_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_channel_mail_wizard_invite_rel');
drop foreign table if exists odoo_mail_channel_moderator_rel cascade;
create FOREIGN TABLE odoo_mail_channel_moderator_rel("mail_channel_id" int4 NOT NULL
	,"res_users_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_channel_moderator_rel');
drop foreign table if exists odoo_mail_channel_partner cascade;
create FOREIGN TABLE odoo_mail_channel_partner("id" int4 NOT NULL
	,"partner_id" int4
	,"channel_id" int4
	,"seen_message_id" int4
	,"fold_state" varchar
	,"is_minimized" bool
	,"is_pinned" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_channel_partner');
drop foreign table if exists odoo_mail_channel_res_groups_rel cascade;
create FOREIGN TABLE odoo_mail_channel_res_groups_rel("mail_channel_id" int4 NOT NULL
	,"res_groups_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_channel_res_groups_rel');
drop foreign table if exists odoo_mail_compose_message cascade;
create FOREIGN TABLE odoo_mail_compose_message("id" int4 NOT NULL
	,"composition_mode" varchar
	,"use_active_domain" bool
	,"active_domain" text
	,"is_log" bool
	,"subject" varchar
	,"notify" bool
	,"auto_delete" bool
	,"auto_delete_message" bool
	,"template_id" int4
	,"message_type" varchar NOT NULL
	,"subtype_id" int4
	,"date" timestamp
	,"body" text
	,"parent_id" int4
	,"model" varchar
	,"res_id" int4
	,"record_name" varchar
	,"mail_activity_type_id" int4
	,"email_from" varchar
	,"author_id" int4
	,"no_auto_thread" bool
	,"message_id" varchar
	,"reply_to" varchar
	,"mail_server_id" int4
	,"moderation_status" varchar
	,"moderator_id" int4
	,"layout" varchar
	,"add_sign" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"mass_mailing_campaign_id" int4
	,"mass_mailing_id" int4
	,"mass_mailing_name" varchar)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_compose_message');
drop foreign table if exists odoo_mail_compose_message_ir_attachments_rel cascade;
create FOREIGN TABLE odoo_mail_compose_message_ir_attachments_rel("wizard_id" int4 NOT NULL
	,"attachment_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_compose_message_ir_attachments_rel');
drop foreign table if exists odoo_mail_compose_message_mail_mass_mailing_list_rel cascade;
create FOREIGN TABLE odoo_mail_compose_message_mail_mass_mailing_list_rel("mail_compose_message_id" int4 NOT NULL
	,"mail_mass_mailing_list_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_compose_message_mail_mass_mailing_list_rel');
drop foreign table if exists odoo_mail_compose_message_res_partner_rel cascade;
create FOREIGN TABLE odoo_mail_compose_message_res_partner_rel("wizard_id" int4 NOT NULL
	,"partner_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_compose_message_res_partner_rel');
drop foreign table if exists odoo_mail_followers cascade;
create FOREIGN TABLE odoo_mail_followers("id" int4 NOT NULL
	,"res_model" varchar NOT NULL
	,"res_id" int4
	,"partner_id" int4
	,"channel_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_followers');
drop foreign table if exists odoo_mail_followers_mail_message_subtype_rel cascade;
create FOREIGN TABLE odoo_mail_followers_mail_message_subtype_rel("mail_followers_id" int4 NOT NULL
	,"mail_message_subtype_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_followers_mail_message_subtype_rel');
drop foreign table if exists odoo_mail_list_wizard_partner cascade;
create FOREIGN TABLE odoo_mail_list_wizard_partner("partner_mail_list_wizard_id" int4 NOT NULL
	,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_list_wizard_partner');
drop foreign table if exists odoo_mail_mail cascade;
create FOREIGN TABLE odoo_mail_mail("id" int4 NOT NULL
	,"mail_message_id" int4 NOT NULL
	,"body_html" text
	,"references" text
	,"headers" text
	,"notification" bool
	,"email_to" text
	,"email_cc" varchar
	,"state" varchar
	,"auto_delete" bool
	,"failure_reason" text
	,"scheduled_date" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"fetchmail_server_id" int4
	,"mailing_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_mail');
drop foreign table if exists odoo_mail_mail_res_partner_rel cascade;
create FOREIGN TABLE odoo_mail_mail_res_partner_rel("mail_mail_id" int4 NOT NULL
	,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_mail_res_partner_rel');
drop foreign table if exists odoo_mail_mail_statistics cascade;
create FOREIGN TABLE odoo_mail_mail_statistics("id" int4 NOT NULL
	,"mail_mail_id" int4
	,"mail_mail_id_int" int4
	,"message_id" varchar
	,"model" varchar
	,"res_id" int4
	,"mass_mailing_id" int4
	,"mass_mailing_campaign_id" int4
	,"ignored" timestamp
	,"scheduled" timestamp
	,"sent" timestamp
	,"exception" timestamp
	,"opened" timestamp
	,"replied" timestamp
	,"bounced" timestamp
	,"clicked" timestamp
	,"state" varchar
	,"state_update" timestamp
	,"email" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"partner_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_mail_statistics');
drop foreign table if exists odoo_mail_mass_mailing cascade;
create FOREIGN TABLE odoo_mail_mass_mailing("id" int4 NOT NULL
	,"active" bool
	,"email_from" varchar NOT NULL
	,"sent_date" timestamp
	,"schedule_date" timestamp
	,"body_html" text
	,"keep_archives" bool
	,"mass_mailing_campaign_id" int4
	,"campaign_id" int4
	,"source_id" int4 NOT NULL
	,"medium_id" int4
	,"state" varchar NOT NULL
	,"color" int4
	,"user_id" int4
	,"reply_to_mode" varchar NOT NULL
	,"reply_to" varchar
	,"mailing_model_id" int4
	,"mailing_domain" varchar
	,"mail_server_id" int4
	,"contact_ab_pc" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_mass_mailing');
drop foreign table if exists odoo_mail_mass_mailing_campaign cascade;
create FOREIGN TABLE odoo_mail_mass_mailing_campaign("id" int4 NOT NULL
	,"stage_id" int4 NOT NULL
	,"user_id" int4 NOT NULL
	,"campaign_id" int4 NOT NULL
	,"source_id" int4
	,"medium_id" int4
	,"unique_ab_testing" bool
	,"color" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_mass_mailing_campaign');
drop foreign table if exists odoo_mail_mass_mailing_contact cascade;
create FOREIGN TABLE odoo_mail_mass_mailing_contact("id" int4 NOT NULL
	,"name" varchar
	,"company_name" varchar
	,"title_id" int4
	,"email" varchar NOT NULL
	,"is_email_valid" bool
	,"message_bounce" int4
	,"country_id" int4
	,"message_main_attachment_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"partner_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_mass_mailing_contact');
drop foreign table if exists odoo_mail_mass_mailing_contact_list_rel cascade;
create FOREIGN TABLE odoo_mail_mass_mailing_contact_list_rel("id" int4 NOT NULL
	,"contact_id" int4 NOT NULL
	,"list_id" int4 NOT NULL
	,"opt_out" bool
	,"unsubscription_date" timestamp
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_mass_mailing_contact_list_rel');
drop foreign table if exists odoo_mail_mass_mailing_contact_res_partner_category_rel cascade;
create FOREIGN TABLE odoo_mail_mass_mailing_contact_res_partner_category_rel("mail_mass_mailing_contact_id" int4 NOT NULL
	,"res_partner_category_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_mass_mailing_contact_res_partner_category_rel');
drop foreign table if exists odoo_mail_mass_mailing_list cascade;
create FOREIGN TABLE odoo_mail_mass_mailing_list("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"active" bool
	,"is_public" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"partner_mandatory" bool
	,"partner_category" int4
	,"dynamic" bool
	,"sync_method" varchar NOT NULL
	,"sync_domain" varchar NOT NULL
	,"is_synced" bool)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_mass_mailing_list');
drop foreign table if exists odoo_mail_mass_mailing_list_mass_mailing_list_merge_rel cascade;
create FOREIGN TABLE odoo_mail_mass_mailing_list_mass_mailing_list_merge_rel("mass_mailing_list_merge_id" int4 NOT NULL
	,"mail_mass_mailing_list_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_mass_mailing_list_mass_mailing_list_merge_rel');
drop foreign table if exists odoo_mail_mass_mailing_list_rel cascade;
create FOREIGN TABLE odoo_mail_mass_mailing_list_rel("mail_mass_mailing_id" int4 NOT NULL
	,"mail_mass_mailing_list_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_mass_mailing_list_rel');
drop foreign table if exists odoo_mail_mass_mailing_load_filter cascade;
create FOREIGN TABLE odoo_mail_mass_mailing_load_filter("id" int4 NOT NULL
	,"filter_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_mass_mailing_load_filter');
drop foreign table if exists odoo_mail_mass_mailing_stage cascade;
create FOREIGN TABLE odoo_mail_mass_mailing_stage("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"sequence" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_mass_mailing_stage');
drop foreign table if exists odoo_mail_mass_mailing_tag cascade;
create FOREIGN TABLE odoo_mail_mass_mailing_tag("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"color" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_mass_mailing_tag');
drop foreign table if exists odoo_mail_mass_mailing_tag_rel cascade;
create FOREIGN TABLE odoo_mail_mass_mailing_tag_rel("tag_id" int4 NOT NULL
	,"campaign_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_mass_mailing_tag_rel');
drop foreign table if exists odoo_mail_mass_mailing_test cascade;
create FOREIGN TABLE odoo_mail_mass_mailing_test("id" int4 NOT NULL
	,"email_to" varchar NOT NULL
	,"mass_mailing_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_mass_mailing_test');
drop foreign table if exists odoo_mail_message cascade;
create FOREIGN TABLE odoo_mail_message("id" int4 NOT NULL
	,"subject" varchar
	,"date" timestamp
	,"body" text
	,"parent_id" int4
	,"model" varchar
	,"res_id" int4
	,"record_name" varchar
	,"message_type" varchar NOT NULL
	,"subtype_id" int4
	,"mail_activity_type_id" int4
	,"email_from" varchar
	,"author_id" int4
	,"no_auto_thread" bool
	,"message_id" varchar
	,"reply_to" varchar
	,"mail_server_id" int4
	,"moderation_status" varchar
	,"moderator_id" int4
	,"layout" varchar
	,"add_sign" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_message');
drop foreign table if exists odoo_mail_message_mail_channel_rel cascade;
create FOREIGN TABLE odoo_mail_message_mail_channel_rel("mail_message_id" int4 NOT NULL
	,"mail_channel_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_message_mail_channel_rel');
drop foreign table if exists odoo_mail_message_res_partner_needaction_rel cascade;
create FOREIGN TABLE odoo_mail_message_res_partner_needaction_rel("id" int4 NOT NULL
	,"mail_message_id" int4 NOT NULL
	,"res_partner_id" int4 NOT NULL
	,"is_read" bool
	,"is_email" bool
	,"email_status" varchar
	,"mail_id" int4
	,"failure_type" varchar
	,"failure_reason" text)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_message_res_partner_needaction_rel');
drop foreign table if exists odoo_mail_message_res_partner_needaction_rel_mail_resend_message_rel cascade;
create FOREIGN TABLE odoo_mail_message_res_partner_needaction_rel_mail_resend_message_rel("mail_resend_message_id" int4 NOT NULL
	,"mail_message_res_partner_needaction_rel_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_message_res_partner_needaction_rel_mail_resend_message_rel');
drop foreign table if exists odoo_mail_message_res_partner_rel cascade;
create FOREIGN TABLE odoo_mail_message_res_partner_rel("mail_message_id" int4 NOT NULL
	,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_message_res_partner_rel');
drop foreign table if exists odoo_mail_message_res_partner_starred_rel cascade;
create FOREIGN TABLE odoo_mail_message_res_partner_starred_rel("mail_message_id" int4 NOT NULL
	,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_message_res_partner_starred_rel');
drop foreign table if exists odoo_mail_message_subtype cascade;
create FOREIGN TABLE odoo_mail_message_subtype("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"description" text
	,"internal" bool
	,"parent_id" int4
	,"relation_field" varchar
	,"res_model" varchar
	,"default" bool
	,"sequence" int4
	,"hidden" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_message_subtype');
drop foreign table if exists odoo_mail_moderation cascade;
create FOREIGN TABLE odoo_mail_moderation("id" int4 NOT NULL
	,"email" varchar NOT NULL
	,"status" varchar NOT NULL
	,"channel_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_moderation');
drop foreign table if exists odoo_mail_resend_cancel cascade;
create FOREIGN TABLE odoo_mail_resend_cancel("id" int4 NOT NULL
	,"model" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_resend_cancel');
drop foreign table if exists odoo_mail_resend_message cascade;
create FOREIGN TABLE odoo_mail_resend_message("id" int4 NOT NULL
	,"mail_message_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_resend_message');
drop foreign table if exists odoo_mail_resend_partner cascade;
create FOREIGN TABLE odoo_mail_resend_partner("id" int4 NOT NULL
	,"partner_id" int4 NOT NULL
	,"resend" bool
	,"resend_wizard_id" int4
	,"message" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_resend_partner');
drop foreign table if exists odoo_mail_shortcode cascade;
create FOREIGN TABLE odoo_mail_shortcode("id" int4 NOT NULL
	,"source" varchar NOT NULL
	,"substitution" text NOT NULL
	,"description" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_shortcode');
drop foreign table if exists odoo_mail_statistics_report cascade;
create FOREIGN TABLE odoo_mail_statistics_report("id" int4
	,"scheduled_date" timestamp
	,"name" varchar
	,"campaign" varchar
	,"bounced" int8
	,"sent" int8
	,"delivered" int8
	,"opened" int8
	,"replied" int8
	,"clicked" int8
	,"state" varchar
	,"email_from" varchar)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_statistics_report');
drop foreign table if exists odoo_mail_template cascade;
create FOREIGN TABLE odoo_mail_template("id" int4 NOT NULL
	,"name" varchar
	,"model_id" int4
	,"model" varchar
	,"lang" varchar
	,"user_signature" bool
	,"subject" varchar
	,"email_from" varchar
	,"use_default_to" bool
	,"email_to" varchar
	,"partner_to" varchar
	,"email_cc" varchar
	,"reply_to" varchar
	,"mail_server_id" int4
	,"body_html" text
	,"report_name" varchar
	,"report_template" int4
	,"ref_ir_act_window" int4
	,"auto_delete" bool
	,"model_object_field" int4
	,"sub_object" int4
	,"sub_model_object_field" int4
	,"null_value" varchar
	,"copyvalue" varchar
	,"scheduled_date" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"force_email_send" bool
	,"easy_my_coop" bool)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_template');
drop foreign table if exists odoo_mail_tracking_value cascade;
create FOREIGN TABLE odoo_mail_tracking_value("id" int4 NOT NULL
	,"field" varchar NOT NULL
	,"field_desc" varchar NOT NULL
	,"field_type" varchar
	,"old_value_integer" int4
	,"old_value_float" float8
	,"old_value_monetary" float8
	,"old_value_char" varchar
	,"old_value_text" text
	,"old_value_datetime" timestamp
	,"new_value_integer" int4
	,"new_value_float" float8
	,"new_value_monetary" float8
	,"new_value_char" varchar
	,"new_value_text" text
	,"new_value_datetime" timestamp
	,"mail_message_id" int4 NOT NULL
	,"track_sequence" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_tracking_value');
drop foreign table if exists odoo_mail_wizard_invite cascade;
create FOREIGN TABLE odoo_mail_wizard_invite("id" int4 NOT NULL
	,"res_model" varchar NOT NULL
	,"res_id" int4
	,"message" text
	,"send_mail" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_wizard_invite');
drop foreign table if exists odoo_mail_wizard_invite_res_partner_rel cascade;
create FOREIGN TABLE odoo_mail_wizard_invite_res_partner_rel("mail_wizard_invite_id" int4 NOT NULL
	,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mail_wizard_invite_res_partner_rel');
drop foreign table if exists odoo_mass_editing cascade;
create FOREIGN TABLE odoo_mass_editing("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"action_name" varchar NOT NULL
	,"message" text
	,"model_id" int4 NOT NULL
	,"ref_ir_act_window_id" int4
	,"domain" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mass_editing');
drop foreign table if exists odoo_mass_editing_line cascade;
create FOREIGN TABLE odoo_mass_editing_line("id" int4 NOT NULL
	,"sequence" int4
	,"mass_editing_id" int4
	,"field_id" int4
	,"widget_option" varchar
	,"apply_domain" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mass_editing_line');
drop foreign table if exists odoo_mass_editing_wizard cascade;
create FOREIGN TABLE odoo_mass_editing_wizard("id" int4 NOT NULL
	,"selected_item_qty" int4
	,"remaining_item_qty" int4
	,"operation_description_info" text
	,"operation_description_warning" text
	,"operation_description_danger" text
	,"message" text
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mass_editing_wizard');
drop foreign table if exists odoo_mass_group_rel cascade;
create FOREIGN TABLE odoo_mass_group_rel("mass_id" int4 NOT NULL
	,"group_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mass_group_rel');
drop foreign table if exists odoo_mass_mailing_ir_attachments_rel cascade;
create FOREIGN TABLE odoo_mass_mailing_ir_attachments_rel("mass_mailing_id" int4 NOT NULL
	,"attachment_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mass_mailing_ir_attachments_rel');
drop foreign table if exists odoo_mass_mailing_list_merge cascade;
create FOREIGN TABLE odoo_mass_mailing_list_merge("id" int4 NOT NULL
	,"dest_list_id" int4
	,"merge_options" varchar NOT NULL
	,"new_list_name" varchar
	,"archive_src_lists" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mass_mailing_list_merge');
drop foreign table if exists odoo_mass_mailing_schedule_date cascade;
create FOREIGN TABLE odoo_mass_mailing_schedule_date("id" int4 NOT NULL
	,"schedule_date" timestamp
	,"mass_mailing_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mass_mailing_schedule_date');
drop foreign table if exists odoo_meeting_category_rel cascade;
create FOREIGN TABLE odoo_meeting_category_rel("event_id" int4 NOT NULL
	,"type_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'meeting_category_rel');
drop foreign table if exists odoo_merge_opportunity_rel cascade;
create FOREIGN TABLE odoo_merge_opportunity_rel("merge_id" int4 NOT NULL
	,"opportunity_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'merge_opportunity_rel');
drop foreign table if exists odoo_message_attachment_rel cascade;
create FOREIGN TABLE odoo_message_attachment_rel("message_id" int4 NOT NULL
	,"attachment_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'message_attachment_rel');
drop foreign table if exists odoo_mis_budget cascade;
create FOREIGN TABLE odoo_mis_budget("id" int4 NOT NULL
	,"report_id" int4 NOT NULL
	,"message_main_attachment_id" int4
	,"name" varchar NOT NULL
	,"description" varchar
	,"date_range_id" int4
	,"date_from" date NOT NULL
	,"date_to" date NOT NULL
	,"state" varchar NOT NULL
	,"company_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mis_budget');
drop foreign table if exists odoo_mis_budget_by_account cascade;
create FOREIGN TABLE odoo_mis_budget_by_account("id" int4 NOT NULL
	,"company_id" int4 NOT NULL
	,"message_main_attachment_id" int4
	,"name" varchar NOT NULL
	,"description" varchar
	,"date_range_id" int4
	,"date_from" date NOT NULL
	,"date_to" date NOT NULL
	,"state" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mis_budget_by_account');
drop foreign table if exists odoo_mis_budget_by_account_item cascade;
create FOREIGN TABLE odoo_mis_budget_by_account_item("id" int4 NOT NULL
	,"budget_id" int4 NOT NULL
	,"debit" numeric
	,"credit" numeric
	,"balance" numeric
	,"company_id" int4
	,"company_currency_id" int4
	,"account_id" int4 NOT NULL
	,"date_range_id" int4
	,"date_from" date NOT NULL
	,"date_to" date NOT NULL
	,"analytic_account_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mis_budget_by_account_item');
drop foreign table if exists odoo_mis_budget_item cascade;
create FOREIGN TABLE odoo_mis_budget_item("id" int4 NOT NULL
	,"amount" float8
	,"budget_id" int4 NOT NULL
	,"kpi_expression_id" int4 NOT NULL
	,"seq1" int4
	,"seq2" int4
	,"date_range_id" int4
	,"date_from" date NOT NULL
	,"date_to" date NOT NULL
	,"analytic_account_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mis_budget_item');
drop foreign table if exists odoo_mis_report cascade;
create FOREIGN TABLE odoo_mis_report("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"description" varchar
	,"style_id" int4
	,"move_lines_source" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mis_report');
drop foreign table if exists odoo_mis_report_instance cascade;
create FOREIGN TABLE odoo_mis_report_instance("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"date" date
	,"report_id" int4 NOT NULL
	,"target_move" varchar NOT NULL
	,"company_id" int4 NOT NULL
	,"multi_company" bool
	,"currency_id" int4
	,"landscape_pdf" bool
	,"no_auto_expand_accounts" bool
	,"display_columns_description" bool
	,"date_range_id" int4
	,"date_from" date
	,"date_to" date
	,"temporary" bool
	,"analytic_account_id" int4
	,"hide_analytic_filters" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mis_report_instance');
drop foreign table if exists odoo_mis_report_instance_period cascade;
create FOREIGN TABLE odoo_mis_report_instance_period("id" int4 NOT NULL
	,"name" varchar(32) NOT NULL
	,"mode" varchar NOT NULL
	,"type" varchar
	,"is_ytd" bool
	,"date_range_type_id" int4
	,"offset" int4
	,"duration" int4
	,"manual_date_from" date
	,"manual_date_to" date
	,"date_range_id" int4
	,"sequence" int4
	,"report_instance_id" int4 NOT NULL
	,"normalize_factor" int4
	,"source" varchar NOT NULL
	,"source_aml_model_id" int4
	,"source_sumcol_accdet" bool
	,"source_cmpcol_from_id" int4
	,"source_cmpcol_to_id" int4
	,"analytic_account_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"source_mis_budget_id" int4
	,"source_mis_budget_by_account_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mis_report_instance_period');
drop foreign table if exists odoo_mis_report_instance_period_mis_report_subkpi_rel cascade;
create FOREIGN TABLE odoo_mis_report_instance_period_mis_report_subkpi_rel("mis_report_instance_period_id" int4 NOT NULL
	,"mis_report_subkpi_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mis_report_instance_period_mis_report_subkpi_rel');
drop foreign table if exists odoo_mis_report_instance_period_sum cascade;
create FOREIGN TABLE odoo_mis_report_instance_period_sum("id" int4 NOT NULL
	,"period_id" int4 NOT NULL
	,"period_to_sum_id" int4 NOT NULL
	,"sign" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mis_report_instance_period_sum');
drop foreign table if exists odoo_mis_report_instance_res_company_rel cascade;
create FOREIGN TABLE odoo_mis_report_instance_res_company_rel("mis_report_instance_id" int4 NOT NULL
	,"res_company_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mis_report_instance_res_company_rel');
drop foreign table if exists odoo_mis_report_kpi cascade;
create FOREIGN TABLE odoo_mis_report_kpi("id" int4 NOT NULL
	,"name" varchar(32) NOT NULL
	,"description" varchar NOT NULL
	,"multi" bool
	,"auto_expand_accounts" bool
	,"auto_expand_accounts_style_id" int4
	,"style_id" int4
	,"style_expression" varchar
	,"type" varchar NOT NULL
	,"compare_method" varchar NOT NULL
	,"accumulation_method" varchar NOT NULL
	,"sequence" int4
	,"report_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"budgetable" bool)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mis_report_kpi');
drop foreign table if exists odoo_mis_report_kpi_expression cascade;
create FOREIGN TABLE odoo_mis_report_kpi_expression("id" int4 NOT NULL
	,"sequence" int4
	,"name" varchar
	,"kpi_id" int4 NOT NULL
	,"subkpi_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mis_report_kpi_expression');
drop foreign table if exists odoo_mis_report_query cascade;
create FOREIGN TABLE odoo_mis_report_query("id" int4 NOT NULL
	,"name" varchar(32) NOT NULL
	,"model_id" int4 NOT NULL
	,"aggregate" varchar
	,"date_field" int4 NOT NULL
	,"domain" varchar
	,"report_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mis_report_query');
drop foreign table if exists odoo_mis_report_style cascade;
create FOREIGN TABLE odoo_mis_report_style("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"color_inherit" bool
	,"color" varchar
	,"background_color_inherit" bool
	,"background_color" varchar
	,"font_style_inherit" bool
	,"font_style" varchar
	,"font_weight_inherit" bool
	,"font_weight" varchar
	,"font_size_inherit" bool
	,"font_size" varchar
	,"indent_level_inherit" bool
	,"indent_level" int4
	,"prefix_inherit" bool
	,"prefix" varchar(16)
	,"suffix_inherit" bool
	,"suffix" varchar(16)
	,"dp_inherit" bool
	,"dp" int4
	,"divider_inherit" bool
	,"divider" varchar
	,"hide_empty_inherit" bool
	,"hide_empty" bool
	,"hide_always_inherit" bool
	,"hide_always" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mis_report_style');
drop foreign table if exists odoo_mis_report_subkpi cascade;
create FOREIGN TABLE odoo_mis_report_subkpi("id" int4 NOT NULL
	,"sequence" int4
	,"report_id" int4 NOT NULL
	,"name" varchar(32) NOT NULL
	,"description" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mis_report_subkpi');
drop foreign table if exists odoo_mis_report_subreport cascade;
create FOREIGN TABLE odoo_mis_report_subreport("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"report_id" int4 NOT NULL
	,"subreport_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mis_report_subreport');
drop foreign table if exists odoo_mm_fiber_service_contract_info cascade;
create FOREIGN TABLE odoo_mm_fiber_service_contract_info("id" int4 NOT NULL
	,"mm_id" varchar NOT NULL
	,"phone_number" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"id_order" varchar
	,"previous_id" varchar)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mm_fiber_service_contract_info');
drop foreign table if exists odoo_mobile_contract_otrs_view cascade;
create FOREIGN TABLE odoo_mobile_contract_otrs_view("contract_id" int4
	,"contract_code" varchar
	,"contract_create_date" timestamp
	,"contract_is_terminated" bool
	,"contract_phone_number" varchar
	,"partner_vat" varchar
	,"partner_name" varchar
	,"partner_ref" varchar
	,"partner_email" varchar
	,"contract_current_tariff" varchar
	,"contract_emails" text)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mobile_contract_otrs_view');
drop foreign table if exists odoo_mobile_isp_info cascade;
create FOREIGN TABLE odoo_mobile_isp_info("id" int4 NOT NULL
	,"icc" varchar
	,"icc_donor" varchar
	,"previous_contract_type" varchar
	,"phone_number" varchar
	,"delivery_full_street" varchar
	,"delivery_street" varchar
	,"delivery_street2" varchar
	,"delivery_zip_code" varchar
	,"delivery_city" varchar
	,"delivery_state_id" int4
	,"delivery_country_id" int4
	,"invoice_full_street" varchar
	,"invoice_street" varchar
	,"invoice_street2" varchar
	,"invoice_zip_code" varchar
	,"invoice_city" varchar
	,"invoice_state_id" int4
	,"invoice_country_id" int4
	,"type" varchar
	,"previous_provider" int4
	,"previous_owner_vat_number" varchar
	,"previous_owner_first_name" varchar
	,"previous_owner_name" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mobile_isp_info');
drop foreign table if exists odoo_mobile_service_contract_info cascade;
create FOREIGN TABLE odoo_mobile_service_contract_info("id" int4 NOT NULL
	,"icc" varchar NOT NULL
	,"phone_number" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"delivery_street" varchar
	,"delivery_street2" varchar
	,"delivery_zip_code" varchar
	,"delivery_city" varchar
	,"delivery_state_id" int4
	,"delivery_country_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'mobile_service_contract_info');
drop foreign table if exists odoo_move_line_add_to_payment_debit_order cascade;
create FOREIGN TABLE odoo_move_line_add_to_payment_debit_order("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'move_line_add_to_payment_debit_order');
drop foreign table if exists odoo_open_items_report_wizard cascade;
create FOREIGN TABLE odoo_open_items_report_wizard("id" int4 NOT NULL
	,"company_id" int4
	,"date_at" date NOT NULL
	,"target_move" varchar NOT NULL
	,"hide_account_at_0" bool
	,"receivable_accounts_only" bool
	,"payable_accounts_only" bool
	,"foreign_currency" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'open_items_report_wizard');
drop foreign table if exists odoo_open_items_report_wizard_res_partner_rel cascade;
create FOREIGN TABLE odoo_open_items_report_wizard_res_partner_rel("open_items_report_wizard_id" int4 NOT NULL
	,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'open_items_report_wizard_res_partner_rel');
drop foreign table if exists odoo_operation_request cascade;
create FOREIGN TABLE odoo_operation_request("id" int4 NOT NULL
	,"request_date" date
	,"effective_date" date
	,"partner_id" int4 NOT NULL
	,"partner_id_to" int4
	,"operation_type" varchar NOT NULL
	,"share_product_id" int4 NOT NULL
	,"share_to_product_id" int4
	,"quantity" int4 NOT NULL
	,"state" varchar NOT NULL
	,"user_id" int4
	,"receiver_not_member" bool
	,"company_id" int4 NOT NULL
	,"invoice" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'operation_request');
drop foreign table if exists odoo_partner_check_somoffice_email_wizard cascade;
create FOREIGN TABLE odoo_partner_check_somoffice_email_wizard("id" int4 NOT NULL
	,"partner_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'partner_check_somoffice_email_wizard');
drop foreign table if exists odoo_partner_create_lead_wizard cascade;
create FOREIGN TABLE odoo_partner_create_lead_wizard("id" int4 NOT NULL
	,"partner_id" int4
	,"title" varchar
	,"opportunity" varchar NOT NULL
	,"bank_id" int4 NOT NULL
	,"email_id" int4 NOT NULL
	,"phone_contact" varchar NOT NULL
	,"product_id" int4 NOT NULL
	,"service_type" varchar
	,"icc" varchar
	,"type" varchar NOT NULL
	,"previous_contract_type" varchar
	,"phone_number" varchar
	,"donor_icc" varchar
	,"previous_mobile_provider" int4
	,"previous_BA_provider" int4
	,"previous_owner_vat_number" varchar
	,"previous_owner_first_name" varchar
	,"previous_owner_name" varchar
	,"keep_landline" bool
	,"previous_BA_service" varchar
	,"delivery_street" varchar NOT NULL
	,"delivery_zip_code" varchar NOT NULL
	,"delivery_city" varchar NOT NULL
	,"delivery_state_id" int4 NOT NULL
	,"delivery_country_id" int4 NOT NULL
	,"invoice_street" varchar
	,"invoice_zip_code" varchar
	,"invoice_city" varchar
	,"invoice_state_id" int4
	,"invoice_country_id" int4
	,"service_street" varchar
	,"service_zip_code" varchar
	,"service_city" varchar
	,"service_state_id" int4
	,"service_country_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"landline" varchar)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'partner_create_lead_wizard');
drop foreign table if exists odoo_partner_create_subscription cascade;
create FOREIGN TABLE odoo_partner_create_subscription("id" int4 NOT NULL
	,"is_company" bool
	,"cooperator" int4
	,"register_number" varchar
	,"email" varchar NOT NULL
	,"bank_account" varchar NOT NULL
	,"share_product" int4 NOT NULL
	,"share_qty" int4 NOT NULL
	,"representative_name" varchar
	,"representative_email" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"bank_id" int4 NOT NULL
	,"payment_type" varchar NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'partner_create_subscription');
drop foreign table if exists odoo_partner_email_change_wizard cascade;
create FOREIGN TABLE odoo_partner_email_change_wizard("id" int4 NOT NULL
	,"partner_id" int4
	,"summary" varchar
	,"done" bool
	,"start_date" date
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"change_contact_email" varchar NOT NULL
	,"email_id" int4
	,"change_contracts_emails" varchar NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'partner_email_change_wizard');
drop foreign table if exists odoo_partner_email_change_wizard_res_partner_rel cascade;
create FOREIGN TABLE odoo_partner_email_change_wizard_res_partner_rel("partner_email_change_wizard_id" int4 NOT NULL
	,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'partner_email_change_wizard_res_partner_rel');
drop foreign table if exists odoo_partner_mail_list_wizard cascade;
create FOREIGN TABLE odoo_partner_mail_list_wizard("id" int4 NOT NULL
	,"mail_list_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'partner_mail_list_wizard');
drop foreign table if exists odoo_partner_otrs_view cascade;
create FOREIGN TABLE odoo_partner_otrs_view("customerid" varchar
	,"partner_id" varchar
	,"first_name" varchar
	,"name" varchar
	,"partner_number" varchar
	,"date_partner" date
	,"date_partner_end" date
	,"active" int4
	,"birthday" date
	,"party_type" text
	,"identifier_type" text
	,"identifier_code" varchar
	,"email" varchar
	,"language" varchar
	,"address" varchar
	,"city" varchar
	,"zip" varchar
	,"country_code" text
	,"country" varchar
	,"subdivision_code" text
	,"subdivision" varchar)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'partner_otrs_view');
drop foreign table if exists odoo_partner_priority cascade;
create FOREIGN TABLE odoo_partner_priority("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"description" text NOT NULL
	,"sequence" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'partner_priority');
drop foreign table if exists odoo_partner_update_info cascade;
create FOREIGN TABLE odoo_partner_update_info("id" int4 NOT NULL
	,"is_company" bool
	,"register_number" varchar
	,"cooperator" int4
	,"all" bool
	,"birthdate" bool
	,"legal_form" bool
	,"representative_function" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'partner_update_info');
drop foreign table if exists odoo_payment_acquirer cascade;
create FOREIGN TABLE odoo_payment_acquirer("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"description" text
	,"sequence" int4
	,"provider" varchar NOT NULL
	,"company_id" int4 NOT NULL
	,"view_template_id" int4 NOT NULL
	,"registration_view_template_id" int4
	,"environment" varchar NOT NULL
	,"website_published" bool
	,"capture_manually" bool
	,"journal_id" int4
	,"specific_countries" bool
	,"pre_msg" text
	,"post_msg" text
	,"pending_msg" text
	,"done_msg" text
	,"cancel_msg" text
	,"error_msg" text
	,"save_token" varchar
	,"fees_active" bool
	,"fees_dom_fixed" float8
	,"fees_dom_var" float8
	,"fees_int_fixed" float8
	,"fees_int_var" float8
	,"qr_code" bool
	,"module_id" int4
	,"payment_flow" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"so_reference_type" varchar)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'payment_acquirer');
drop foreign table if exists odoo_payment_acquirer_onboarding_wizard cascade;
create FOREIGN TABLE odoo_payment_acquirer_onboarding_wizard("id" int4 NOT NULL
	,"payment_method" varchar
	,"paypal_email_account" varchar
	,"paypal_seller_account" varchar
	,"paypal_pdt_token" varchar
	,"stripe_secret_key" varchar
	,"stripe_publishable_key" varchar
	,"manual_name" varchar
	,"journal_name" varchar
	,"acc_number" varchar
	,"manual_post_msg" text
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'payment_acquirer_onboarding_wizard');
drop foreign table if exists odoo_payment_acquirer_payment_icon_rel cascade;
create FOREIGN TABLE odoo_payment_acquirer_payment_icon_rel("payment_acquirer_id" int4 NOT NULL
	,"payment_icon_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'payment_acquirer_payment_icon_rel');
drop foreign table if exists odoo_payment_country_rel cascade;
create FOREIGN TABLE odoo_payment_country_rel("payment_id" int4 NOT NULL
	,"country_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'payment_country_rel');
drop foreign table if exists odoo_payment_icon cascade;
create FOREIGN TABLE odoo_payment_icon("id" int4 NOT NULL
	,"name" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'payment_icon');
drop foreign table if exists odoo_payment_order_generated_uploaded_queued cascade;
create FOREIGN TABLE odoo_payment_order_generated_uploaded_queued("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'payment_order_generated_uploaded_queued');
drop foreign table if exists odoo_payment_return cascade;
create FOREIGN TABLE odoo_payment_return("id" int4 NOT NULL
	,"message_main_attachment_id" int4
	,"company_id" int4 NOT NULL
	,"date" date
	,"name" varchar NOT NULL
	,"journal_id" int4 NOT NULL
	,"move_id" int4
	,"state" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"imported_bank_account_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'payment_return');
drop foreign table if exists odoo_payment_return_import cascade;
create FOREIGN TABLE odoo_payment_return_import("id" int4 NOT NULL
	,"journal_id" int4
	,"data_file" bytea NOT NULL
	,"match_after_import" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'payment_return_import');
drop foreign table if exists odoo_payment_return_line cascade;
create FOREIGN TABLE odoo_payment_return_line("id" int4 NOT NULL
	,"return_id" int4 NOT NULL
	,"concept" varchar
	,"reason_id" int4
	,"reason_additional_information" varchar
	,"reference" varchar
	,"date" date
	,"partner_name" varchar
	,"partner_id" int4
	,"amount" numeric
	,"expense_account" int4
	,"expense_amount" float8
	,"expense_partner_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"unique_import_id" varchar
	,"raw_import_data" varchar)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'payment_return_line');
drop foreign table if exists odoo_payment_return_reason cascade;
create FOREIGN TABLE odoo_payment_return_reason("id" int4 NOT NULL
	,"code" varchar
	,"name" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'payment_return_reason');
drop foreign table if exists odoo_payment_token cascade;
create FOREIGN TABLE odoo_payment_token("id" int4 NOT NULL
	,"name" varchar
	,"partner_id" int4 NOT NULL
	,"acquirer_id" int4 NOT NULL
	,"acquirer_ref" varchar NOT NULL
	,"active" bool
	,"verified" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'payment_token');
drop foreign table if exists odoo_payment_transaction cascade;
create FOREIGN TABLE odoo_payment_transaction("id" int4 NOT NULL
	,"date" timestamp
	,"acquirer_id" int4 NOT NULL
	,"type" varchar NOT NULL
	,"state" varchar NOT NULL
	,"state_message" text
	,"amount" numeric NOT NULL
	,"fees" numeric
	,"currency_id" int4 NOT NULL
	,"reference" varchar NOT NULL
	,"acquirer_reference" varchar
	,"partner_id" int4
	,"partner_name" varchar
	,"partner_lang" varchar
	,"partner_email" varchar
	,"partner_zip" varchar
	,"partner_address" varchar
	,"partner_city" varchar
	,"partner_country_id" int4 NOT NULL
	,"partner_phone" varchar
	,"html_3ds" varchar
	,"callback_model_id" int4
	,"callback_res_id" int4
	,"callback_method" varchar
	,"callback_hash" varchar
	,"return_url" varchar
	,"is_processed" bool
	,"payment_token_id" int4
	,"payment_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'payment_transaction');
drop foreign table if exists odoo_pg_stat_statements cascade;
create FOREIGN TABLE odoo_pg_stat_statements("userid" oid
	,"dbid" oid
	,"queryid" int8
	,"query" text
	,"calls" int8
	,"total_time" float8
	,"min_time" float8
	,"max_time" float8
	,"mean_time" float8
	,"stddev_time" float8
	,"rows" int8
	,"shared_blks_hit" int8
	,"shared_blks_read" int8
	,"shared_blks_dirtied" int8
	,"shared_blks_written" int8
	,"local_blks_hit" int8
	,"local_blks_read" int8
	,"local_blks_dirtied" int8
	,"local_blks_written" int8
	,"temp_blks_read" int8
	,"temp_blks_written" int8
	,"blk_read_time" float8
	,"blk_write_time" float8)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'pg_stat_statements');
drop foreign table if exists odoo_portal_share cascade;
create FOREIGN TABLE odoo_portal_share("id" int4 NOT NULL
	,"res_model" varchar NOT NULL
	,"res_id" int4 NOT NULL
	,"note" text
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'portal_share');
drop foreign table if exists odoo_portal_share_res_partner_rel cascade;
create FOREIGN TABLE odoo_portal_share_res_partner_rel("portal_share_id" int4 NOT NULL
	,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'portal_share_res_partner_rel');
drop foreign table if exists odoo_portal_wizard cascade;
create FOREIGN TABLE odoo_portal_wizard("id" int4 NOT NULL
	,"welcome_message" text
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'portal_wizard');
drop foreign table if exists odoo_portal_wizard_user cascade;
create FOREIGN TABLE odoo_portal_wizard_user("id" int4 NOT NULL
	,"wizard_id" int4 NOT NULL
	,"partner_id" int4 NOT NULL
	,"email" varchar
	,"in_portal" bool
	,"user_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'portal_wizard_user');
drop foreign table if exists odoo_previous_provider cascade;
create FOREIGN TABLE odoo_previous_provider("id" int4 NOT NULL
	,"name" varchar
	,"code" varchar
	,"mobile" bool
	,"broadband" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'previous_provider');
drop foreign table if exists odoo_procurement_group cascade;
create FOREIGN TABLE odoo_procurement_group("id" int4 NOT NULL
	,"partner_id" int4
	,"name" varchar NOT NULL
	,"move_type" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"sale_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'procurement_group');
drop foreign table if exists odoo_product_attr_exclusion_value_ids_rel cascade;
create FOREIGN TABLE odoo_product_attr_exclusion_value_ids_rel("product_template_attribute_exclusion_id" int4 NOT NULL
	,"product_template_attribute_value_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'product_attr_exclusion_value_ids_rel');
drop foreign table if exists odoo_product_attribute cascade;
create FOREIGN TABLE odoo_product_attribute("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"sequence" int4
	,"create_variant" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"type" varchar NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'product_attribute');
drop foreign table if exists odoo_product_attribute_custom_value cascade;
create FOREIGN TABLE odoo_product_attribute_custom_value("id" int4 NOT NULL
	,"attribute_value_id" int4
	,"sale_order_line_id" int4
	,"custom_value" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'product_attribute_custom_value');
drop foreign table if exists odoo_product_attribute_value cascade;
create FOREIGN TABLE odoo_product_attribute_value("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"sequence" int4
	,"attribute_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"is_custom" bool
	,"html_color" varchar
	,"catalog_name" varchar)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'product_attribute_value');
drop foreign table if exists odoo_product_attribute_value_product_product_rel cascade;
create FOREIGN TABLE odoo_product_attribute_value_product_product_rel("product_product_id" int4 NOT NULL
	,"product_attribute_value_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'product_attribute_value_product_product_rel');
drop foreign table if exists odoo_product_attribute_value_product_template_attribute_line_rel cascade;
create FOREIGN TABLE odoo_product_attribute_value_product_template_attribute_line_rel("product_template_attribute_line_id" int4 NOT NULL
	,"product_attribute_value_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'product_attribute_value_product_template_attribute_line_rel');
drop foreign table if exists odoo_product_category cascade;
create FOREIGN TABLE odoo_product_category("id" int4 NOT NULL
	,"parent_path" varchar
	,"name" varchar NOT NULL
	,"complete_name" varchar
	,"parent_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"removal_strategy_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'product_category');
drop foreign table if exists odoo_product_category_technology_supplier cascade;
create FOREIGN TABLE odoo_product_category_technology_supplier("id" int4 NOT NULL
	,"product_category_id" int4
	,"service_supplier_id" int4
	,"service_technology_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'product_category_technology_supplier');
drop foreign table if exists odoo_product_optional_rel cascade;
create FOREIGN TABLE odoo_product_optional_rel("src_id" int4 NOT NULL
	,"dest_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'product_optional_rel');
drop foreign table if exists odoo_product_packaging cascade;
create FOREIGN TABLE odoo_product_packaging("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"sequence" int4
	,"product_id" int4
	,"qty" float8
	,"barcode" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'product_packaging');
drop foreign table if exists odoo_product_price_history cascade;
create FOREIGN TABLE odoo_product_price_history("id" int4 NOT NULL
	,"company_id" int4 NOT NULL
	,"product_id" int4 NOT NULL
	,"datetime" timestamp
	,"cost" numeric
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'product_price_history');
drop foreign table if exists odoo_product_price_list cascade;
create FOREIGN TABLE odoo_product_price_list("id" int4 NOT NULL
	,"price_list" int4 NOT NULL
	,"qty1" int4
	,"qty2" int4
	,"qty3" int4
	,"qty4" int4
	,"qty5" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'product_price_list');
drop foreign table if exists odoo_product_pricelist cascade;
create FOREIGN TABLE odoo_product_pricelist("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"active" bool
	,"currency_id" int4 NOT NULL
	,"company_id" int4
	,"sequence" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"discount_policy" varchar
	,"code" varchar NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'product_pricelist');
drop foreign table if exists odoo_product_pricelist_item cascade;
create FOREIGN TABLE odoo_product_pricelist_item("id" int4 NOT NULL
	,"product_tmpl_id" int4
	,"product_id" int4
	,"categ_id" int4
	,"min_quantity" int4
	,"applied_on" varchar NOT NULL
	,"base" varchar NOT NULL
	,"base_pricelist_id" int4
	,"pricelist_id" int4
	,"price_surcharge" numeric
	,"price_discount" numeric
	,"price_round" numeric
	,"price_min_margin" numeric
	,"price_max_margin" numeric
	,"company_id" int4
	,"currency_id" int4
	,"date_start" date
	,"date_end" date
	,"compute_price" varchar
	,"fixed_price" numeric
	,"percent_price" float8
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'product_pricelist_item');
drop foreign table if exists odoo_product_product cascade;
create FOREIGN TABLE odoo_product_product("id" int4 NOT NULL
	,"message_main_attachment_id" int4
	,"default_code" varchar
	,"active" bool
	,"product_tmpl_id" int4 NOT NULL
	,"barcode" varchar
	,"volume" float8
	,"weight" numeric
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"custom_name" varchar
	,"discontinued" bool
	,"showed_name" varchar
	,"public" bool)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'product_product');
drop foreign table if exists odoo_product_putaway cascade;
create FOREIGN TABLE odoo_product_putaway("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'product_putaway');
drop foreign table if exists odoo_product_removal cascade;
create FOREIGN TABLE odoo_product_removal("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"method" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'product_removal');
drop foreign table if exists odoo_product_replenish cascade;
create FOREIGN TABLE odoo_product_replenish("id" int4 NOT NULL
	,"product_id" int4 NOT NULL
	,"product_tmpl_id" int4 NOT NULL
	,"product_has_variants" bool NOT NULL
	,"product_uom_id" int4 NOT NULL
	,"quantity" float8 NOT NULL
	,"date_planned" timestamp
	,"warehouse_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'product_replenish');
drop foreign table if exists odoo_product_replenish_stock_location_route_rel cascade;
create FOREIGN TABLE odoo_product_replenish_stock_location_route_rel("product_replenish_id" int4 NOT NULL
	,"stock_location_route_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'product_replenish_stock_location_route_rel');
drop foreign table if exists odoo_product_supplier_taxes_rel cascade;
create FOREIGN TABLE odoo_product_supplier_taxes_rel("prod_id" int4 NOT NULL
	,"tax_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'product_supplier_taxes_rel');
drop foreign table if exists odoo_product_supplierinfo cascade;
create FOREIGN TABLE odoo_product_supplierinfo("id" int4 NOT NULL
	,"name" int4 NOT NULL
	,"product_name" varchar
	,"product_code" varchar
	,"sequence" int4
	,"min_qty" float8 NOT NULL
	,"price" numeric NOT NULL
	,"company_id" int4
	,"currency_id" int4 NOT NULL
	,"date_start" date
	,"date_end" date
	,"product_id" int4
	,"product_tmpl_id" int4
	,"delay" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'product_supplierinfo');
drop foreign table if exists odoo_product_taxes_rel cascade;
create FOREIGN TABLE odoo_product_taxes_rel("prod_id" int4 NOT NULL
	,"tax_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'product_taxes_rel');
drop foreign table if exists odoo_product_template cascade;
create FOREIGN TABLE odoo_product_template("id" int4 NOT NULL
	,"message_main_attachment_id" int4
	,"name" varchar NOT NULL
	,"sequence" int4
	,"description" text
	,"description_purchase" text
	,"description_sale" text
	,"type" varchar NOT NULL
	,"rental" bool
	,"categ_id" int4 NOT NULL
	,"list_price" numeric
	,"volume" float8
	,"weight" numeric
	,"sale_ok" bool
	,"purchase_ok" bool
	,"uom_id" int4 NOT NULL
	,"uom_po_id" int4 NOT NULL
	,"company_id" int4
	,"active" bool
	,"color" int4
	,"default_code" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"responsible_id" int4 NOT NULL
	,"sale_delay" float8
	,"tracking" varchar NOT NULL
	,"description_picking" text
	,"description_pickingout" text
	,"description_pickingin" text
	,"service_type" varchar
	,"sale_line_warn" varchar NOT NULL
	,"sale_line_warn_msg" text
	,"expense_policy" varchar
	,"invoice_policy" varchar
	,"is_share" bool
	,"short_name" varchar
	,"display_on_website" bool
	,"default_share_product" bool
	,"minimum_quantity" int4
	,"force_min_qty" bool
	,"by_company" bool
	,"by_individual" bool
	,"customer" bool
	,"mail_template" int4
	,"_api_external_id" int4
	,"external_id_sequence_id" int4
	,"catalog_attribute_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'product_template');
drop foreign table if exists odoo_product_template_attribute_exclusion cascade;
create FOREIGN TABLE odoo_product_template_attribute_exclusion("id" int4 NOT NULL
	,"product_template_attribute_value_id" int4
	,"product_tmpl_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'product_template_attribute_exclusion');
drop foreign table if exists odoo_product_template_attribute_line cascade;
create FOREIGN TABLE odoo_product_template_attribute_line("id" int4 NOT NULL
	,"product_tmpl_id" int4 NOT NULL
	,"attribute_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'product_template_attribute_line');
drop foreign table if exists odoo_product_template_attribute_value cascade;
create FOREIGN TABLE odoo_product_template_attribute_value("id" int4 NOT NULL
	,"product_attribute_value_id" int4 NOT NULL
	,"product_tmpl_id" int4 NOT NULL
	,"price_extra" numeric
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'product_template_attribute_value');
drop foreign table if exists odoo_product_template_attribute_value_sale_order_line_rel cascade;
create FOREIGN TABLE odoo_product_template_attribute_value_sale_order_line_rel("sale_order_line_id" int4 NOT NULL
	,"product_template_attribute_value_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'product_template_attribute_value_sale_order_line_rel');
drop foreign table if exists odoo_queue_job cascade;
create FOREIGN TABLE odoo_queue_job("id" int4 NOT NULL
	,"uuid" varchar NOT NULL
	,"user_id" int4 NOT NULL
	,"company_id" int4
	,"name" varchar
	,"model_name" varchar
	,"method_name" varchar
	,"record_ids" text
	,"args" text
	,"kwargs" text
	,"func_string" varchar
	,"state" varchar NOT NULL
	,"priority" int4
	,"exc_info" text
	,"result" text
	,"date_created" timestamp
	,"date_started" timestamp
	,"date_enqueued" timestamp
	,"date_done" timestamp
	,"eta" timestamp
	,"retry" int4
	,"max_retries" int4
	,"channel_method_name" varchar
	,"job_function_id" int4
	,"override_channel" varchar
	,"channel" varchar
	,"identity_key" varchar
	,"message_main_attachment_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'queue_job');
drop foreign table if exists odoo_queue_job_channel cascade;
create FOREIGN TABLE odoo_queue_job_channel("id" int4 NOT NULL
	,"name" varchar
	,"complete_name" varchar
	,"parent_id" int4
	,"removal_interval" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'queue_job_channel');
drop foreign table if exists odoo_queue_job_function cascade;
create FOREIGN TABLE odoo_queue_job_function("id" int4 NOT NULL
	,"name" varchar
	,"channel_id" int4 NOT NULL
	,"channel" varchar)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'queue_job_function');
drop foreign table if exists odoo_queue_job_queue_jobs_to_done_rel cascade;
create FOREIGN TABLE odoo_queue_job_queue_jobs_to_done_rel("queue_jobs_to_done_id" int4 NOT NULL
	,"queue_job_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'queue_job_queue_jobs_to_done_rel');
drop foreign table if exists odoo_queue_job_queue_requeue_job_rel cascade;
create FOREIGN TABLE odoo_queue_job_queue_requeue_job_rel("queue_requeue_job_id" int4 NOT NULL
	,"queue_job_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'queue_job_queue_requeue_job_rel');
drop foreign table if exists odoo_queue_jobs_to_done cascade;
create FOREIGN TABLE odoo_queue_jobs_to_done("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'queue_jobs_to_done');
drop foreign table if exists odoo_queue_requeue_job cascade;
create FOREIGN TABLE odoo_queue_requeue_job("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'queue_requeue_job');
drop foreign table if exists odoo_rel_modules_langexport cascade;
create FOREIGN TABLE odoo_rel_modules_langexport("wiz_id" int4 NOT NULL
	,"module_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'rel_modules_langexport');
drop foreign table if exists odoo_rel_server_actions cascade;
create FOREIGN TABLE odoo_rel_server_actions("server_id" int4 NOT NULL
	,"action_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'rel_server_actions');
drop foreign table if exists odoo_report_aged_partner_balance cascade;
create FOREIGN TABLE odoo_report_aged_partner_balance("id" int4 NOT NULL
	,"date_at" date
	,"only_posted_moves" bool
	,"company_id" int4
	,"show_move_line_details" bool
	,"open_items_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"group_by_select" varchar)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_aged_partner_balance');
drop foreign table if exists odoo_report_aged_partner_balance_account cascade;
create FOREIGN TABLE odoo_report_aged_partner_balance_account("id" int4 NOT NULL
	,"report_id" int4
	,"account_id" int4
	,"code" varchar
	,"name" varchar
	,"cumul_amount_residual" numeric
	,"cumul_current" numeric
	,"cumul_age_30_days" numeric
	,"cumul_age_60_days" numeric
	,"cumul_age_90_days" numeric
	,"cumul_age_120_days" numeric
	,"cumul_older" numeric
	,"percent_current" numeric
	,"percent_age_30_days" numeric
	,"percent_age_60_days" numeric
	,"percent_age_90_days" numeric
	,"percent_age_120_days" numeric
	,"percent_older" numeric
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"cumul_amount_due" numeric
	,"percent_amount_due" numeric)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_aged_partner_balance_account');
drop foreign table if exists odoo_report_aged_partner_balance_line cascade;
create FOREIGN TABLE odoo_report_aged_partner_balance_line("id" int4 NOT NULL
	,"report_partner_id" int4
	,"partner" varchar
	,"amount_residual" numeric
	,"current" numeric
	,"age_30_days" numeric
	,"age_60_days" numeric
	,"age_90_days" numeric
	,"age_120_days" numeric
	,"older" numeric
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"amount_due" numeric
	,"vat" varchar)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_aged_partner_balance_line');
drop foreign table if exists odoo_report_aged_partner_balance_move_line cascade;
create FOREIGN TABLE odoo_report_aged_partner_balance_move_line("id" int4 NOT NULL
	,"report_partner_id" int4
	,"move_line_id" int4
	,"date" date
	,"date_due" date
	,"entry" varchar
	,"journal" varchar
	,"account" varchar
	,"partner" varchar
	,"label" varchar
	,"amount_residual" numeric
	,"current" numeric
	,"age_30_days" numeric
	,"age_60_days" numeric
	,"age_90_days" numeric
	,"age_120_days" numeric
	,"older" numeric
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"amount_due" numeric
	,"partner_cummul_id" int4
	,"vat" varchar)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_aged_partner_balance_move_line');
drop foreign table if exists odoo_report_aged_partner_balance_partner cascade;
create FOREIGN TABLE odoo_report_aged_partner_balance_partner("id" int4 NOT NULL
	,"report_account_id" int4
	,"partner_id" int4
	,"name" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"vat" varchar)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_aged_partner_balance_partner');
drop foreign table if exists odoo_report_aged_partner_balance_partner_cummul cascade;
create FOREIGN TABLE odoo_report_aged_partner_balance_partner_cummul("id" int4 NOT NULL
	,"partner_id" int4
	,"name" varchar
	,"vat" varchar
	,"report_id" int4
	,"amount_residual" numeric
	,"amount_due" numeric
	,"current" numeric
	,"age_30_days" numeric
	,"age_60_days" numeric
	,"age_90_days" numeric
	,"age_120_days" numeric
	,"older" numeric
	,"percent_current" numeric
	,"percent_age_30_days" numeric
	,"percent_age_60_days" numeric
	,"percent_age_90_days" numeric
	,"percent_age_120_days" numeric
	,"percent_older" numeric
	,"percent_amount_due" numeric
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_aged_partner_balance_partner_cummul');
drop foreign table if exists odoo_report_aged_partner_balance_res_partner_rel cascade;
create FOREIGN TABLE odoo_report_aged_partner_balance_res_partner_rel("report_aged_partner_balance_id" int4 NOT NULL
	,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_aged_partner_balance_res_partner_rel');
drop foreign table if exists odoo_report_all_channels_sales cascade;
create FOREIGN TABLE odoo_report_all_channels_sales("id" int4
	,"name" varchar
	,"partner_id" int4
	,"product_id" int4
	,"product_tmpl_id" int4
	,"date_order" timestamp
	,"user_id" int4
	,"categ_id" int4
	,"company_id" int4
	,"price_total" numeric
	,"pricelist_id" int4
	,"analytic_account_id" int4
	,"country_id" int4
	,"team_id" int4
	,"price_subtotal" numeric
	,"product_qty" numeric)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_all_channels_sales');
drop foreign table if exists odoo_report_general_ledger cascade;
create FOREIGN TABLE odoo_report_general_ledger("id" int4 NOT NULL
	,"date_from" date
	,"date_to" date
	,"fy_start_date" date
	,"only_posted_moves" bool
	,"hide_account_at_0" bool
	,"foreign_currency" bool
	,"show_analytic_tags" bool
	,"company_id" int4
	,"centralize" bool
	,"show_cost_center" bool
	,"partner_ungrouped" bool
	,"unaffected_earnings_account" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_general_ledger');
drop foreign table if exists odoo_report_general_ledger_account cascade;
create FOREIGN TABLE odoo_report_general_ledger_account("id" int4 NOT NULL
	,"report_id" int4
	,"account_id" int4
	,"code" varchar
	,"name" varchar
	,"initial_debit" numeric
	,"initial_credit" numeric
	,"initial_balance" numeric
	,"currency_id" int4
	,"initial_balance_foreign_currency" numeric
	,"final_debit" numeric
	,"final_credit" numeric
	,"final_balance" numeric
	,"final_balance_foreign_currency" numeric
	,"is_partner_account" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_general_ledger_account');
drop foreign table if exists odoo_report_general_ledger_move_line cascade;
create FOREIGN TABLE odoo_report_general_ledger_move_line("id" int4 NOT NULL
	,"report_account_id" int4
	,"report_partner_id" int4
	,"move_line_id" int4
	,"date" date
	,"entry" varchar
	,"journal" varchar
	,"account" varchar
	,"taxes_description" varchar
	,"partner" varchar
	,"label" varchar
	,"cost_center" varchar
	,"tags" varchar
	,"matching_number" varchar
	,"debit" numeric
	,"credit" numeric
	,"cumul_balance" numeric
	,"currency_id" int4
	,"amount_currency" numeric
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_general_ledger_move_line');
drop foreign table if exists odoo_report_general_ledger_partner cascade;
create FOREIGN TABLE odoo_report_general_ledger_partner("id" int4 NOT NULL
	,"report_account_id" int4
	,"partner_id" int4
	,"name" varchar
	,"initial_debit" numeric
	,"initial_credit" numeric
	,"initial_balance" numeric
	,"currency_id" int4
	,"initial_balance_foreign_currency" numeric
	,"final_debit" numeric
	,"final_credit" numeric
	,"final_balance" numeric
	,"final_balance_foreign_currency" numeric
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_general_ledger_partner');
drop foreign table if exists odoo_report_general_ledger_res_partner_rel cascade;
create FOREIGN TABLE odoo_report_general_ledger_res_partner_rel("report_general_ledger_id" int4 NOT NULL
	,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_general_ledger_res_partner_rel');
drop foreign table if exists odoo_report_journal_ledger cascade;
create FOREIGN TABLE odoo_report_journal_ledger("id" int4 NOT NULL
	,"date_from" date NOT NULL
	,"date_to" date NOT NULL
	,"company_id" int4 NOT NULL
	,"move_target" varchar NOT NULL
	,"sort_option" varchar NOT NULL
	,"group_option" varchar NOT NULL
	,"foreign_currency" bool
	,"with_account_name" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_journal_ledger');
drop foreign table if exists odoo_report_journal_ledger_journal cascade;
create FOREIGN TABLE odoo_report_journal_ledger_journal("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"code" varchar
	,"report_id" int4 NOT NULL
	,"journal_id" int4 NOT NULL
	,"debit" numeric
	,"credit" numeric
	,"company_id" int4 NOT NULL
	,"currency_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_journal_ledger_journal');
drop foreign table if exists odoo_report_journal_ledger_journal_tax_line cascade;
create FOREIGN TABLE odoo_report_journal_ledger_journal_tax_line("id" int4 NOT NULL
	,"report_journal_ledger_id" int4 NOT NULL
	,"report_id" int4 NOT NULL
	,"tax_id" int4
	,"tax_name" varchar
	,"tax_code" varchar
	,"base_debit" numeric
	,"base_credit" numeric
	,"tax_debit" numeric
	,"tax_credit" numeric
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_journal_ledger_journal_tax_line');
drop foreign table if exists odoo_report_journal_ledger_move cascade;
create FOREIGN TABLE odoo_report_journal_ledger_move("id" int4 NOT NULL
	,"report_id" int4 NOT NULL
	,"report_journal_ledger_id" int4 NOT NULL
	,"move_id" int4 NOT NULL
	,"name" varchar
	,"company_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_journal_ledger_move');
drop foreign table if exists odoo_report_journal_ledger_move_line cascade;
create FOREIGN TABLE odoo_report_journal_ledger_move_line("id" int4 NOT NULL
	,"report_id" int4 NOT NULL
	,"report_journal_ledger_id" int4 NOT NULL
	,"report_move_id" int4 NOT NULL
	,"move_line_id" int4 NOT NULL
	,"account_id" int4
	,"account" varchar
	,"account_code" varchar
	,"account_type" varchar
	,"partner" varchar
	,"partner_id" int4
	,"date" date
	,"entry" varchar
	,"label" varchar
	,"debit" numeric
	,"credit" numeric
	,"company_currency_id" int4
	,"amount_currency" numeric
	,"currency_id" int4
	,"currency_name" varchar
	,"taxes_description" varchar
	,"tax_id" int4
	,"company_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_journal_ledger_move_line');
drop foreign table if exists odoo_report_journal_ledger_report_tax_line cascade;
create FOREIGN TABLE odoo_report_journal_ledger_report_tax_line("id" int4 NOT NULL
	,"report_id" int4 NOT NULL
	,"tax_id" int4
	,"tax_name" varchar
	,"tax_code" varchar
	,"base_debit" numeric
	,"base_credit" numeric
	,"tax_debit" numeric
	,"tax_credit" numeric
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_journal_ledger_report_tax_line');
drop foreign table if exists odoo_report_layout cascade;
create FOREIGN TABLE odoo_report_layout("id" int4 NOT NULL
	,"view_id" int4 NOT NULL
	,"image" varchar
	,"pdf" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_layout');
drop foreign table if exists odoo_report_open_items cascade;
create FOREIGN TABLE odoo_report_open_items("id" int4 NOT NULL
	,"date_at" date
	,"only_posted_moves" bool
	,"hide_account_at_0" bool
	,"foreign_currency" bool
	,"company_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_open_items');
drop foreign table if exists odoo_report_open_items_account cascade;
create FOREIGN TABLE odoo_report_open_items_account("id" int4 NOT NULL
	,"report_id" int4
	,"account_id" int4
	,"code" varchar
	,"name" varchar
	,"currency_id" int4
	,"final_amount_residual" numeric
	,"final_amount_total_due" numeric
	,"final_amount_residual_currency" numeric
	,"final_amount_total_due_currency" numeric
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_open_items_account');
drop foreign table if exists odoo_report_open_items_move_line cascade;
create FOREIGN TABLE odoo_report_open_items_move_line("id" int4 NOT NULL
	,"report_partner_id" int4
	,"move_line_id" int4
	,"date" date
	,"date_due" date
	,"entry" varchar
	,"journal" varchar
	,"account" varchar
	,"partner" varchar
	,"label" varchar
	,"amount_total_due" numeric
	,"amount_residual" numeric
	,"currency_id" int4
	,"amount_total_due_currency" numeric
	,"amount_residual_currency" numeric
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_open_items_move_line');
drop foreign table if exists odoo_report_open_items_partner cascade;
create FOREIGN TABLE odoo_report_open_items_partner("id" int4 NOT NULL
	,"report_account_id" int4
	,"partner_id" int4
	,"name" varchar
	,"currency_id" int4
	,"final_amount_residual" numeric
	,"final_amount_total_due" numeric
	,"final_amount_residual_currency" numeric
	,"final_amount_total_due_currency" numeric
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_open_items_partner');
drop foreign table if exists odoo_report_open_items_res_partner_rel cascade;
create FOREIGN TABLE odoo_report_open_items_res_partner_rel("report_open_items_id" int4 NOT NULL
	,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_open_items_res_partner_rel');
drop foreign table if exists odoo_report_paperformat cascade;
create FOREIGN TABLE odoo_report_paperformat("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"default" bool
	,"format" varchar
	,"margin_top" float8
	,"margin_bottom" float8
	,"margin_left" float8
	,"margin_right" float8
	,"page_height" int4
	,"page_width" int4
	,"orientation" varchar
	,"header_line" bool
	,"header_spacing" int4
	,"dpi" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_paperformat');
drop foreign table if exists odoo_report_stock_forecast cascade;
create FOREIGN TABLE odoo_report_stock_forecast("id" int4
	,"product_id" int4
	,"date" text
	,"quantity" float8
	,"cumulative_quantity" float8
	,"company_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_stock_forecast');
drop foreign table if exists odoo_report_trial_balance cascade;
create FOREIGN TABLE odoo_report_trial_balance("id" int4 NOT NULL
	,"date_from" date
	,"date_to" date
	,"fy_start_date" date
	,"only_posted_moves" bool
	,"hide_account_at_0" bool
	,"foreign_currency" bool
	,"company_id" int4
	,"show_partner_details" bool
	,"hierarchy_on" varchar NOT NULL
	,"limit_hierarchy_level" bool
	,"show_hierarchy_level" int4
	,"hide_parent_hierarchy_level" bool
	,"general_ledger_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_trial_balance');
drop foreign table if exists odoo_report_trial_balance_account cascade;
create FOREIGN TABLE odoo_report_trial_balance_account("id" int4 NOT NULL
	,"report_id" int4
	,"sequence" varchar
	,"level" int4
	,"account_id" int4
	,"account_group_id" int4
	,"parent_id" int4
	,"child_account_ids" varchar
	,"code" varchar
	,"name" varchar
	,"currency_id" int4
	,"initial_balance" numeric
	,"initial_balance_foreign_currency" numeric
	,"debit" numeric
	,"credit" numeric
	,"period_balance" numeric
	,"final_balance" numeric
	,"final_balance_foreign_currency" numeric
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_trial_balance_account');
drop foreign table if exists odoo_report_trial_balance_partner cascade;
create FOREIGN TABLE odoo_report_trial_balance_partner("id" int4 NOT NULL
	,"report_account_id" int4
	,"partner_id" int4
	,"name" varchar
	,"currency_id" int4
	,"initial_balance" numeric
	,"initial_balance_foreign_currency" numeric
	,"debit" numeric
	,"credit" numeric
	,"period_balance" numeric
	,"final_balance" numeric
	,"final_balance_foreign_currency" numeric
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_trial_balance_partner');
drop foreign table if exists odoo_report_trial_balance_res_partner_rel cascade;
create FOREIGN TABLE odoo_report_trial_balance_res_partner_rel("report_trial_balance_id" int4 NOT NULL
	,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_trial_balance_res_partner_rel');
drop foreign table if exists odoo_report_vat_report cascade;
create FOREIGN TABLE odoo_report_vat_report("id" int4 NOT NULL
	,"company_id" int4
	,"date_from" date
	,"date_to" date
	,"based_on" varchar NOT NULL
	,"tax_detail" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_vat_report');
drop foreign table if exists odoo_report_vat_report_tax cascade;
create FOREIGN TABLE odoo_report_vat_report_tax("id" int4 NOT NULL
	,"report_tax_id" int4
	,"tax_id" int4
	,"code" varchar
	,"name" varchar
	,"net" numeric
	,"tax" numeric
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_vat_report_tax');
drop foreign table if exists odoo_report_vat_report_taxtag cascade;
create FOREIGN TABLE odoo_report_vat_report_taxtag("id" int4 NOT NULL
	,"report_id" int4
	,"taxtag_id" int4
	,"taxgroup_id" int4
	,"code" varchar
	,"name" varchar
	,"net" numeric
	,"tax" numeric
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'report_vat_report_taxtag');
drop foreign table if exists odoo_res_bank cascade;
create FOREIGN TABLE odoo_res_bank("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"street" varchar
	,"street2" varchar
	,"zip" varchar
	,"city" varchar
	,"state" int4
	,"country" int4
	,"email" varchar
	,"phone" varchar
	,"active" bool
	,"bic" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"code" varchar
	,"lname" varchar(128)
	,"vat" varchar(32)
	,"website" varchar(64))SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'res_bank');
drop foreign table if exists odoo_res_company cascade;
create FOREIGN TABLE odoo_res_company("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"partner_id" int4 NOT NULL
	,"currency_id" int4 NOT NULL
	,"sequence" int4
	,"create_date" timestamp
	,"parent_id" int4
	,"report_header" text
	,"report_footer" text
	,"logo_web" bytea
	,"account_no" varchar
	,"email" varchar
	,"phone" varchar
	,"company_registry" varchar
	,"paperformat_id" int4
	,"external_report_layout_id" int4
	,"base_onboarding_company_state" varchar
	,"create_uid" int4
	,"write_uid" int4
	,"write_date" timestamp
	,"resource_calendar_id" int4
	,"social_twitter" varchar
	,"social_facebook" varchar
	,"social_github" varchar
	,"social_linkedin" varchar
	,"social_youtube" varchar
	,"social_googleplus" varchar
	,"social_instagram" varchar
	,"nomenclature_id" int4
	,"favicon_backend" bytea
	,"favicon_backend_mimetype" varchar
	,"propagation_minimum_delta" int4
	,"internal_transit_location_id" int4
	,"fiscalyear_last_day" int4 NOT NULL
	,"fiscalyear_last_month" int4 NOT NULL
	,"period_lock_date" date
	,"fiscalyear_lock_date" date
	,"transfer_account_id" int4
	,"expects_chart_of_accounts" bool
	,"chart_template_id" int4
	,"bank_account_code_prefix" varchar
	,"cash_account_code_prefix" varchar
	,"transfer_account_code_prefix" varchar
	,"account_sale_tax_id" int4
	,"account_purchase_tax_id" int4
	,"tax_cash_basis_journal_id" int4
	,"tax_calculation_rounding_method" varchar
	,"currency_exchange_journal_id" int4
	,"anglo_saxon_accounting" bool
	,"property_stock_account_input_categ_id" int4
	,"property_stock_account_output_categ_id" int4
	,"property_stock_valuation_account_id" int4
	,"overdue_msg" text
	,"tax_exigibility" bool
	,"account_bank_reconciliation_start" date
	,"incoterm_id" int4
	,"invoice_reference_type" varchar
	,"qr_code" bool
	,"invoice_is_email" bool
	,"invoice_is_print" bool
	,"account_opening_move_id" int4
	,"account_setup_bank_data_state" varchar
	,"account_setup_fy_data_state" varchar
	,"account_setup_coa_state" varchar
	,"account_onboarding_invoice_layout_state" varchar
	,"account_onboarding_sample_invoice_state" varchar
	,"account_onboarding_sale_tax_state" varchar
	,"account_invoice_onboarding_state" varchar
	,"account_dashboard_onboarding_state" varchar
	,"vat_check_vies" bool
	,"create_new_line_at_contract_line_renew" bool
	,"payment_acquirer_onboarding_state" varchar
	,"payment_onboarding_payment_method" varchar
	,"sale_note" text
	,"portal_confirmation_sign" bool
	,"portal_confirmation_pay" bool
	,"quotation_validity_days" int4
	,"sale_quotation_onboarding_state" varchar
	,"sale_onboarding_order_confirmation_state" varchar
	,"sale_onboarding_sample_quotation_state" varchar
	,"sale_onboarding_payment_method" varchar
	,"coop_email_contact" varchar
	,"subscription_maximum_amount" float8
	,"default_country_id" int4
	,"default_lang_id" int4
	,"allow_id_card_upload" bool
	,"create_user" bool
	,"board_representative" varchar
	,"signature_scan" bytea
	,"unmix_share_type" bool
	,"display_logo1" bool
	,"display_logo2" bool
	,"bottom_logo1" bytea
	,"bottom_logo2" bytea
	,"display_data_policy_approval" bool
	,"data_policy_approval_required" bool
	,"data_policy_approval_text" text
	,"display_internal_rules_approval" bool
	,"internal_rules_approval_required" bool
	,"internal_rules_approval_text" text
	,"display_financial_risk_approval" bool
	,"financial_risk_approval_required" bool
	,"financial_risk_approval_text" text
	,"security_lead" float8 NOT NULL
	,"initiating_party_issuer" varchar(35)
	,"initiating_party_identifier" varchar(35)
	,"initiating_party_scheme" varchar(35)
	,"sepa_creditor_identifier" varchar(35)
	,"send_certificate_email" bool
	,"send_confirmation_email" bool
	,"send_capital_release_email" bool
	,"tax_agency_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'res_company');
drop foreign table if exists odoo_res_company_users_rel cascade;
create FOREIGN TABLE odoo_res_company_users_rel("cid" int4 NOT NULL
	,"user_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'res_company_users_rel');
drop foreign table if exists odoo_res_config cascade;
create FOREIGN TABLE odoo_res_config("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'res_config');
drop foreign table if exists odoo_res_config_installer cascade;
create FOREIGN TABLE odoo_res_config_installer("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'res_config_installer');
drop foreign table if exists odoo_res_config_settings cascade;
create FOREIGN TABLE odoo_res_config_settings("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"group_multi_company" bool
	,"company_id" int4 NOT NULL
	,"user_default_rights" bool
	,"external_email_server_default" bool
	,"module_base_import" bool
	,"module_google_calendar" bool
	,"module_google_drive" bool
	,"module_google_spreadsheet" bool
	,"module_auth_oauth" bool
	,"module_auth_ldap" bool
	,"module_base_gengo" bool
	,"module_inter_company_rules" bool
	,"module_pad" bool
	,"module_voip" bool
	,"module_web_unsplash" bool
	,"module_partner_autocomplete" bool
	,"company_share_partner" bool
	,"group_multi_currency" bool
	,"show_effect" bool
	,"fail_counter" int4
	,"alias_domain" varchar
	,"partner_names_order" varchar NOT NULL
	,"unsplash_access_key" varchar
	,"auth_signup_reset_password" bool
	,"auth_signup_uninvited" varchar
	,"auth_signup_template_user_id" int4
	,"company_share_product" bool
	,"group_uom" bool
	,"group_product_variant" bool
	,"group_stock_packaging" bool
	,"group_sale_pricelist" bool
	,"group_product_pricelist" bool
	,"group_pricelist_item" bool
	,"product_weight_in_lbs" varchar
	,"group_mass_mailing_campaign" bool
	,"mass_mailing_outgoing_mail_server" bool
	,"mass_mailing_mail_server_id" int4
	,"show_blacklist_buttons" bool
	,"module_procurement_jit" int4
	,"module_product_expiry" bool
	,"group_stock_production_lot" bool
	,"group_lot_on_delivery_slip" bool
	,"group_stock_tracking_lot" bool
	,"group_stock_tracking_owner" bool
	,"group_stock_adv_location" bool
	,"group_warning_stock" bool
	,"use_propagation_minimum_delta" bool
	,"module_stock_picking_batch" bool
	,"module_stock_barcode" bool
	,"module_delivery_dhl" bool
	,"module_delivery_fedex" bool
	,"module_delivery_ups" bool
	,"module_delivery_usps" bool
	,"module_delivery_bpost" bool
	,"module_delivery_easypost" bool
	,"group_stock_multi_locations" bool
	,"group_stock_multi_warehouses" bool
	,"digest_emails" bool
	,"digest_id" int4
	,"chart_template_id" int4
	,"module_account_accountant" bool
	,"group_analytic_accounting" bool
	,"group_analytic_tags" bool
	,"group_warning_account" bool
	,"group_cash_rounding" bool
	,"group_fiscal_year" bool
	,"group_show_line_subtotals_tax_excluded" bool
	,"group_show_line_subtotals_tax_included" bool
	,"group_products_in_bills" bool
	,"show_line_subtotals_tax_selection" varchar NOT NULL
	,"module_account_asset" bool
	,"module_account_deferred_revenue" bool
	,"module_account_budget" bool
	,"module_account_payment" bool
	,"module_account_reports" bool
	,"module_account_reports_followup" bool
	,"module_account_check_printing" bool
	,"module_account_batch_payment" bool
	,"module_account_sepa" bool
	,"module_account_sepa_direct_debit" bool
	,"module_account_plaid" bool
	,"module_account_yodlee" bool
	,"module_account_bank_statement_import_qif" bool
	,"module_account_bank_statement_import_ofx" bool
	,"module_account_bank_statement_import_csv" bool
	,"module_account_bank_statement_import_camt" bool
	,"module_currency_rate_live" bool
	,"module_account_intrastat" bool
	,"module_product_margin" bool
	,"module_l10n_eu_service" bool
	,"module_account_taxcloud" bool
	,"module_account_invoice_extract" bool
	,"crm_alias_prefix" varchar
	,"generate_lead_from_alias" bool
	,"group_use_lead" bool
	,"module_crm_phone_validation" bool
	,"module_crm_reveal" bool
	,"module_account_asset_management" bool
	,"module_stock_landed_costs" bool
	,"use_sale_note" bool
	,"group_discount_per_so_line" bool
	,"module_sale_margin" bool
	,"use_quotation_validity_days" bool
	,"group_warning_sale" bool
	,"group_sale_delivery_address" bool
	,"multi_sales_price" bool
	,"multi_sales_price_method" varchar
	,"sale_pricelist_setting" varchar
	,"group_proforma_sales" bool
	,"group_sale_order_dates" bool
	,"default_invoice_policy" varchar
	,"deposit_default_product_id" int4
	,"auto_done_setting" bool
	,"module_website_sale_digital" bool
	,"module_delivery" bool
	,"module_product_email_template" bool
	,"module_sale_coupon" bool
	,"automatic_invoice" bool
	,"template_id" int4
	,"group_sale_order_template" bool
	,"default_sale_order_template_id" int4
	,"module_sale_quotation_builder" bool
	,"group_route_so_lines" bool
	,"group_display_incoterm" bool
	,"use_security_lead" bool
	,"default_picking_policy" varchar NOT NULL
	,"group_pain_multiple_identifier" bool)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'res_config_settings');
drop foreign table if exists odoo_res_country cascade;
create FOREIGN TABLE odoo_res_country("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"code" varchar(2)
	,"address_format" text
	,"address_view_id" int4
	,"currency_id" int4
	,"phone_code" int4
	,"name_position" varchar
	,"vat_label" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'res_country');
drop foreign table if exists odoo_res_country_group cascade;
create FOREIGN TABLE odoo_res_country_group("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'res_country_group');
drop foreign table if exists odoo_res_country_group_pricelist_rel cascade;
create FOREIGN TABLE odoo_res_country_group_pricelist_rel("pricelist_id" int4 NOT NULL
	,"res_country_group_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'res_country_group_pricelist_rel');
drop foreign table if exists odoo_res_country_res_country_group_rel cascade;
create FOREIGN TABLE odoo_res_country_res_country_group_rel("res_country_id" int4 NOT NULL
	,"res_country_group_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'res_country_res_country_group_rel');
drop foreign table if exists odoo_res_country_state cascade;
create FOREIGN TABLE odoo_res_country_state("id" int4 NOT NULL
	,"country_id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"code" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"aeat_code" varchar)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'res_country_state');
drop foreign table if exists odoo_res_currency cascade;
create FOREIGN TABLE odoo_res_currency("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"symbol" varchar NOT NULL
	,"rounding" numeric
	,"decimal_places" int4
	,"active" bool
	,"position" varchar
	,"currency_unit_label" varchar
	,"currency_subunit_label" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'res_currency');
drop foreign table if exists odoo_res_currency_rate cascade;
create FOREIGN TABLE odoo_res_currency_rate("id" int4 NOT NULL
	,"name" date NOT NULL
	,"rate" numeric
	,"currency_id" int4
	,"company_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'res_currency_rate');
drop foreign table if exists odoo_res_groups cascade;
create FOREIGN TABLE odoo_res_groups("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"comment" text
	,"category_id" int4
	,"color" int4
	,"share" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'res_groups');
drop foreign table if exists odoo_res_groups_implied_rel cascade;
create FOREIGN TABLE odoo_res_groups_implied_rel("gid" int4 NOT NULL
	,"hid" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'res_groups_implied_rel');
drop foreign table if exists odoo_res_groups_report_rel cascade;
create FOREIGN TABLE odoo_res_groups_report_rel("uid" int4 NOT NULL
	,"gid" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'res_groups_report_rel');
drop foreign table if exists odoo_res_groups_users_rel cascade;
create FOREIGN TABLE odoo_res_groups_users_rel("gid" int4 NOT NULL
	,"uid" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'res_groups_users_rel');
drop foreign table if exists odoo_res_lang cascade;
create FOREIGN TABLE odoo_res_lang("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"code" varchar NOT NULL
	,"iso_code" varchar
	,"translatable" bool
	,"active" bool
	,"direction" varchar NOT NULL
	,"date_format" varchar NOT NULL
	,"time_format" varchar NOT NULL
	,"week_start" int4 NOT NULL
	,"grouping" varchar NOT NULL
	,"decimal_point" varchar NOT NULL
	,"thousands_sep" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'res_lang');
drop foreign table if exists odoo_res_partner cascade;
create FOREIGN TABLE odoo_res_partner("id" int4 NOT NULL
	,"name" varchar
	,"company_id" int4
	,"create_date" timestamp
	,"display_name" varchar
	,"date" date
	,"title" int4
	,"parent_id" int4
	,"ref" varchar
	,"lang" varchar
	,"tz" varchar
	,"user_id" int4
	,"vat" varchar
	,"website" varchar
	,"comment" text
	,"credit_limit" float8
	,"barcode" varchar
	,"active" bool
	,"customer" bool
	,"supplier" bool
	,"employee" bool
	,"function" varchar
	,"type" varchar
	,"street" varchar
	,"street2" varchar
	,"zip" varchar
	,"city" varchar
	,"state_id" int4
	,"country_id" int4
	,"email" varchar
	,"phone" varchar
	,"mobile" varchar
	,"is_company" bool
	,"industry_id" int4
	,"color" int4
	,"partner_share" bool
	,"commercial_partner_id" int4
	,"commercial_company_name" varchar
	,"company_name" varchar
	,"create_uid" int4
	,"write_uid" int4
	,"write_date" timestamp
	,"birthdate_date" date
	,"message_main_attachment_id" int4
	,"message_bounce" int4
	,"firstname" varchar
	,"lastname" varchar
	,"signup_token" varchar
	,"signup_type" varchar
	,"signup_expiration" timestamp
	,"calendar_last_notif_ack" timestamp
	,"team_id" int4
	,"priority_id" int4
	,"picking_warn" varchar
	,"picking_warn_msg" text
	,"mass_mailing_contacts_count" int4
	,"mass_mailing_stats_count" int4
	,"debit_limit" numeric
	,"last_time_entries_checked" timestamp
	,"invoice_warn" varchar
	,"invoice_warn_msg" text
	,"sale_warn" varchar
	,"sale_warn_msg" text
	,"cooperator" bool
	,"member" bool
	,"coop_candidate" bool
	,"old_member" bool
	,"gender" varchar
	,"cooperator_register_number" int4
	,"company_register_number" varchar
	,"cooperator_type" varchar
	,"effective_date" date
	,"representative" bool
	,"representative_of_member_company" bool
	,"legal_form" varchar
	,"data_policy_approved" bool
	,"internal_rules_approved" bool
	,"financial_risk_approved" bool
	,"aeat_anonymous_cash_customer" bool
	,"comercial" varchar(128)
	,"_api_external_id" int4
	,"external_id_sequence_id" int4
	,"sponsor_id" int4
	,"coop_sponsee" bool
	,"incluir_190" bool
	,"aeat_perception_key_id" int4
	,"aeat_perception_subkey_id" int4
	,"representante_legal_vat" varchar(9)
	,"a_nacimiento" varchar(4)
	,"ceuta_melilla" varchar(1)
	,"situacion_familiar" varchar
	,"nif_conyuge" varchar(15)
	,"discapacidad" varchar
	,"contrato_o_relacion" varchar
	,"movilidad_geografica" varchar
	,"hijos_y_descendientes_m" int4
	,"hijos_y_descendientes_m_entero" int4
	,"hijos_y_descendientes" int4
	,"hijos_y_descendientes_entero" int4
	,"hijos_y_desc_discapacidad_mr" int4
	,"hijos_y_desc_discapacidad_entero_mr" int4
	,"hijos_y_desc_discapacidad_33" int4
	,"hijos_y_desc_discapacidad_entero_33" int4
	,"hijos_y_desc_discapacidad_66" int4
	,"hijos_y_desc_discapacidad_entero_66" int4
	,"ascendientes" int4
	,"ascendientes_entero" int4
	,"ascendientes_m75" int4
	,"ascendientes_entero_m75" int4
	,"ascendientes_discapacidad_33" int4
	,"ascendientes_discapacidad_entero_33" int4
	,"ascendientes_discapacidad_mr" int4
	,"ascendientes_discapacidad_entero_mr" int4
	,"ascendientes_discapacidad_66" int4
	,"ascendientes_discapacidad_entero_66" int4
	,"computo_primeros_hijos_1" int4
	,"computo_primeros_hijos_2" int4
	,"computo_primeros_hijos_3" int4
	,"coop_agreement_id" int4
	,"coop_agreement" bool
	,"cooperator_end_date" date
	,"volunteer" bool
	,"nationality" int4
	,"full_street" varchar
	,"sc_effective_date" date
	,"sc_cooperator_end_date" date
	,"sc_cooperator_register_number" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'res_partner');
drop foreign table if exists odoo_res_partner_bank cascade;
create FOREIGN TABLE odoo_res_partner_bank("id" int4 NOT NULL
	,"acc_number" varchar NOT NULL
	,"sanitized_acc_number" varchar
	,"acc_holder_name" varchar
	,"partner_id" int4 NOT NULL
	,"bank_id" int4
	,"sequence" int4
	,"currency_id" int4
	,"company_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"acc_type" varchar)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'res_partner_bank');
drop foreign table if exists odoo_res_partner_category cascade;
create FOREIGN TABLE odoo_res_partner_category("id" int4 NOT NULL
	,"parent_path" varchar
	,"name" varchar NOT NULL
	,"color" int4
	,"parent_id" int4
	,"active" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'res_partner_category');
drop foreign table if exists odoo_res_partner_industry cascade;
create FOREIGN TABLE odoo_res_partner_industry("id" int4 NOT NULL
	,"name" varchar
	,"full_name" varchar
	,"active" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'res_partner_industry');
drop foreign table if exists odoo_res_partner_res_partner_category_rel cascade;
create FOREIGN TABLE odoo_res_partner_res_partner_category_rel("category_id" int4 NOT NULL
	,"partner_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'res_partner_res_partner_category_rel');
drop foreign table if exists odoo_res_partner_title cascade;
create FOREIGN TABLE odoo_res_partner_title("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"shortcut" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'res_partner_title');
drop foreign table if exists odoo_res_partner_trial_balance_report_wizard_rel cascade;
create FOREIGN TABLE odoo_res_partner_trial_balance_report_wizard_rel("trial_balance_report_wizard_id" int4 NOT NULL
	,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'res_partner_trial_balance_report_wizard_rel');
drop foreign table if exists odoo_res_users cascade;
create FOREIGN TABLE odoo_res_users("id" int4 NOT NULL
	,"active" bool
	,"login" varchar NOT NULL
	,"password" varchar
	,"company_id" int4 NOT NULL
	,"partner_id" int4 NOT NULL
	,"create_date" timestamp
	,"signature" text
	,"action_id" int4
	,"share" bool
	,"create_uid" int4
	,"write_uid" int4
	,"write_date" timestamp
	,"alias_id" int4
	,"notification_type" varchar NOT NULL
	,"odoobot_state" varchar NOT NULL
	,"sale_team_id" int4
	,"chatter_position" varchar
	,"target_sales_won" int4
	,"target_sales_done" int4
	,"target_sales_invoiced" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'res_users');
drop foreign table if exists odoo_res_users_log cascade;
create FOREIGN TABLE odoo_res_users_log("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'res_users_log');
drop foreign table if exists odoo_resource_calendar cascade;
create FOREIGN TABLE odoo_resource_calendar("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"company_id" int4
	,"hours_per_day" float8
	,"tz" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'resource_calendar');
drop foreign table if exists odoo_resource_calendar_attendance cascade;
create FOREIGN TABLE odoo_resource_calendar_attendance("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"dayofweek" varchar NOT NULL
	,"date_from" date
	,"date_to" date
	,"hour_from" float8 NOT NULL
	,"hour_to" float8 NOT NULL
	,"calendar_id" int4 NOT NULL
	,"day_period" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'resource_calendar_attendance');
drop foreign table if exists odoo_resource_calendar_leaves cascade;
create FOREIGN TABLE odoo_resource_calendar_leaves("id" int4 NOT NULL
	,"name" varchar
	,"company_id" int4
	,"calendar_id" int4
	,"date_from" timestamp NOT NULL
	,"date_to" timestamp NOT NULL
	,"resource_id" int4
	,"time_type" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'resource_calendar_leaves');
drop foreign table if exists odoo_resource_resource cascade;
create FOREIGN TABLE odoo_resource_resource("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"active" bool
	,"company_id" int4
	,"resource_type" varchar NOT NULL
	,"user_id" int4
	,"time_efficiency" float8 NOT NULL
	,"calendar_id" int4 NOT NULL
	,"tz" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'resource_resource');
drop foreign table if exists odoo_resource_test cascade;
create FOREIGN TABLE odoo_resource_test("id" int4 NOT NULL
	,"name" varchar
	,"resource_id" int4 NOT NULL
	,"company_id" int4
	,"resource_calendar_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'resource_test');
drop foreign table if exists odoo_rule_group_rel cascade;
create FOREIGN TABLE odoo_rule_group_rel("rule_group_id" int4 NOT NULL
	,"group_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'rule_group_rel');
drop foreign table if exists odoo_sale_advance_payment_inv cascade;
create FOREIGN TABLE odoo_sale_advance_payment_inv("id" int4 NOT NULL
	,"advance_payment_method" varchar NOT NULL
	,"product_id" int4
	,"count" int4
	,"amount" numeric
	,"deposit_account_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'sale_advance_payment_inv');
drop foreign table if exists odoo_sale_order cascade;
create FOREIGN TABLE odoo_sale_order("id" int4 NOT NULL
	,"message_main_attachment_id" int4
	,"access_token" varchar
	,"name" varchar NOT NULL
	,"origin" varchar
	,"client_order_ref" varchar
	,"reference" varchar
	,"state" varchar
	,"date_order" timestamp NOT NULL
	,"validity_date" date
	,"require_signature" bool
	,"require_payment" bool
	,"create_date" timestamp
	,"confirmation_date" timestamp
	,"user_id" int4
	,"partner_id" int4 NOT NULL
	,"partner_invoice_id" int4 NOT NULL
	,"partner_shipping_id" int4 NOT NULL
	,"pricelist_id" int4 NOT NULL
	,"analytic_account_id" int4
	,"invoice_status" varchar
	,"note" text
	,"amount_untaxed" numeric
	,"amount_tax" numeric
	,"amount_total" numeric
	,"currency_rate" numeric
	,"payment_term_id" int4
	,"fiscal_position_id" int4
	,"company_id" int4
	,"team_id" int4
	,"signed_by" varchar
	,"commitment_date" timestamp
	,"create_uid" int4
	,"write_uid" int4
	,"write_date" timestamp
	,"sale_order_template_id" int4
	,"incoterm" int4
	,"picking_policy" varchar NOT NULL
	,"warehouse_id" int4 NOT NULL
	,"procurement_group_id" int4
	,"effective_date" date
	,"campaign_id" int4
	,"source_id" int4
	,"medium_id" int4
	,"opportunity_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'sale_order');
drop foreign table if exists odoo_sale_order_line cascade;
create FOREIGN TABLE odoo_sale_order_line("id" int4 NOT NULL
	,"order_id" int4 NOT NULL
	,"name" text NOT NULL
	,"sequence" int4
	,"invoice_status" varchar
	,"price_unit" numeric NOT NULL
	,"price_subtotal" numeric
	,"price_tax" float8
	,"price_total" numeric
	,"price_reduce" numeric
	,"price_reduce_taxinc" numeric
	,"price_reduce_taxexcl" numeric
	,"discount" numeric
	,"product_id" int4
	,"product_uom_qty" numeric NOT NULL
	,"product_uom" int4
	,"qty_delivered_method" varchar
	,"qty_delivered" numeric
	,"qty_delivered_manual" numeric
	,"qty_to_invoice" numeric
	,"qty_invoiced" numeric
	,"untaxed_amount_invoiced" numeric
	,"untaxed_amount_to_invoice" numeric
	,"salesman_id" int4
	,"currency_id" int4
	,"company_id" int4
	,"order_partner_id" int4
	,"is_expense" bool
	,"is_downpayment" bool
	,"state" varchar
	,"customer_lead" float8 NOT NULL
	,"display_type" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"product_packaging" int4
	,"route_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'sale_order_line');
drop foreign table if exists odoo_sale_order_line_invoice_rel cascade;
create FOREIGN TABLE odoo_sale_order_line_invoice_rel("invoice_line_id" int4 NOT NULL
	,"order_line_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'sale_order_line_invoice_rel');
drop foreign table if exists odoo_sale_order_option cascade;
create FOREIGN TABLE odoo_sale_order_option("id" int4 NOT NULL
	,"order_id" int4
	,"line_id" int4
	,"name" text NOT NULL
	,"product_id" int4 NOT NULL
	,"price_unit" numeric NOT NULL
	,"discount" numeric
	,"uom_id" int4 NOT NULL
	,"quantity" numeric NOT NULL
	,"sequence" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'sale_order_option');
drop foreign table if exists odoo_sale_order_tag_rel cascade;
create FOREIGN TABLE odoo_sale_order_tag_rel("order_id" int4 NOT NULL
	,"tag_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'sale_order_tag_rel');
drop foreign table if exists odoo_sale_order_template cascade;
create FOREIGN TABLE odoo_sale_order_template("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"note" text
	,"number_of_days" int4
	,"require_signature" bool
	,"require_payment" bool
	,"mail_template_id" int4
	,"active" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'sale_order_template');
drop foreign table if exists odoo_sale_order_template_line cascade;
create FOREIGN TABLE odoo_sale_order_template_line("id" int4 NOT NULL
	,"sequence" int4
	,"sale_order_template_id" int4 NOT NULL
	,"name" text NOT NULL
	,"product_id" int4
	,"price_unit" numeric NOT NULL
	,"discount" numeric
	,"product_uom_qty" numeric NOT NULL
	,"product_uom_id" int4
	,"display_type" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'sale_order_template_line');
drop foreign table if exists odoo_sale_order_template_option cascade;
create FOREIGN TABLE odoo_sale_order_template_option("id" int4 NOT NULL
	,"sale_order_template_id" int4 NOT NULL
	,"name" text NOT NULL
	,"product_id" int4 NOT NULL
	,"price_unit" numeric NOT NULL
	,"discount" numeric
	,"uom_id" int4 NOT NULL
	,"quantity" numeric NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'sale_order_template_option');
drop foreign table if exists odoo_sale_order_transaction_rel cascade;
create FOREIGN TABLE odoo_sale_order_transaction_rel("sale_order_id" int4 NOT NULL
	,"transaction_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'sale_order_transaction_rel');
drop foreign table if exists odoo_sale_payment_acquirer_onboarding_wizard cascade;
create FOREIGN TABLE odoo_sale_payment_acquirer_onboarding_wizard("id" int4 NOT NULL
	,"paypal_email_account" varchar
	,"paypal_seller_account" varchar
	,"paypal_pdt_token" varchar
	,"stripe_secret_key" varchar
	,"stripe_publishable_key" varchar
	,"manual_name" varchar
	,"journal_name" varchar
	,"acc_number" varchar
	,"manual_post_msg" text
	,"payment_method" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'sale_payment_acquirer_onboarding_wizard');
drop foreign table if exists odoo_sale_product_configurator cascade;
create FOREIGN TABLE odoo_sale_product_configurator("id" int4 NOT NULL
	,"product_template_id" int4 NOT NULL
	,"pricelist_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'sale_product_configurator');
drop foreign table if exists odoo_sale_report cascade;
create FOREIGN TABLE odoo_sale_report("id" int4
	,"product_id" int4
	,"product_uom" int4
	,"product_uom_qty" numeric
	,"qty_delivered" numeric
	,"qty_invoiced" numeric
	,"qty_to_invoice" numeric
	,"price_total" numeric
	,"price_subtotal" numeric
	,"untaxed_amount_to_invoice" numeric
	,"untaxed_amount_invoiced" numeric
	,"nbr" int8
	,"name" varchar
	,"date" timestamp
	,"confirmation_date" timestamp
	,"state" varchar
	,"partner_id" int4
	,"user_id" int4
	,"company_id" int4
	,"delay" float8
	,"categ_id" int4
	,"pricelist_id" int4
	,"analytic_account_id" int4
	,"team_id" int4
	,"product_tmpl_id" int4
	,"country_id" int4
	,"commercial_partner_id" int4
	,"weight" numeric
	,"volume" float8
	,"discount" numeric
	,"discount_amount" numeric
	,"order_id" int4
	,"warehouse_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'sale_report');
drop foreign table if exists odoo_server_config cascade;
create FOREIGN TABLE odoo_server_config("id" int4 NOT NULL
	,"config" text
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'server_config');
drop foreign table if exists odoo_service_supplier cascade;
create FOREIGN TABLE odoo_service_supplier("id" int4 NOT NULL
	,"name" varchar
	,"ref" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'service_supplier');
drop foreign table if exists odoo_service_technology cascade;
create FOREIGN TABLE odoo_service_technology("id" int4 NOT NULL
	,"name" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'service_technology');
drop foreign table if exists odoo_service_technology_service_supplier cascade;
create FOREIGN TABLE odoo_service_technology_service_supplier("id" int4 NOT NULL
	,"service_supplier_id" int4
	,"service_technology_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'service_technology_service_supplier');
drop foreign table if exists odoo_share_line cascade;
create FOREIGN TABLE odoo_share_line("id" int4 NOT NULL
	,"share_product_id" int4 NOT NULL
	,"share_number" int4 NOT NULL
	,"share_unit_price" numeric
	,"effective_date" date
	,"partner_id" int4 NOT NULL
	,"company_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'share_line');
drop foreign table if exists odoo_share_line_update_info cascade;
create FOREIGN TABLE odoo_share_line_update_info("id" int4 NOT NULL
	,"effective_date" date NOT NULL
	,"share_line" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'share_line_update_info');
drop foreign table if exists odoo_somconnexio_module cascade;
create FOREIGN TABLE odoo_somconnexio_module("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'somconnexio_module');
drop foreign table if exists odoo_sparse_fields_test cascade;
create FOREIGN TABLE odoo_sparse_fields_test("id" int4 NOT NULL
	,"data" text
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'sparse_fields_test');
drop foreign table if exists odoo_stock_backorder_confirmation cascade;
create FOREIGN TABLE odoo_stock_backorder_confirmation("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_backorder_confirmation');
drop foreign table if exists odoo_stock_change_product_qty cascade;
create FOREIGN TABLE odoo_stock_change_product_qty("id" int4 NOT NULL
	,"product_id" int4 NOT NULL
	,"product_tmpl_id" int4 NOT NULL
	,"new_quantity" numeric NOT NULL
	,"location_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_change_product_qty');
drop foreign table if exists odoo_stock_change_standard_price cascade;
create FOREIGN TABLE odoo_stock_change_standard_price("id" int4 NOT NULL
	,"new_price" numeric NOT NULL
	,"counterpart_account_id" int4
	,"counterpart_account_id_required" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_change_standard_price');
drop foreign table if exists odoo_stock_fixed_putaway_strat cascade;
create FOREIGN TABLE odoo_stock_fixed_putaway_strat("id" int4 NOT NULL
	,"product_id" int4
	,"putaway_id" int4 NOT NULL
	,"category_id" int4
	,"fixed_location_id" int4 NOT NULL
	,"sequence" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_fixed_putaway_strat');
drop foreign table if exists odoo_stock_immediate_transfer cascade;
create FOREIGN TABLE odoo_stock_immediate_transfer("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_immediate_transfer');
drop foreign table if exists odoo_stock_inventory cascade;
create FOREIGN TABLE odoo_stock_inventory("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"date" timestamp NOT NULL
	,"state" varchar
	,"company_id" int4 NOT NULL
	,"location_id" int4 NOT NULL
	,"product_id" int4
	,"package_id" int4
	,"partner_id" int4
	,"lot_id" int4
	,"filter" varchar NOT NULL
	,"category_id" int4
	,"exhausted" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"accounting_date" date)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_inventory');
drop foreign table if exists odoo_stock_inventory_line cascade;
create FOREIGN TABLE odoo_stock_inventory_line("id" int4 NOT NULL
	,"inventory_id" int4
	,"partner_id" int4
	,"product_id" int4 NOT NULL
	,"product_uom_id" int4 NOT NULL
	,"product_qty" numeric
	,"location_id" int4 NOT NULL
	,"package_id" int4
	,"prod_lot_id" int4
	,"company_id" int4
	,"theoretical_qty" numeric
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_inventory_line');
drop foreign table if exists odoo_stock_location cascade;
create FOREIGN TABLE odoo_stock_location("id" int4 NOT NULL
	,"parent_path" varchar
	,"name" varchar NOT NULL
	,"complete_name" varchar
	,"active" bool
	,"usage" varchar NOT NULL
	,"location_id" int4
	,"partner_id" int4
	,"comment" text
	,"posx" int4
	,"posy" int4
	,"posz" int4
	,"company_id" int4
	,"scrap_location" bool
	,"return_location" bool
	,"removal_strategy_id" int4
	,"putaway_strategy_id" int4
	,"barcode" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"valuation_in_account_id" int4
	,"valuation_out_account_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_location');
drop foreign table if exists odoo_stock_location_route cascade;
create FOREIGN TABLE odoo_stock_location_route("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"active" bool
	,"sequence" int4
	,"product_selectable" bool
	,"product_categ_selectable" bool
	,"warehouse_selectable" bool
	,"supplied_wh_id" int4
	,"supplier_wh_id" int4
	,"company_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"sale_selectable" bool)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_location_route');
drop foreign table if exists odoo_stock_location_route_categ cascade;
create FOREIGN TABLE odoo_stock_location_route_categ("route_id" int4 NOT NULL
	,"categ_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_location_route_categ');
drop foreign table if exists odoo_stock_location_route_move cascade;
create FOREIGN TABLE odoo_stock_location_route_move("move_id" int4 NOT NULL
	,"route_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_location_route_move');
drop foreign table if exists odoo_stock_location_route_stock_rules_report_rel cascade;
create FOREIGN TABLE odoo_stock_location_route_stock_rules_report_rel("stock_rules_report_id" int4 NOT NULL
	,"stock_location_route_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_location_route_stock_rules_report_rel');
drop foreign table if exists odoo_stock_move cascade;
create FOREIGN TABLE odoo_stock_move("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"sequence" int4
	,"priority" varchar
	,"create_date" timestamp
	,"date" timestamp NOT NULL
	,"company_id" int4 NOT NULL
	,"date_expected" timestamp NOT NULL
	,"product_id" int4 NOT NULL
	,"product_qty" numeric
	,"product_uom_qty" numeric NOT NULL
	,"product_uom" int4 NOT NULL
	,"product_packaging" int4
	,"location_id" int4 NOT NULL
	,"location_dest_id" int4 NOT NULL
	,"partner_id" int4
	,"picking_id" int4
	,"note" text
	,"state" varchar
	,"price_unit" float8
	,"origin" varchar
	,"procure_method" varchar NOT NULL
	,"scrapped" bool
	,"group_id" int4
	,"rule_id" int4
	,"propagate" bool
	,"picking_type_id" int4
	,"inventory_id" int4
	,"origin_returned_move_id" int4
	,"restrict_partner_id" int4
	,"warehouse_id" int4
	,"additional" bool
	,"reference" varchar
	,"package_level_id" int4
	,"create_uid" int4
	,"write_uid" int4
	,"write_date" timestamp
	,"to_refund" bool
	,"value" float8
	,"remaining_qty" float8
	,"remaining_value" float8
	,"sale_line_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_move');
drop foreign table if exists odoo_stock_move_line cascade;
create FOREIGN TABLE odoo_stock_move_line("id" int4 NOT NULL
	,"picking_id" int4
	,"move_id" int4
	,"product_id" int4
	,"product_uom_id" int4 NOT NULL
	,"product_qty" numeric
	,"product_uom_qty" numeric NOT NULL
	,"qty_done" numeric
	,"package_id" int4
	,"package_level_id" int4
	,"lot_id" int4
	,"lot_name" varchar
	,"result_package_id" int4
	,"date" timestamp NOT NULL
	,"owner_id" int4
	,"location_id" int4 NOT NULL
	,"location_dest_id" int4 NOT NULL
	,"state" varchar
	,"reference" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"lot_router_mac" varchar)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_move_line');
drop foreign table if exists odoo_stock_move_line_consume_rel cascade;
create FOREIGN TABLE odoo_stock_move_line_consume_rel("consume_line_id" int4 NOT NULL
	,"produce_line_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_move_line_consume_rel');
drop foreign table if exists odoo_stock_move_move_rel cascade;
create FOREIGN TABLE odoo_stock_move_move_rel("move_orig_id" int4 NOT NULL
	,"move_dest_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_move_move_rel');
drop foreign table if exists odoo_stock_overprocessed_transfer cascade;
create FOREIGN TABLE odoo_stock_overprocessed_transfer("id" int4 NOT NULL
	,"picking_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_overprocessed_transfer');
drop foreign table if exists odoo_stock_package_destination cascade;
create FOREIGN TABLE odoo_stock_package_destination("id" int4 NOT NULL
	,"picking_id" int4 NOT NULL
	,"location_dest_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_package_destination');
drop foreign table if exists odoo_stock_package_level cascade;
create FOREIGN TABLE odoo_stock_package_level("id" int4 NOT NULL
	,"package_id" int4 NOT NULL
	,"picking_id" int4
	,"location_dest_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_package_level');
drop foreign table if exists odoo_stock_picking cascade;
create FOREIGN TABLE odoo_stock_picking("id" int4 NOT NULL
	,"message_main_attachment_id" int4
	,"name" varchar
	,"origin" varchar
	,"note" text
	,"backorder_id" int4
	,"move_type" varchar NOT NULL
	,"state" varchar
	,"group_id" int4
	,"priority" varchar
	,"scheduled_date" timestamp
	,"date" timestamp
	,"date_done" timestamp
	,"location_id" int4 NOT NULL
	,"location_dest_id" int4 NOT NULL
	,"picking_type_id" int4 NOT NULL
	,"partner_id" int4
	,"company_id" int4 NOT NULL
	,"owner_id" int4
	,"printed" bool
	,"is_locked" bool
	,"immediate_transfer" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"sale_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_picking');
drop foreign table if exists odoo_stock_picking_backorder_rel cascade;
create FOREIGN TABLE odoo_stock_picking_backorder_rel("stock_backorder_confirmation_id" int4 NOT NULL
	,"stock_picking_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_picking_backorder_rel');
drop foreign table if exists odoo_stock_picking_transfer_rel cascade;
create FOREIGN TABLE odoo_stock_picking_transfer_rel("stock_immediate_transfer_id" int4 NOT NULL
	,"stock_picking_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_picking_transfer_rel');
drop foreign table if exists odoo_stock_picking_type cascade;
create FOREIGN TABLE odoo_stock_picking_type("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"color" int4
	,"sequence" int4
	,"sequence_id" int4 NOT NULL
	,"default_location_src_id" int4
	,"default_location_dest_id" int4
	,"code" varchar NOT NULL
	,"return_picking_type_id" int4
	,"show_entire_packs" bool
	,"warehouse_id" int4
	,"active" bool
	,"use_create_lots" bool
	,"use_existing_lots" bool
	,"show_operations" bool
	,"show_reserved" bool
	,"barcode" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_picking_type');
drop foreign table if exists odoo_stock_production_lot cascade;
create FOREIGN TABLE odoo_stock_production_lot("id" int4 NOT NULL
	,"message_main_attachment_id" int4
	,"name" varchar NOT NULL
	,"ref" varchar
	,"product_id" int4 NOT NULL
	,"product_uom_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"router_mac_address" varchar)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_production_lot');
drop foreign table if exists odoo_stock_quant cascade;
create FOREIGN TABLE odoo_stock_quant("id" int4 NOT NULL
	,"product_id" int4 NOT NULL
	,"company_id" int4
	,"location_id" int4 NOT NULL
	,"lot_id" int4
	,"package_id" int4
	,"owner_id" int4
	,"quantity" float8 NOT NULL
	,"reserved_quantity" float8 NOT NULL
	,"in_date" timestamp
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_quant');
drop foreign table if exists odoo_stock_quant_package cascade;
create FOREIGN TABLE odoo_stock_quant_package("id" int4 NOT NULL
	,"name" varchar
	,"packaging_id" int4
	,"location_id" int4
	,"company_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_quant_package');
drop foreign table if exists odoo_stock_quantity_history cascade;
create FOREIGN TABLE odoo_stock_quantity_history("id" int4 NOT NULL
	,"compute_at_date" int4
	,"date" timestamp
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_quantity_history');
drop foreign table if exists odoo_stock_return_picking cascade;
create FOREIGN TABLE odoo_stock_return_picking("id" int4 NOT NULL
	,"picking_id" int4
	,"move_dest_exists" bool
	,"original_location_id" int4
	,"parent_location_id" int4
	,"location_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_return_picking');
drop foreign table if exists odoo_stock_return_picking_line cascade;
create FOREIGN TABLE odoo_stock_return_picking_line("id" int4 NOT NULL
	,"product_id" int4 NOT NULL
	,"quantity" numeric NOT NULL
	,"wizard_id" int4
	,"move_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"to_refund" bool)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_return_picking_line');
drop foreign table if exists odoo_stock_route_product cascade;
create FOREIGN TABLE odoo_stock_route_product("route_id" int4 NOT NULL
	,"product_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_route_product');
drop foreign table if exists odoo_stock_route_warehouse cascade;
create FOREIGN TABLE odoo_stock_route_warehouse("route_id" int4 NOT NULL
	,"warehouse_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_route_warehouse');
drop foreign table if exists odoo_stock_rule cascade;
create FOREIGN TABLE odoo_stock_rule("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"active" bool
	,"group_propagation_option" varchar
	,"group_id" int4
	,"action" varchar NOT NULL
	,"sequence" int4
	,"company_id" int4
	,"location_id" int4 NOT NULL
	,"location_src_id" int4
	,"route_id" int4 NOT NULL
	,"procure_method" varchar NOT NULL
	,"route_sequence" int4
	,"picking_type_id" int4 NOT NULL
	,"delay" int4
	,"partner_address_id" int4
	,"propagate" bool
	,"warehouse_id" int4
	,"propagate_warehouse_id" int4
	,"auto" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_rule');
drop foreign table if exists odoo_stock_rules_report cascade;
create FOREIGN TABLE odoo_stock_rules_report("id" int4 NOT NULL
	,"product_id" int4 NOT NULL
	,"product_tmpl_id" int4 NOT NULL
	,"product_has_variants" bool NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_rules_report');
drop foreign table if exists odoo_stock_rules_report_stock_warehouse_rel cascade;
create FOREIGN TABLE odoo_stock_rules_report_stock_warehouse_rel("stock_rules_report_id" int4 NOT NULL
	,"stock_warehouse_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_rules_report_stock_warehouse_rel');
drop foreign table if exists odoo_stock_scheduler_compute cascade;
create FOREIGN TABLE odoo_stock_scheduler_compute("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_scheduler_compute');
drop foreign table if exists odoo_stock_scrap cascade;
create FOREIGN TABLE odoo_stock_scrap("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"origin" varchar
	,"product_id" int4 NOT NULL
	,"product_uom_id" int4 NOT NULL
	,"lot_id" int4
	,"package_id" int4
	,"owner_id" int4
	,"move_id" int4
	,"picking_id" int4
	,"location_id" int4 NOT NULL
	,"scrap_location_id" int4 NOT NULL
	,"scrap_qty" float8 NOT NULL
	,"state" varchar
	,"date_expected" timestamp
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_scrap');
drop foreign table if exists odoo_stock_traceability_report cascade;
create FOREIGN TABLE odoo_stock_traceability_report("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_traceability_report');
drop foreign table if exists odoo_stock_track_confirmation cascade;
create FOREIGN TABLE odoo_stock_track_confirmation("id" int4 NOT NULL
	,"inventory_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_track_confirmation');
drop foreign table if exists odoo_stock_track_line cascade;
create FOREIGN TABLE odoo_stock_track_line("id" int4 NOT NULL
	,"product_id" int4
	,"tracking" varchar
	,"wizard_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_track_line');
drop foreign table if exists odoo_stock_warehouse cascade;
create FOREIGN TABLE odoo_stock_warehouse("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"active" bool
	,"company_id" int4 NOT NULL
	,"partner_id" int4
	,"view_location_id" int4 NOT NULL
	,"lot_stock_id" int4 NOT NULL
	,"code" varchar(5) NOT NULL
	,"reception_steps" varchar NOT NULL
	,"delivery_steps" varchar NOT NULL
	,"wh_input_stock_loc_id" int4
	,"wh_qc_stock_loc_id" int4
	,"wh_output_stock_loc_id" int4
	,"wh_pack_stock_loc_id" int4
	,"mto_pull_id" int4
	,"pick_type_id" int4
	,"pack_type_id" int4
	,"out_type_id" int4
	,"in_type_id" int4
	,"int_type_id" int4
	,"crossdock_route_id" int4
	,"reception_route_id" int4
	,"delivery_route_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_warehouse');
drop foreign table if exists odoo_stock_warehouse_orderpoint cascade;
create FOREIGN TABLE odoo_stock_warehouse_orderpoint("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"active" bool
	,"warehouse_id" int4 NOT NULL
	,"location_id" int4 NOT NULL
	,"product_id" int4 NOT NULL
	,"product_min_qty" numeric NOT NULL
	,"product_max_qty" numeric NOT NULL
	,"qty_multiple" numeric NOT NULL
	,"group_id" int4
	,"company_id" int4 NOT NULL
	,"lead_days" int4
	,"lead_type" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_warehouse_orderpoint');
drop foreign table if exists odoo_stock_warn_insufficient_qty_scrap cascade;
create FOREIGN TABLE odoo_stock_warn_insufficient_qty_scrap("id" int4 NOT NULL
	,"scrap_id" int4
	,"product_id" int4 NOT NULL
	,"location_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_warn_insufficient_qty_scrap');
drop foreign table if exists odoo_stock_wh_resupply_table cascade;
create FOREIGN TABLE odoo_stock_wh_resupply_table("supplied_wh_id" int4 NOT NULL
	,"supplier_wh_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'stock_wh_resupply_table');
drop foreign table if exists odoo_subscription_register cascade;
create FOREIGN TABLE odoo_subscription_register("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"register_number_operation" int4 NOT NULL
	,"partner_id" int4 NOT NULL
	,"partner_id_to" int4
	,"date" date NOT NULL
	,"quantity" int4
	,"share_unit_price" numeric
	,"share_product_id" int4 NOT NULL
	,"share_to_product_id" int4
	,"quantity_to" int4
	,"share_to_unit_price" numeric
	,"type" varchar
	,"company_id" int4 NOT NULL
	,"user_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'subscription_register');
drop foreign table if exists odoo_subscription_request cascade;
create FOREIGN TABLE odoo_subscription_request("id" int4 NOT NULL
	,"already_cooperator" bool
	,"name" varchar NOT NULL
	,"firstname" varchar
	,"lastname" varchar
	,"birthdate" date
	,"gender" varchar
	,"type" varchar
	,"state" varchar NOT NULL
	,"email" varchar NOT NULL
	,"iban" varchar NOT NULL
	,"partner_id" int4
	,"share_product_id" int4
	,"ordered_parts" int4 NOT NULL
	,"address" varchar NOT NULL
	,"city" varchar NOT NULL
	,"zip_code" varchar NOT NULL
	,"country_id" int4 NOT NULL
	,"phone" varchar
	,"user_id" int4
	,"skip_control_ng" bool
	,"lang" varchar NOT NULL
	,"date" date NOT NULL
	,"company_id" int4 NOT NULL
	,"is_company" bool
	,"is_operation" bool
	,"company_name" varchar
	,"company_email" varchar
	,"company_register_number" varchar
	,"company_type" varchar
	,"same_address" bool
	,"activities_address" varchar
	,"activities_city" varchar
	,"activities_zip_code" varchar
	,"activities_country_id" int4
	,"contact_person_function" varchar
	,"operation_request_id" int4
	,"capital_release_request_date" date
	,"source" varchar
	,"data_policy_approved" bool
	,"internal_rules_approved" bool
	,"financial_risk_approved" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"_api_external_id" int4
	,"external_id_sequence_id" int4
	,"vat" varchar
	,"voluntary_contribution" numeric
	,"sponsor_id" int4
	,"coop_agreement_id" int4
	,"nationality" int4
	,"payment_type" varchar
	,"state_id" int4
	,"discovery_channel_id" int4
	,"verbose_name" varchar
	,"message_main_attachment_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'subscription_request');
drop foreign table if exists odoo_subscription_upgrade_sponsee cascade;
create FOREIGN TABLE odoo_subscription_upgrade_sponsee("id" int4 NOT NULL
	,"partner_id" int4 NOT NULL
	,"share_product_id" int4 NOT NULL
	,"ordered_parts" int4 NOT NULL
	,"start_date" date NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'subscription_upgrade_sponsee');
drop foreign table if exists odoo_tax_adjustments_wizard cascade;
create FOREIGN TABLE odoo_tax_adjustments_wizard("id" int4 NOT NULL
	,"reason" varchar NOT NULL
	,"journal_id" int4 NOT NULL
	,"date" date NOT NULL
	,"debit_account_id" int4 NOT NULL
	,"credit_account_id" int4 NOT NULL
	,"amount" numeric NOT NULL
	,"company_currency_id" int4
	,"tax_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'tax_adjustments_wizard');
drop foreign table if exists odoo_team_favorite_user_rel cascade;
create FOREIGN TABLE odoo_team_favorite_user_rel("team_id" int4 NOT NULL
	,"user_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'team_favorite_user_rel');
drop foreign table if exists odoo_trial_balance_report_wizard cascade;
create FOREIGN TABLE odoo_trial_balance_report_wizard("id" int4 NOT NULL
	,"company_id" int4
	,"date_range_id" int4
	,"date_from" date NOT NULL
	,"date_to" date NOT NULL
	,"target_move" varchar NOT NULL
	,"hierarchy_on" varchar NOT NULL
	,"limit_hierarchy_level" bool
	,"show_hierarchy_level" int4
	,"hide_parent_hierarchy_level" bool
	,"hide_account_at_0" bool
	,"receivable_accounts_only" bool
	,"payable_accounts_only" bool
	,"show_partner_details" bool
	,"not_only_one_unaffected_earnings_account" bool
	,"foreign_currency" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'trial_balance_report_wizard');
drop foreign table if exists odoo_uom_category cascade;
create FOREIGN TABLE odoo_uom_category("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"measure_type" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'uom_category');
drop foreign table if exists odoo_uom_uom cascade;
create FOREIGN TABLE odoo_uom_uom("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"category_id" int4 NOT NULL
	,"factor" numeric NOT NULL
	,"rounding" numeric NOT NULL
	,"active" bool
	,"uom_type" varchar NOT NULL
	,"measure_type" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'uom_uom');
drop foreign table if exists odoo_utm_campaign cascade;
create FOREIGN TABLE odoo_utm_campaign("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'utm_campaign');
drop foreign table if exists odoo_utm_medium cascade;
create FOREIGN TABLE odoo_utm_medium("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"active" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'utm_medium');
drop foreign table if exists odoo_utm_source cascade;
create FOREIGN TABLE odoo_utm_source("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'utm_source');
drop foreign table if exists odoo_validate_account_move cascade;
create FOREIGN TABLE odoo_validate_account_move("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'validate_account_move');
drop foreign table if exists odoo_validate_subscription_request cascade;
create FOREIGN TABLE odoo_validate_subscription_request("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'validate_subscription_request');
drop foreign table if exists odoo_vat_report_wizard cascade;
create FOREIGN TABLE odoo_vat_report_wizard("id" int4 NOT NULL
	,"company_id" int4
	,"date_range_id" int4
	,"date_from" date NOT NULL
	,"date_to" date NOT NULL
	,"based_on" varchar NOT NULL
	,"tax_detail" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'vat_report_wizard');
drop foreign table if exists odoo_vodafone_fiber_service_contract_info cascade;
create FOREIGN TABLE odoo_vodafone_fiber_service_contract_info("id" int4 NOT NULL
	,"vodafone_id" varchar NOT NULL
	,"vodafone_offer_code" varchar NOT NULL
	,"phone_number" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"id_order" varchar
	,"previous_id" varchar)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'vodafone_fiber_service_contract_info');
drop foreign table if exists odoo_web_editor_converter_test cascade;
create FOREIGN TABLE odoo_web_editor_converter_test("id" int4 NOT NULL
	,"char" varchar
	,"integer" int4
	,"float" float8
	,"numeric" numeric
	,"many2one" int4
	,"binary" bytea
	,"date" date
	,"datetime" timestamp
	,"selection" int4
	,"selection_str" varchar
	,"html" text
	,"text" text
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'web_editor_converter_test');
drop foreign table if exists odoo_web_editor_converter_test_sub cascade;
create FOREIGN TABLE odoo_web_editor_converter_test_sub("id" int4 NOT NULL
	,"name" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'web_editor_converter_test_sub');
drop foreign table if exists odoo_web_tour_tour cascade;
create FOREIGN TABLE odoo_web_tour_tour("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"user_id" int4)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'web_tour_tour');
drop foreign table if exists odoo_wizard_account_matching cascade;
create FOREIGN TABLE odoo_wizard_account_matching("id" int4 NOT NULL
	,"update_chart_wizard_id" int4 NOT NULL
	,"sequence" int4 NOT NULL
	,"matching_value" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'wizard_account_matching');
drop foreign table if exists odoo_wizard_fp_matching cascade;
create FOREIGN TABLE odoo_wizard_fp_matching("id" int4 NOT NULL
	,"update_chart_wizard_id" int4 NOT NULL
	,"sequence" int4 NOT NULL
	,"matching_value" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'wizard_fp_matching');
drop foreign table if exists odoo_wizard_ir_model_menu_create cascade;
create FOREIGN TABLE odoo_wizard_ir_model_menu_create("id" int4 NOT NULL
	,"menu_id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'wizard_ir_model_menu_create');
drop foreign table if exists odoo_wizard_matching cascade;
create FOREIGN TABLE odoo_wizard_matching("id" int4 NOT NULL
	,"update_chart_wizard_id" int4 NOT NULL
	,"sequence" int4 NOT NULL
	,"matching_value" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'wizard_matching');
drop foreign table if exists odoo_wizard_open_tax_balances cascade;
create FOREIGN TABLE odoo_wizard_open_tax_balances("id" int4 NOT NULL
	,"company_id" int4 NOT NULL
	,"from_date" date NOT NULL
	,"to_date" date NOT NULL
	,"date_range_id" int4
	,"target_move" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'wizard_open_tax_balances');
drop foreign table if exists odoo_wizard_tax_matching cascade;
create FOREIGN TABLE odoo_wizard_tax_matching("id" int4 NOT NULL
	,"update_chart_wizard_id" int4 NOT NULL
	,"sequence" int4 NOT NULL
	,"matching_value" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'wizard_tax_matching');
drop foreign table if exists odoo_wizard_update_charts_account_fields_rel cascade;
create FOREIGN TABLE odoo_wizard_update_charts_account_fields_rel("wizard_update_charts_accounts_id" int4 NOT NULL
	,"ir_model_fields_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'wizard_update_charts_account_fields_rel');
drop foreign table if exists odoo_wizard_update_charts_accounts cascade;
create FOREIGN TABLE odoo_wizard_update_charts_accounts("id" int4 NOT NULL
	,"state" varchar
	,"company_id" int4 NOT NULL
	,"chart_template_id" int4 NOT NULL
	,"lang" varchar NOT NULL
	,"update_tax" bool
	,"update_account" bool
	,"update_fiscal_position" bool
	,"continue_on_errors" bool
	,"recreate_xml_ids" bool
	,"rejected_new_account_number" int4
	,"rejected_updated_account_number" int4
	,"log" text
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'wizard_update_charts_accounts');
drop foreign table if exists odoo_wizard_update_charts_accounts_account cascade;
create FOREIGN TABLE odoo_wizard_update_charts_accounts_account("id" int4 NOT NULL
	,"account_id" int4 NOT NULL
	,"update_chart_wizard_id" int4 NOT NULL
	,"type" varchar
	,"update_account_id" int4
	,"notes" text
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'wizard_update_charts_accounts_account');
drop foreign table if exists odoo_wizard_update_charts_accounts_fiscal_position cascade;
create FOREIGN TABLE odoo_wizard_update_charts_accounts_fiscal_position("id" int4 NOT NULL
	,"fiscal_position_id" int4 NOT NULL
	,"update_chart_wizard_id" int4 NOT NULL
	,"type" varchar NOT NULL
	,"update_fiscal_position_id" int4
	,"notes" text
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'wizard_update_charts_accounts_fiscal_position');
drop foreign table if exists odoo_wizard_update_charts_accounts_tax cascade;
create FOREIGN TABLE odoo_wizard_update_charts_accounts_tax("id" int4 NOT NULL
	,"tax_id" int4
	,"update_chart_wizard_id" int4 NOT NULL
	,"type" varchar
	,"update_tax_id" int4
	,"notes" text
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'wizard_update_charts_accounts_tax');
drop foreign table if exists odoo_wizard_update_charts_fp_fields_rel cascade;
create FOREIGN TABLE odoo_wizard_update_charts_fp_fields_rel("wizard_update_charts_accounts_id" int4 NOT NULL
	,"ir_model_fields_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'wizard_update_charts_fp_fields_rel');
drop foreign table if exists odoo_wizard_update_charts_tax_fields_rel cascade;
create FOREIGN TABLE odoo_wizard_update_charts_tax_fields_rel("wizard_update_charts_accounts_id" int4 NOT NULL
	,"ir_model_fields_id" int4 NOT NULL)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'wizard_update_charts_tax_fields_rel');
drop foreign table if exists odoo_xoln_fiber_service_contract_info cascade;
create FOREIGN TABLE odoo_xoln_fiber_service_contract_info("id" int4 NOT NULL
	,"external_id" varchar NOT NULL
	,"project" varchar NOT NULL
	,"id_order" varchar
	,"router_product_id" int4 NOT NULL
	,"router_lot_id" int4 NOT NULL
	,"phone_number" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"previous_id" varchar)SERVER postgres_fdw_odoo12 OPTIONS (schema_name 'public', table_name 'xoln_fiber_service_contract_info');
