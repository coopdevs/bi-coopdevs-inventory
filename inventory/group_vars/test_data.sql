create database test1;

create table agg_vendes(
	id serial primary key,
	sales_date date,
	sales_group varchar(200),
	sales_subgroup varchar(200),
	volume int,
	unit_price numeric(9,2),
	total_price numeric(19,2)
);

create table aux_dims(
	lvl1 varchar(50), lvl2 varchar(50)
);
/*
insert into aux_dims values ('internet','vodafone');
insert into aux_dims values ('internet','jazztel');
insert into aux_dims values ('mòbil','vodafone');
insert into aux_dims values ('mòbil','vodafone');
insert into aux_dims values ('mòbil','Masmovil');
*/

/*
insert into aux_dims values ('zona1','cotxe1');
insert into aux_dims values ('zona1','cotxe2');
insert into aux_dims values ('zona2','cotxe1');
insert into aux_dims values ('zona2','cotxe2');
insert into aux_dims values ('zona2','cotxe3');
insert into aux_dims values ('zona3','cotxe1');
insert into aux_dims values ('zona3','cotxe2');
insert into aux_dims values ('zona3','cotxe3');
insert into aux_dims values ('zona4','cotxe1');
insert into aux_dims values ('zona4','cotxe2');
*/

insert into aux_dims values ('producte fresc','fruita ecològica');
insert into aux_dims values ('producte fresc','fruita no ecològica');
insert into aux_dims values ('producte sec','pasta');
insert into aux_dims values ('producte sec','cereals');
insert into aux_dims values ('producte sec','altres');
insert into aux_dims values ('drogueria','higiene personal');
insert into aux_dims values ('drogueria','neteja llar');
insert into aux_dims values ('drogueria','altres');


create table data( data date, dies_mes smallint);
insert into data (data)
select  cast('2009-12-31' as date) + ((rn||' days')::INTERVAL)
from (
select row_number() over (order by (select 1)) as rn from generate_series(1,5000) limit 5000
) a;

 update data set dies_mes=
    DATE_PART('days',
        DATE_TRUNC('month', data.data)
        + '1 MONTH'::INTERVAL
        - '1 DAY'::INTERVAL
    );

insert into agg_vendes (sales_Date, sales_group, sales_subgroup, volume)
select data."data", ad.lvl1 , ad.lvl2,  (random()* (15-0 + 1) + 0)::int
from aux_dims ad , data;

UPDATE agg_vendes SET unit_price = rnd
FROM (
select   DATE_TRUNC('month', sales_date) as sd, sales_group, sales_subgroup, (random()* (10-8 + 1) + 8) as rnd
from agg_vendes
) AS s
WHERE DATE_TRUNC('month', agg_vendes.sales_date)=sd
	and agg_vendes.sales_group=s.sales_group and agg_vendes.sales_subgroup=s.sales_subgroup;

UPDATE agg_vendes set total_price=volume*unit_price;




/*


	SELECT  v.idvisit, v.idvisitor, v.referer_url, a.name as current_url, a2.name as page_name, a3.name as previous_url
	,va.pageview_position, va.server_time
	,SPLIT_PART(a.name, '/', 1) as sp1
	,SPLIT_PART(a.name, '/', 2) as sp2
	,SPLIT_PART(a.name, '/', 3) as sp3
	,SPLIT_PART(a.name, '/', 4) as sp4
	,SPLIT_PART(a.name, '/', 5) as sp5
	,SPLIT_PART(a.name, '/', 6) as sp6
	,SPLIT_PART(a.name, '/', 7) as sp7
	,SPLIT_PART(a.name, '/', 8) as sp8
	FROM matomo_db_matomo_log_visit v
	LEFT JOIN matomo_db_matomo_log_link_visit_action va ON v.idvisit = va.idvisit
	LEFT JOIN matomo_db_matomo_log_action a ON a.idaction = va.idaction_url
	LEFT JOIN matomo_db_matomo_log_action a2 ON a2.idaction = va.idaction_name_ref
	LEFT JOIN matomo_db_matomo_log_action a3 ON a3.idaction = va.idaction_url_ref
	where v.idsite=3   -- and (v.idvisitor= upper('4196c1181e011e43'));


SELECT v.idvisit, v.idvisitor, v.referer_url
	,c.idorder, c.server_time, c.items, c.revenue, c.revenue_shipping, c.revenue_subtotal, c.revenue_tax, c.revenue_discount, c.visitor_returning, c.visitor_count_visits
 	,ci.idorder, ci.price, ci.quantity, ci.deleted
 	, acs.name  as ref, an.name as product_name, ac.name as product_cat_lvl1, ac2.name as product_cat_lvl2, ac3.name as product_cat_lvl3, ac4.name as product_cat_lvl4, ac5.name as product_cat_lvl5
FROM matomo_db_matomo_log_visit v
	JOIN matomo_db_matomo_log_conversion c ON v.idvisit = c.idvisit
	JOIN matomo_db_matomo_log_conversion_item ci ON v.idvisit = ci.idvisit
	left join matomo_db_matomo_log_action acs on acs.idaction =ci.idaction_sku
	left join matomo_db_matomo_log_action an on an.idaction =ci.idaction_name
	left join matomo_db_matomo_log_action ac on ac.idaction =ci.idaction_category
	left join matomo_db_matomo_log_action ac2 on ac2.idaction =ci.idaction_category2
	left join matomo_db_matomo_log_action ac3 on ac3.idaction =ci.idaction_category3
	left join matomo_db_matomo_log_action ac4 on ac4.idaction =ci.idaction_category4
	left join matomo_db_matomo_log_action ac5 on ac5.idaction =ci.idaction_category5
where v.idsite=3

*/





/*
sudo su bi
psql -d dwh
\c odoo
CREATE ROLE usrodoo LOGIN PASSWORD '123456';
grant connect on database odoo to usrodoo;
revoke all on schema public from public;
grant usage on schema public to usrodoo;
grant select on all tables in schema public to usrodoo;
ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT select on tables to usrodoo;


\c test1
CREATE ROLE usr1 LOGIN PASSWORD '123456';
grant connect on database odoo to test1;
revoke all on schema public from public;
grant usage on schema public to usr1;
grant select on all tables in schema public to usr1;
ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT select on tables to usr1;


\c test2
CREATE ROLE usr2 LOGIN PASSWORD '123456';
grant connect on database odoo to test2;
revoke all on schema public from public;
grant usage on schema public to usr2;
grant select on all tables in schema public to usr2;
ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT select on tables to usr2;


\c test3
CREATE ROLE usr3 LOGIN PASSWORD '123456';
grant connect on database odoo to test3;
revoke all on schema public from public;
grant usage on schema public to usr3;
grant select on all tables in schema public to usr3;
ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT select on tables to usr3;


--usuari per defecte
sudo mysql
create user usrmysql;
alter user usrmysql identified by '123456';
grant select on matomo_db.* to usrmysql;D


*/
