| Item         | Price | # In stock |
|----------|:-----:|-----------:|
| Juicy Apples |  1.99 |        739 |
| Bananas      |  1.89 |          6 |
# BI Coopdevs Ansible inventory

This repository stores hosts informations and related variables for this specific instance of BI infrastructure.

## Requirements

1. Clone this repo and [bi-provisioning](https://gitlab.com/coopdevs/bi-provisioning) in the same directory
2. If you want to test this set up locally, install [devenv](https://github.com/coopdevs/devenv/) and do:
   ```sh
   cd bi-coopdevs-inventory
   devenv # this creates the lxc container and sets its hostname
   ```
3. Go to `bi-provisioning` directory and install its Ansible dependencies:
   ```sh
   ansible-galaxy install -r requirements.yml
   ```
4. Run `ansible-playbook` command pointing to the `inventory/hosts` file of this repository:
   * development local mode
   ```sh
   # tell it to keep it local with limit=dev
   # use the user root the first time to create the other users: --user=root
   ansible-playbook playbooks/sys_admins.yml -i ../bi-coopdevs-inventory/inventory/hosts --limit=dev
   ansible-playbook playbooks/provision.yml -i ../bi-coopdevs-inventory/inventory/hosts --ask-vault-pass --limit=dev
   ```
   BW key: BI Coopdevs provisioning secret dev

   * Production mode
   ```sh
   ansible-playbook playbooks/sys_admins.yml -i ../bi-coopdevs-inventory/inventory/hosts --ask-vault-pass --limit=production
   ansible-playbook playbooks/provision.yml -i ../bi-coopdevs-inventory/inventory/hosts --ask-vault-pass --limit=production
   ```
   BW key: BI Coopdevs provisioning secret

5. For Cercles you have to create a ssh key, configure the key in Cercles server (162.55.62.29) and put a ssh tunnel inside the superset_app docker container
   ```sh
    docker exec -it superset_app ssh -4 -NL 5432:localhost:5432 jordi@162.55.62.29 &
   ```